package com.ruoyi.system.service;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;
import com.tencentcloudapi.sms.v20190711.models.SendStatus;
import org.springframework.stereotype.Service;

/**
 * 短信服务实现类
 *
 * Tencent Cloud Sms Sendsms
 * https://cloud.tencent.com/document/product/382/38778
 *
 * 需要引入腾讯云的maven依赖

    <dependency>
        <groupId>com.tencentcloudapi</groupId>
        <artifactId>tencentcloud-sdk-java</artifactId>
        <version>4.0.11</version>
    </dependency>

 */
@Service
public class SmsService {
//    Credential cred = new Credential("AKIDUosgDUWHqGwUnpbGfogHdiX6NN7a3KL6", "XpsgEXaPyHKBsNcbMr77sNcbLp2zg8LH");
    /** 支持短信的appId */
    public static final String appId = "1400620984";
    /** 用户密钥，登录腾讯云获取 */
    public static final String secretId = "AKIDUosgDUWHqGwUnpbGfogHdiX6NN7a3KL6";
    public static final String secretKey = "XpsgEXaPyHKBsNcbMr77sNcbLp2zg8LH";
    /** 短信签名（在这里和公众号同名） */
    public static final String signText = "每日健步走";
    /** 短信模板 */
    public static final String templateID = "1274091";

    /**
     * 给单个手机发送提醒短信
     * @param phoneNumber
     * @param code
     */
    public boolean sendMessage(String phoneNumber, String code){
        try {
            /* 必要步骤：
             * 实例化一个认证对象，入参需要传入腾讯云账户密钥对 secretId 和 secretKey
             * 本示例采用从环境变量读取的方式，需要预先在环境变量中设置这两个值
             * 您也可以直接在代码中写入密钥对，但需谨防泄露，不要将代码复制、上传或者分享给他人
             * CAM 密钥查询：https://console.cloud.tencent.com/cam/capi
             */
            Credential cred = new Credential(secretId, secretKey);

            /* 非必要步骤:
             * 实例化一个客户端配置对象，可以指定超时时间等配置 */
            ClientProfile clientProfile = new ClientProfile();
            /* SDK 默认用 TC3-HMAC-SHA256 进行签名
             * 非必要请不要修改该字段 */
            clientProfile.setSignMethod("HmacSHA256");
            /* 实例化 SMS 的 client 对象
             * 第二个参数是地域信息，可以直接填写字符串 ap-guangzhou，或者引用预设的常量 */
            SmsClient client = new SmsClient(cred, "",clientProfile);
            /* 实例化一个请求对象，根据调用的接口和实际情况，可以进一步设置请求参数
             * 您可以直接查询 SDK 源码确定接口有哪些属性可以设置
             * 属性可能是基本类型，也可能引用了另一个数据结构
             * 推荐使用 IDE 进行开发，可以方便地跳转查阅各个接口和数据结构的文档说明 */
            SendSmsRequest req = new SendSmsRequest();
            /* 填充请求参数，这里 request 对象的成员变量即对应接口的入参
             * 您可以通过官网接口文档或跳转到 request 对象的定义处查看请求参数的定义
             * 基本类型的设置:
             * 帮助链接：
             * 短信控制台：https://console.cloud.tencent.com/smsv2
             * sms helper：https://cloud.tencent.com/document/product/382/3773 */
            /* 短信应用 ID: 在 [短信控制台] 添加应用后生成的实际 SDKAppID，例如1400006666 */
            req.setSmsSdkAppid(appId);
            /* 短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，可登录 [短信控制台] 查看签名信息 */
            req.setSign(signText);
            /* 国际/港澳台短信 senderid: 国内短信填空，默认未开通，如需开通请联系 [sms helper] */
//            req.setSenderId("");
            /* 用户的 session 内容: 可以携带用户侧 ID 等上下文信息，server 会原样返回 */
//            req.setSessionContext("");
            /* 短信码号扩展号: 默认未开通，如需开通请联系 [sms helper] */
//            req.setExtendCode("");
            /* 模板 ID: 必须填写已审核通过的模板 ID，可登录 [短信控制台] 查看模板 ID */
            req.setTemplateID(templateID);
            /* 下发手机号码，采用 e.164 标准，+[国家或地区码][手机号]
             * 例如+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号*/
            String[] phoneNumbers = {"+86" + phoneNumber};
            req.setPhoneNumberSet(phoneNumbers);

            /* 模板参数: 若无模板参数，则设置为空*/
            String[] templateParams = {code};
            req.setTemplateParamSet(templateParams);

            /* 通过 client 对象调用 SendSms 方法发起请求。注意请求方法名与请求对象是对应的
             * 返回的 res 是一个 SendSmsResponse 类的实例，与请求对象对应 */
            SendSmsResponse res = client.SendSms(req);

            // 输出 JSON 格式的字符串回包
            System.out.println(SendSmsResponse.toJsonString(res));

            SendStatus[] sendStatusSet = res.getSendStatusSet();
            String str = "手机号为%s的短信，发送%s";
            if(sendStatusSet != null && sendStatusSet.length != 0){
                for(SendStatus status : sendStatusSet){
                    if("Ok".equals(status.getCode())){
                        System.out.println(String.format(str, status.getPhoneNumber(), "成功！"));
                        return true;
                    }else{
                        System.out.println(String.format(str, status.getPhoneNumber(), "失败，失败原因是："+status.getMessage()));
                        return false;
                    }
                }
            }

        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static void main( String[] args ) {
        /** 待发送的手机号 */
      String phoneNumber = "";
        /** 短信模板里面的参数 */
        String code = "123456";
        SmsService smsService = new SmsService();
        smsService.sendMessage(phoneNumber, code);
    }
}
