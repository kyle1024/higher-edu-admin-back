package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.ProductSku;
import com.ruoyi.system.domain.UserSkuStar;

/**
 * 用户sku收藏Service接口
 *
 * @author ruoyi
 * @date 2022-03-15
 */
public interface IUserSkuStarService
{
    /**
     * 查询用户sku收藏
     *
     * @param userId 用户sku收藏主键
     * @return 用户sku收藏
     */
    public UserSkuStar selectUserSkuStarByUserId(Long userId);

    /**
     * 查询用户sku收藏列表
     *
     * @return 用户sku收藏集合
     */
    public List<ProductSku> selectUserSkuStarList();

    /**
     * 新增用户sku收藏
     *
     * @param userSkuStar 用户sku收藏
     * @return 结果
     */
    public int insertUserSkuStar(UserSkuStar userSkuStar);

    /**
     * 修改用户sku收藏
     *
     * @param userSkuStar 用户sku收藏
     * @return 结果
     */
    public int updateUserSkuStar(UserSkuStar userSkuStar);

    /**
     * 批量删除用户sku收藏
     *
     * @param star 需要删除的用户sku收藏主键集合
     * @return 结果
     */
    public int deleteUserSkuStarByUserIdAndSkuId(UserSkuStar star);

    /**
     * 删除用户sku收藏信息
     *
     * @param userId 用户sku收藏主键
     * @return 结果
     */
    public int deleteUserSkuStarByUserId(Long userId);

    int transferStars(UserSkuStar star);
}
