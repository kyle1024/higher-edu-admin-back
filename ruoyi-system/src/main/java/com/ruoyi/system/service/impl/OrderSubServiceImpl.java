package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.dto.OrderSubDTO;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.service.TokenService;
import com.ruoyi.common.utils.Assert;
import com.ruoyi.system.domain.ProductSku;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.OrderSubMapper;
import com.ruoyi.system.domain.OrderSub;
import com.ruoyi.system.service.IOrderSubService;

/**
 * 用户订单子Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-18
 */
@Slf4j
@Service
public class OrderSubServiceImpl implements IOrderSubService
{
    @Autowired
    private OrderSubMapper orderSubMapper;
    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private TokenService tokenService;

    /**
     * 查询用户订单子
     *
     * @param id 用户订单子主键
     * @return 用户订单子
     */
    @Override
    public OrderSub selectOrderSubById(Long id)
    {
        return orderSubMapper.selectOrderSubById(id);
    }

    /**
     * 查询用户订单子列表
     *
     * @param orderSub 用户订单子
     * @return 用户订单子
     */
    @Override
    public List<OrderSub> selectOrderSubList(OrderSub orderSub)
    {
        return orderSubMapper.selectOrderSubList(orderSub);
    }

    @Override
    public List<ProductSku> selectUserOrderSubList(OrderSub orderSub)
    {
        Long userId = tokenService.getLoginUserId();
        return orderSubMapper.selectUserOrderSubList(userId);
    }

    /**
     * 新增用户订单子
     *
     * @param orderSub 用户订单子
     * @return 结果
     */
    @Override
    public int insertOrderSub(OrderSub orderSub)
    {
        return orderSubMapper.insertOrderSub(orderSub);
    }

    /**
     * 修改用户订单子
     *
     * @param orderSub 用户订单子
     * @return 结果
     */
    @Override
    public int updateOrderSub(OrderSub orderSub)
    {
        return orderSubMapper.updateOrderSub(orderSub);
    }

    /**
     * 批量删除用户订单子
     *
     * @param ids 需要删除的用户订单子主键
     * @return 结果
     */
    @Override
    public int deleteOrderSubByIds(Long[] ids)
    {
        return orderSubMapper.deleteOrderSubByIds(ids);
    }

    /**
     * 删除用户订单子信息
     *
     * @param id 用户订单子主键
     * @return 结果
     */
    @Override
    public int deleteOrderSubById(Long id)
    {
        return orderSubMapper.deleteOrderSubById(id);
    }

    @Override
    public int tranfer(OrderSubDTO dto) {
        OrderSub sub = new OrderSub();
        sub.setUserId(dto.getUserId());
        sub.setSkuId(dto.getSkuId());
        return orderSubMapper.insertOrderSub(sub);
    }

    @Override
    public int tranferBatch(OrderSubDTO dto) {
        int count = 0;
        OrderSub sub = new OrderSub();
        for (Long userId : dto.getUserIdList()) {
            sub.setUserId(userId);
            sub.setSkuId(dto.getSkuId());
            count += orderSubMapper.insertOrderSub(sub);
        }
        return count;
    }
}
