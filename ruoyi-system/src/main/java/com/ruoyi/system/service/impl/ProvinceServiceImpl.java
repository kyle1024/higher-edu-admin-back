package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProvinceMapper;
import com.ruoyi.system.domain.Province;
import com.ruoyi.system.service.IProvinceService;

/**
 * 省份Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
@Service
public class ProvinceServiceImpl implements IProvinceService 
{
    @Autowired
    private ProvinceMapper provinceMapper;

    /**
     * 查询省份
     * 
     * @param code 省份主键
     * @return 省份
     */
    @Override
    public Province selectProvinceByCode(String code)
    {
        return provinceMapper.selectProvinceByCode(code);
    }

    /**
     * 查询省份列表
     * 
     * @param province 省份
     * @return 省份
     */
    @Override
    public List<Province> selectProvinceList(Province province)
    {
        return provinceMapper.selectProvinceList(province);
    }

    /**
     * 新增省份
     * 
     * @param province 省份
     * @return 结果
     */
    @Override
    public int insertProvince(Province province)
    {
        return provinceMapper.insertProvince(province);
    }

    /**
     * 修改省份
     * 
     * @param province 省份
     * @return 结果
     */
    @Override
    public int updateProvince(Province province)
    {
        return provinceMapper.updateProvince(province);
    }

    /**
     * 批量删除省份
     * 
     * @param codes 需要删除的省份主键
     * @return 结果
     */
    @Override
    public int deleteProvinceByCodes(String[] codes)
    {
        return provinceMapper.deleteProvinceByCodes(codes);
    }

    /**
     * 删除省份信息
     * 
     * @param code 省份主键
     * @return 结果
     */
    @Override
    public int deleteProvinceByCode(String code)
    {
        return provinceMapper.deleteProvinceByCode(code);
    }
}
