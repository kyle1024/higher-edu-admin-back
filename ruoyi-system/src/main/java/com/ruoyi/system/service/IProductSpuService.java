package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.ProductSpu;

/**
 * 商品spuService接口
 * 
 * @author ruoyi
 * @date 2022-03-15
 */
public interface IProductSpuService 
{
    /**
     * 查询商品spu
     * 
     * @param id 商品spu主键
     * @return 商品spu
     */
    public ProductSpu selectProductSpuById(Long id);

    /**
     * 查询商品spu列表
     * 
     * @param productSpu 商品spu
     * @return 商品spu集合
     */
    public List<ProductSpu> selectProductSpuList(ProductSpu productSpu);

    /**
     * 新增商品spu
     * 
     * @param productSpu 商品spu
     * @return 结果
     */
    public int insertProductSpu(ProductSpu productSpu);

    /**
     * 修改商品spu
     * 
     * @param productSpu 商品spu
     * @return 结果
     */
    public int updateProductSpu(ProductSpu productSpu);

    /**
     * 批量删除商品spu
     * 
     * @param ids 需要删除的商品spu主键集合
     * @return 结果
     */
    public int deleteProductSpuByIds(Long[] ids);

    /**
     * 删除商品spu信息
     * 
     * @param id 商品spu主键
     * @return 结果
     */
    public int deleteProductSpuById(Long id);
}
