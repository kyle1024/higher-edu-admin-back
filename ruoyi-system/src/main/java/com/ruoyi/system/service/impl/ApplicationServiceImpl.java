package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.vo.ApplicationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ApplicationMapper;
import com.ruoyi.system.domain.Application;
import com.ruoyi.system.service.IApplicationService;

/**
 * 报名信息Service业务层处理
 *
 * @author ruoyi
 * @date 2022-05-08
 */
@Service
public class ApplicationServiceImpl implements IApplicationService
{
    @Autowired
    private ApplicationMapper applicationMapper;

    /**
     * 查询报名信息
     *
     * @param id 报名信息主键
     * @return 报名信息
     */
    @Override
    public Application selectApplicationById(Long id)
    {
        return applicationMapper.selectApplicationById(id);
    }

    /**
     * 查询报名信息列表
     *
     * @param application 报名信息
     * @return 报名信息
     */
    @Override
    public List<Application> selectApplicationList(Application application)
    {
        return applicationMapper.selectApplicationList(application);
    }

    @Override
    public List<ApplicationVO> selectApplicationList2(Application application)
    {
        return applicationMapper.selectApplicationList2(application);
    }

    /**
     * 新增报名信息
     *
     * @param application 报名信息
     * @return 结果
     */
    @Override
    public int insertApplication(Application application)
    {
        application.setCreateTime(DateUtils.getNowDate());
        return applicationMapper.insertApplication(application);
    }

    /**
     * 修改报名信息
     *
     * @param application 报名信息
     * @return 结果
     */
    @Override
    public int updateApplication(Application application)
    {
        application.setUpdateTime(DateUtils.getNowDate());
        return applicationMapper.updateApplication(application);
    }

    /**
     * 批量删除报名信息
     *
     * @param ids 需要删除的报名信息主键
     * @return 结果
     */
    @Override
    public int deleteApplicationByIds(Long[] ids)
    {
        return applicationMapper.deleteApplicationByIds(ids);
    }

    /**
     * 删除报名信息信息
     *
     * @param id 报名信息主键
     * @return 结果
     */
    @Override
    public int deleteApplicationById(Long id)
    {
        return applicationMapper.deleteApplicationById(id);
    }
}
