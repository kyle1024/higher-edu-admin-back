package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.service.TokenService;
import com.ruoyi.system.domain.ProductSku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UserSkuStarMapper;
import com.ruoyi.system.domain.UserSkuStar;
import com.ruoyi.system.service.IUserSkuStarService;

/**
 * 用户sku收藏Service业务层处理
 *
 * @author ruoyi
 * @date 2022-03-15
 */
@Service
public class UserSkuStarServiceImpl implements IUserSkuStarService
{
    @Autowired
    private UserSkuStarMapper userSkuStarMapper;
    @Autowired
    private TokenService tokenService;

    /**
     * 查询用户sku收藏
     *
     * @param userId 用户sku收藏主键
     * @return 用户sku收藏
     */
    @Override
    public UserSkuStar selectUserSkuStarByUserId(Long userId)
    {
        return userSkuStarMapper.selectUserSkuStarByUserId(userId);
    }

    /**
     * 查询用户sku收藏列表
     *
     * @return 用户sku收藏
     */
    @Override
    public List<ProductSku> selectUserSkuStarList()
    {
        Long userId = tokenService.getLoginUserId();
        return userSkuStarMapper.selectUserSkuStarList(userId);
//        return null;
    }

    /**
     * 新增用户sku收藏
     *
     * @param userSkuStar 用户sku收藏
     * @return 结果
     */
    @Override
    public int insertUserSkuStar(UserSkuStar userSkuStar)
    {
        return userSkuStarMapper.insertUserSkuStar(userSkuStar);
    }

    /**
     * 修改用户sku收藏
     *
     * @param userSkuStar 用户sku收藏
     * @return 结果
     */
    @Override
    public int updateUserSkuStar(UserSkuStar userSkuStar)
    {
        return userSkuStarMapper.updateUserSkuStar(userSkuStar);
    }

    /**
     * 批量删除用户sku收藏
     *
     * @param star 需要删除的用户sku收藏主键
     * @return 结果
     */
    @Override
    public int deleteUserSkuStarByUserIdAndSkuId(UserSkuStar star)
    {
        return userSkuStarMapper.deleteUserSkuStarByUserIdAndSkuId(star);
    }

    /**
     * 删除用户sku收藏信息
     *
     * @param userId 用户sku收藏主键
     * @return 结果
     */
    @Override
    public int deleteUserSkuStarByUserId(Long userId)
    {
        return userSkuStarMapper.deleteUserSkuStarByUserId(userId);
    }

    @Override
    public int transferStars(UserSkuStar star) {
        Long myUserId = tokenService.getLoginUserId();
        return userSkuStarMapper.transferStars(myUserId, star.getUserId());
    }
}
