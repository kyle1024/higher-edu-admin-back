package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Areas;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
public interface IAreasService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param code 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public Areas selectAreasByCode(String code);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param areas 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Areas> selectAreasList(Areas areas);

    /**
     * 新增【请填写功能名称】
     * 
     * @param areas 【请填写功能名称】
     * @return 结果
     */
    public int insertAreas(Areas areas);

    /**
     * 修改【请填写功能名称】
     * 
     * @param areas 【请填写功能名称】
     * @return 结果
     */
    public int updateAreas(Areas areas);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param codes 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteAreasByCodes(String[] codes);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param code 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteAreasByCode(String code);
}
