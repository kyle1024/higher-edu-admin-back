package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.AreasMapper;
import com.ruoyi.system.domain.Areas;
import com.ruoyi.system.service.IAreasService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
@Service
public class AreasServiceImpl implements IAreasService 
{
    @Autowired
    private AreasMapper areasMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param code 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public Areas selectAreasByCode(String code)
    {
        return areasMapper.selectAreasByCode(code);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param areas 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Areas> selectAreasList(Areas areas)
    {
        return areasMapper.selectAreasList(areas);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param areas 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertAreas(Areas areas)
    {
        return areasMapper.insertAreas(areas);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param areas 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateAreas(Areas areas)
    {
        return areasMapper.updateAreas(areas);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param codes 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAreasByCodes(String[] codes)
    {
        return areasMapper.deleteAreasByCodes(codes);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param code 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteAreasByCode(String code)
    {
        return areasMapper.deleteAreasByCode(code);
    }
}
