package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanValidators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MajorMapper;
import com.ruoyi.system.domain.Major;
import com.ruoyi.system.service.IMajorService;

import javax.validation.Validator;

/**
 * 专业Service业务层处理
 *
 * @author ruoyi
 * @date 2022-05-08
 */

@Slf4j
@Service
public class MajorServiceImpl implements IMajorService
{
    @Autowired
    private MajorMapper majorMapper;

    @Autowired
    protected Validator validator;

    /**
     * 查询专业
     *
     * @param id 专业主键
     * @return 专业
     */
    @Override
    public Major selectMajorById(Long id)
    {
        return majorMapper.selectMajorById(id);
    }

    /**
     * 查询专业列表
     *
     * @param major 专业
     * @return 专业
     */
    @Override
    public List<Major> selectMajorList(Major major)
    {
        return majorMapper.selectMajorList(major);
    }

    /**
     * 新增专业
     *
     * @param major 专业
     * @return 结果
     */
    @Override
    public int insertMajor(Major major)
    {
        major.setCreateTime(DateUtils.getNowDate());
        return majorMapper.insertMajor(major);
    }

    /**
     * 修改专业
     *
     * @param major 专业
     * @return 结果
     */
    @Override
    public int updateMajor(Major major)
    {
        major.setUpdateTime(DateUtils.getNowDate());
        return majorMapper.updateMajor(major);
    }

    /**
     * 批量删除专业
     *
     * @param ids 需要删除的专业主键
     * @return 结果
     */
    @Override
    public int deleteMajorByIds(Long[] ids)
    {
        return majorMapper.deleteMajorByIds(ids);
    }

    /**
     * 删除专业信息
     *
     * @param id 专业主键
     * @return 结果
     */
    @Override
    public int deleteMajorById(Long id)
    {
        return majorMapper.deleteMajorById(id);
    }

    /**
     * 导入专业数据
     *
     * @param majorList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importMajor(List<Major> majorList, boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(majorList) || majorList.size() == 0) {
            throw new ServiceException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (Major major : majorList) {
            try {
                // 验证是否存在这个专业
                Major m = majorMapper.selectMajorByCode(major.getCode());
                if (StringUtils.isNull(m)) {
                    BeanValidators.validateWithException(validator, major);
                    major.setCreateBy(operName);
                    this.insertMajor(major);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、专业 【" + major.getName() + "】 导入成功");
                } else if (isUpdateSupport) {
                    BeanValidators.validateWithException(validator, major);
                    major.setUpdateBy(operName);
                    this.updateMajor(major);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、专业 【" + major.getName() + "】 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、专业 【" + major.getName() + "】 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、专业 【" + major.getName() + "】 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
