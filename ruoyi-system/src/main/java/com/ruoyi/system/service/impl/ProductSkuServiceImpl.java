package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ProductSkuMapper;
import com.ruoyi.system.domain.ProductSku;
import com.ruoyi.system.service.IProductSkuService;

/**
 * 商品skuService业务层处理
 * 
 * @author ruoyi
 * @date 2022-03-15
 */
@Service
public class ProductSkuServiceImpl implements IProductSkuService 
{
    @Autowired
    private ProductSkuMapper productSkuMapper;

    /**
     * 查询商品sku
     * 
     * @param id 商品sku主键
     * @return 商品sku
     */
    @Override
    public ProductSku selectProductSkuById(Long id)
    {
        return productSkuMapper.selectProductSkuById(id);
    }

    /**
     * 查询商品sku列表
     * 
     * @param productSku 商品sku
     * @return 商品sku
     */
    @Override
    public List<ProductSku> selectProductSkuList(ProductSku productSku)
    {
        return productSkuMapper.selectProductSkuList(productSku);
    }

    /**
     * 新增商品sku
     * 
     * @param productSku 商品sku
     * @return 结果
     */
    @Override
    public int insertProductSku(ProductSku productSku)
    {
        productSku.setCreateTime(DateUtils.getNowDate());
        return productSkuMapper.insertProductSku(productSku);
    }

    /**
     * 修改商品sku
     * 
     * @param productSku 商品sku
     * @return 结果
     */
    @Override
    public int updateProductSku(ProductSku productSku)
    {
        productSku.setUpdateTime(DateUtils.getNowDate());
        return productSkuMapper.updateProductSku(productSku);
    }

    /**
     * 批量删除商品sku
     * 
     * @param ids 需要删除的商品sku主键
     * @return 结果
     */
    @Override
    public int deleteProductSkuByIds(Long[] ids)
    {
        return productSkuMapper.deleteProductSkuByIds(ids);
    }

    /**
     * 删除商品sku信息
     * 
     * @param id 商品sku主键
     * @return 结果
     */
    @Override
    public int deleteProductSkuById(Long id)
    {
        return productSkuMapper.deleteProductSkuById(id);
    }
}
