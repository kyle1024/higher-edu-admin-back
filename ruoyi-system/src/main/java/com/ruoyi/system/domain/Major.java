package com.ruoyi.system.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.time.LocalDateTime;

/**
 * 专业对象 major
 *
 * @author ruoyi
 * @date 2022-05-08
 */
@ApiModel("专业对象")
public class Major extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 专业代码 */
    @Excel(name = "专业代码")
    @ApiModelProperty("专业代码")
    private String code;

    /** 专业名称 */
    @Excel(name = "专业名称")
    @ApiModelProperty("专业名称")
    private String name;

    /** 专业层次code */
    @Excel(name = "专业层次code")
    @ApiModelProperty("专业层次code")
    private String level;

    /** 学校归属 */
    @Excel(name = "学校归属")
    @ApiModelProperty("学校归属")
    private String schoolBelonged;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setLevel(String level)
    {
        this.level = level;
    }

    public String getLevel()
    {
        return level;
    }
    public void setSchoolBelonged(String schoolBelonged)
    {
        this.schoolBelonged = schoolBelonged;
    }

    public String getSchoolBelonged()
    {
        return schoolBelonged;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("code", getCode())
                .append("name", getName())
                .append("level", getLevel())
                .append("schoolBelonged", getSchoolBelonged())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
