package com.ruoyi.system.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.poi.hpsf.Decimal;

import java.math.BigDecimal;

/**
 * 商品sku对象 product_sku
 *
 * @author ruoyi
 * @date 2022-03-15
 */
@ApiModel("商品sku对象")
public class ProductSku extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("商品skuID")
    private Long id;

    /** spu的id */
    private Long spuId;

    @ApiModelProperty("商品展示价格")
    private String showPrice;

    @ApiModelProperty("商品名")
    private String name;

    @ApiModelProperty("商品数量")
    private Long sum;

    @ApiModelProperty("首页图片路径")
    private String bannerUrl;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getShowPrice() {
        return showPrice;
    }

    public void setShowPrice(String showPrice) {
        this.showPrice = showPrice;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSpuId(Long spuId)
    {
        this.spuId = spuId;
    }

    public Long getSpuId()
    {
        return spuId;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setSum(Long sum)
    {
        this.sum = sum;
    }

    public Long getSum()
    {
        return sum;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("spuId", getSpuId())
            .append("name", getName())
            .append("sum", getSum())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
