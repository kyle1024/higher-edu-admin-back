package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 城市对象 city
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
public class City extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 城市code */
    private String code;

    /** 城市名 */
    @Excel(name = "城市名")
    private String name;

    /** 省份code */
    @Excel(name = "省份code")
    private String provinceCode;

    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setProvinceCode(String provinceCode) 
    {
        this.provinceCode = provinceCode;
    }

    public String getProvinceCode() 
    {
        return provinceCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("code", getCode())
            .append("name", getName())
            .append("provinceCode", getProvinceCode())
            .toString();
    }
}
