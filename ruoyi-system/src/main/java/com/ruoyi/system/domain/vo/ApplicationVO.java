package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 报名信息对象 application
 *
 * @author ruoyi
 * @date 2022-05-08
 */
@ApiModel("报名信息对象")
public class ApplicationVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 学生姓名 */
    @ApiModelProperty("学生姓名")
    private String studentName;

    /** 区域code */
    @Excel(name = "区域code")
    @ApiModelProperty("区域code")
    private String areaCode;

    @ApiModelProperty("区域")
    private String area;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    /** 学历code */
    @Excel(name = "学历code")
    @ApiModelProperty("学历code")
    private String eduCode;

    /** 专业code */
    @Excel(name = "专业code")
    @ApiModelProperty("专业code")
    private String majorCode;

    /** 专业code */
    @ApiModelProperty("专业")
    private String major;

    /** 专业id */
    @Excel(name = "专业id")
    @ApiModelProperty("专业id")
    private Long majorId;

    /** 手机号 */
    @Excel(name = "手机号")
    @ApiModelProperty("手机号")
    private String phone;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setStudentName(String studentName)
    {
        this.studentName = studentName;
    }

    public String getStudentName()
    {
        return studentName;
    }
    public void setAreaCode(String areaCode)
    {
        this.areaCode = areaCode;
    }

    public String getAreaCode()
    {
        return areaCode;
    }
    public void setEduCode(String eduCode)
    {
        this.eduCode = eduCode;
    }

    public String getEduCode()
    {
        return eduCode;
    }
    public void setMajorCode(String majorCode)
    {
        this.majorCode = majorCode;
    }

    public String getMajorCode()
    {
        return majorCode;
    }
    public void setMajorId(Long majorId)
    {
        this.majorId = majorId;
    }

    public Long getMajorId()
    {
        return majorId;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("studentName", getStudentName())
                .append("areaCode", getAreaCode())
                .append("eduCode", getEduCode())
                .append("majorCode", getMajorCode())
                .append("majorId", getMajorId())
                .append("phone", getPhone())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
