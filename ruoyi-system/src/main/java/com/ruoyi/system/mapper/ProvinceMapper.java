package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Province;

/**
 * 省份Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-08
 */
public interface ProvinceMapper 
{
    /**
     * 查询省份
     * 
     * @param code 省份主键
     * @return 省份
     */
    public Province selectProvinceByCode(String code);

    /**
     * 查询省份列表
     * 
     * @param province 省份
     * @return 省份集合
     */
    public List<Province> selectProvinceList(Province province);

    /**
     * 新增省份
     * 
     * @param province 省份
     * @return 结果
     */
    public int insertProvince(Province province);

    /**
     * 修改省份
     * 
     * @param province 省份
     * @return 结果
     */
    public int updateProvince(Province province);

    /**
     * 删除省份
     * 
     * @param code 省份主键
     * @return 结果
     */
    public int deleteProvinceByCode(String code);

    /**
     * 批量删除省份
     * 
     * @param codes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProvinceByCodes(String[] codes);
}
