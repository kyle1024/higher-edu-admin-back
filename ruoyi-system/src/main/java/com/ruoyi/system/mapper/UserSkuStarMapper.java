package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.ProductSku;
import com.ruoyi.system.domain.UserSkuStar;
import org.apache.ibatis.annotations.Param;

/**
 * 用户sku收藏Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-15
 */
public interface UserSkuStarMapper
{
    /**
     * 查询用户sku收藏
     *
     * @param userId 用户sku收藏主键
     * @return 用户sku收藏
     */
    public UserSkuStar selectUserSkuStarByUserId(Long userId);

    /**
     * 查询用户sku收藏列表
     *
     * @param userId 用户sku收藏
     * @return 用户sku收藏集合
     */
    List<ProductSku> selectUserSkuStarList(Long userId);

    /**
     * 新增用户sku收藏
     *
     * @param userSkuStar 用户sku收藏
     * @return 结果
     */
    public int insertUserSkuStar(UserSkuStar userSkuStar);

    /**
     * 修改用户sku收藏
     *
     * @param userSkuStar 用户sku收藏
     * @return 结果
     */
    public int updateUserSkuStar(UserSkuStar userSkuStar);

    /**
     * 删除用户sku收藏
     *
     * @param userId 用户sku收藏主键
     * @return 结果
     */
    public int deleteUserSkuStarByUserId(Long userId);

    /**
     * 批量删除用户sku收藏
     *
     * @param star 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserSkuStarByUserIdAndSkuId(UserSkuStar star);

    int transferStars(@Param("myUserId") Long myUserId, @Param("youUserId")Long youUserId);
}
