package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.OrderSub;
import com.ruoyi.system.domain.ProductSku;

/**
 * 用户订单子Mapper接口
 *
 * @author ruoyi
 * @date 2022-03-18
 */
public interface OrderSubMapper
{
    /**
     * 查询用户订单子
     *
     * @param id 用户订单子主键
     * @return 用户订单子
     */
    public OrderSub selectOrderSubById(Long id);

    /**
     * 查询用户订单子列表
     *
     * @param userId 用户订单子
     * @return 用户订单子集合
     */
    public List<ProductSku> selectUserOrderSubList(Long userId);

    public List<OrderSub> selectOrderSubList(OrderSub orderSub);

    /**
     * 新增用户订单子
     *
     * @param orderSub 用户订单子
     * @return 结果
     */
    public int insertOrderSub(OrderSub orderSub);

    /**
     * 修改用户订单子
     *
     * @param orderSub 用户订单子
     * @return 结果
     */
    public int updateOrderSub(OrderSub orderSub);

    /**
     * 删除用户订单子
     *
     * @param id 用户订单子主键
     * @return 结果
     */
    public int deleteOrderSubById(Long id);

    /**
     * 批量删除用户订单子
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderSubByIds(Long[] ids);
}
