package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.ProductSku;

/**
 * 商品skuMapper接口
 * 
 * @author ruoyi
 * @date 2022-03-15
 */
public interface ProductSkuMapper 
{
    /**
     * 查询商品sku
     * 
     * @param id 商品sku主键
     * @return 商品sku
     */
    public ProductSku selectProductSkuById(Long id);

    /**
     * 查询商品sku列表
     * 
     * @param productSku 商品sku
     * @return 商品sku集合
     */
    public List<ProductSku> selectProductSkuList(ProductSku productSku);

    /**
     * 新增商品sku
     * 
     * @param productSku 商品sku
     * @return 结果
     */
    public int insertProductSku(ProductSku productSku);

    /**
     * 修改商品sku
     * 
     * @param productSku 商品sku
     * @return 结果
     */
    public int updateProductSku(ProductSku productSku);

    /**
     * 删除商品sku
     * 
     * @param id 商品sku主键
     * @return 结果
     */
    public int deleteProductSkuById(Long id);

    /**
     * 批量删除商品sku
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductSkuByIds(Long[] ids);
}
