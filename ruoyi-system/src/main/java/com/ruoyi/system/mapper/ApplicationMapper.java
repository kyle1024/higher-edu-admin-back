package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Application;
import com.ruoyi.system.domain.vo.ApplicationVO;

/**
 * 报名信息Mapper接口
 *
 * @author ruoyi
 * @date 2022-05-08
 */
public interface ApplicationMapper
{
    /**
     * 查询报名信息
     *
     * @param id 报名信息主键
     * @return 报名信息
     */
    public Application selectApplicationById(Long id);

    /**
     * 查询报名信息列表
     *
     * @param application 报名信息
     * @return 报名信息集合
     */
    public List<Application> selectApplicationList(Application application);

    public List<ApplicationVO> selectApplicationList2(Application application);

    /**
     * 新增报名信息
     *
     * @param application 报名信息
     * @return 结果
     */
    public int insertApplication(Application application);

    /**
     * 修改报名信息
     *
     * @param application 报名信息
     * @return 结果
     */
    public int updateApplication(Application application);

    /**
     * 删除报名信息
     *
     * @param id 报名信息主键
     * @return 结果
     */
    public int deleteApplicationById(Long id);

    /**
     * 批量删除报名信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteApplicationByIds(Long[] ids);
}
