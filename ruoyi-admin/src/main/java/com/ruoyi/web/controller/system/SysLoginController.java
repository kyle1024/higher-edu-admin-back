package com.ruoyi.web.controller.system;

import java.util.List;
import java.util.Set;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.dto.SysUserDTO;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.Assert;
import com.ruoyi.framework.web.service.TempTokenService;
import com.ruoyi.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.framework.web.service.SysPermissionService;
import com.ruoyi.system.service.ISysMenuService;

/**
 * 登录验证
 *
 * @author ruoyi
 */
@Api("登录验证模块")
@Slf4j
@RestController
public class SysLoginController
{
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private TempTokenService tempTokenService;

    /**
     * 登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody)
    {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    @GetMapping("/sendMessage/{phoneNumber}")
    @ApiOperation(value = "发送短信", httpMethod = "GET")
    public AjaxResult sendMessage(@ApiParam("phoneNumber") @PathVariable("phoneNumber")String phoneNumber)
    {
        loginService.sendMessage(phoneNumber);
        return AjaxResult.success();
    }

    /**
     * 获取验证码
     *
     * @return 结果
     */
    @GetMapping("/verificationCode")
    public AjaxResult getVerificationCode(String phonenumber)
    {
        String code = loginService.getVerificationCode(phonenumber);
        Assert.notEmpty(code, "验证码仍在有效期内");
        return AjaxResult.success();
    }

    /**
     *
     */
    @PostMapping("signin")
    @ApiOperation(value = "注册/登录统一接口（自动注册）", httpMethod = "POST")
    public AjaxResult register(@RequestBody SysUserDTO user)
    {
        Assert.notEmpty(user.getPhoneNumber(), "手机号不能为空");
        Assert.notEmpty(user.getCode(), "验证码不能为空");
        SysUser sysUser = new SysUser();
        sysUser.setPhonenumber(user.getPhoneNumber());
        String verificationCode = loginService.getVerificationCode(user.getPhoneNumber());
        Assert.isTrue(user.getCode().equals(verificationCode), "手机号/验证码不正确");
        if(UserConstants.UNIQUE.equals(userService.checkPhoneUnique(sysUser))) {
            sysUser.setUserName(user.getPhoneNumber());
            sysUser.setNickName(user.getPhoneNumber());
            userService.insertUser(sysUser);
            log.info("注册成功");
        }
        SysUser oneUser = userService.selectUserByPhoneNumber(user.getPhoneNumber());
        AjaxResult ajax = AjaxResult.success();
//        // 生成令牌
        LoginUser loginUser = new LoginUser();
        loginUser.setUserId(oneUser.getUserId());
        loginUser.setUser(oneUser);
        String token = tempTokenService.createToken(loginUser);
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo()
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public AjaxResult getRouters()
    {
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(userId);
        return AjaxResult.success(menuService.buildMenus(menus));
    }
}
