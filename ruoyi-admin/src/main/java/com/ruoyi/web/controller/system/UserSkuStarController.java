package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.ProductSku;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.UserSkuStar;
import com.ruoyi.system.service.IUserSkuStarService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户sku收藏Controller
 *
 * @author ruoyi
 * @date 2022-03-15
 */
//@Api("收藏模块")
@RestController
@RequestMapping("/system/star")
public class UserSkuStarController extends BaseController
{
    @Autowired
    private IUserSkuStarService userSkuStarService;

    @GetMapping("/page")
//    @ApiOperation(value = "查询用户sku收藏列表(分页)", httpMethod = "GET")
    public TableDataInfo page()
    {
        startPage();
        List<ProductSku> list = userSkuStarService.selectUserSkuStarList();
        return getDataTable(list);
    }

    @GetMapping("/list")
//    @ApiOperation(value = "查询用户sku收藏列表", httpMethod = "GET")
    public AjaxResult list()
    {
        List<ProductSku> list = userSkuStarService.selectUserSkuStarList();
        return AjaxResult.success(list);
    }

    /**
     * 导出用户sku收藏列表
     */
//    @Log(title = "用户sku收藏", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, UserSkuStar userSkuStar)
    {
//        List<UserSkuStar> list = userSkuStarService.selectUserSkuStarList();
//        ExcelUtil<UserSkuStar> util = new ExcelUtil<UserSkuStar>(UserSkuStar.class);
//        util.exportExcel(response, list, "用户sku收藏数据");
    }

    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(userSkuStarService.selectUserSkuStarByUserId(userId));
    }

    @PostMapping
//    @ApiOperation(value = "用户新增收藏", httpMethod = "POST")
    public AjaxResult add(@RequestBody UserSkuStar star)
    {
        userSkuStarService.insertUserSkuStar(star);
        return AjaxResult.success();
    }

//    @Log(title = "用户取消收藏", businessType = BusinessType.DELETE)
	@DeleteMapping()
//    @ApiOperation(value = "用户取消收藏", httpMethod = "DELETE")
    public AjaxResult remove(@RequestBody UserSkuStar star)
    {
        userSkuStarService.deleteUserSkuStarByUserIdAndSkuId(star);
        return AjaxResult.success();
    }

    @PostMapping("transfer")
//    @ApiOperation(value = "用户转移收藏", httpMethod = "POST")
    public AjaxResult transfer(@RequestBody UserSkuStar star)
    {
        userSkuStarService.transferStars(star);
        return AjaxResult.success();
    }
}
