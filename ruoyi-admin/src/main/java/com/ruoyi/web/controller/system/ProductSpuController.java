package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductSpu;
import com.ruoyi.system.service.IProductSpuService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品spuController
 *
 * @author ruoyi
 * @date 2022-03-15
 */
@RestController
@RequestMapping("/system/spu")
public class ProductSpuController extends BaseController
{
    @Autowired
    private IProductSpuService productSpuService;

    /**
     * 查询商品spu列表
     */
    @PreAuthorize("@ss.hasPermi('system:spu:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductSpu productSpu)
    {
        startPage();
        List<ProductSpu> list = productSpuService.selectProductSpuList(productSpu);
        return getDataTable(list);
    }

    /**
     * 导出商品spu列表
     */
    @PreAuthorize("@ss.hasPermi('system:spu:export')")
    @Log(title = "商品spu", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductSpu productSpu)
    {
        List<ProductSpu> list = productSpuService.selectProductSpuList(productSpu);
        ExcelUtil<ProductSpu> util = new ExcelUtil<ProductSpu>(ProductSpu.class);
        util.exportExcel(response, list, "商品spu数据");
    }

    /**
     * 获取商品spu详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:spu:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(productSpuService.selectProductSpuById(id));
    }

    /**
     * 新增商品spu
     */
    @PreAuthorize("@ss.hasPermi('system:spu:add')")
    @Log(title = "商品spu", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductSpu productSpu)
    {
        return toAjax(productSpuService.insertProductSpu(productSpu));
    }

    /**
     * 修改商品spu
     */
    @PreAuthorize("@ss.hasPermi('system:spu:edit')")
    @Log(title = "商品spu", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductSpu productSpu)
    {
        return toAjax(productSpuService.updateProductSpu(productSpu));
    }

    /**
     * 删除商品spu
     */
    @PreAuthorize("@ss.hasPermi('system:spu:remove')")
    @Log(title = "商品spu", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productSpuService.deleteProductSpuByIds(ids));
    }
}
