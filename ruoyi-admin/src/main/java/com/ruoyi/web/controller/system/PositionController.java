package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.Areas;
import com.ruoyi.system.domain.City;
import com.ruoyi.system.service.IAreasService;
import com.ruoyi.system.service.ICityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Province;
import com.ruoyi.system.service.IProvinceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 省份Controller
 *
 * @author ruoyi
 * @date 2022-05-08
 */
@Api("行政区划管理")
@RestController
@RequestMapping("/system/position")
public class PositionController extends BaseController
{
    @Autowired
    private IProvinceService provinceService;
    @Autowired
    private ICityService cityService;
    @Autowired
    private IAreasService areasService;

    /**
     * 查询省份列表
     */
    @ApiOperation("省份列表")
    @GetMapping("/province/list")
    public AjaxResult provinceList(Province province)
    {
        List<Province> list = provinceService.selectProvinceList(province);
        return AjaxResult.success(list);
    }

    /**
     * 根据省份code查询城市列表
     */
    @ApiOperation("根据省份code查询城市列表")
    @GetMapping("/city/list")
    public AjaxResult cityList(@RequestParam("provinceCode") String provinceCode)
    {
        City city = new City();
        city.setProvinceCode(provinceCode);
        List<City> list = cityService.selectCityList(city);
        return AjaxResult.success(list);
    }

    /**
     * 根据省份code查询城市列表
     */
    @ApiOperation("根据省份code查询城市列表")
    @GetMapping("/areas/list")
    public AjaxResult list(@RequestParam("cityCode") String cityCode)
    {
        Areas areas = new Areas();
        areas.setCityCode(cityCode);
        List<Areas> list = areasService.selectAreasList(areas);
        return AjaxResult.success(list);
    }



    /**
     * 导出省份列表
     */
    @PreAuthorize("@ss.hasPermi('system:province:export')")
    @Log(title = "省份", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Province province)
    {
        List<Province> list = provinceService.selectProvinceList(province);
        ExcelUtil<Province> util = new ExcelUtil<Province>(Province.class);
        util.exportExcel(response, list, "省份数据");
    }

    /**
     * 获取省份详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:province:query')")
    @GetMapping(value = "/{code}")
    public AjaxResult getInfo(@PathVariable("code") String code)
    {
        return AjaxResult.success(provinceService.selectProvinceByCode(code));
    }

    /**
     * 新增省份
     */
    @PreAuthorize("@ss.hasPermi('system:province:add')")
    @Log(title = "省份", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Province province)
    {
        return toAjax(provinceService.insertProvince(province));
    }

    /**
     * 修改省份
     */
    @PreAuthorize("@ss.hasPermi('system:province:edit')")
    @Log(title = "省份", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Province province)
    {
        return toAjax(provinceService.updateProvince(province));
    }

    /**
     * 删除省份
     */
    @PreAuthorize("@ss.hasPermi('system:province:remove')")
    @Log(title = "省份", businessType = BusinessType.DELETE)
	@DeleteMapping("/{codes}")
    public AjaxResult remove(@PathVariable String[] codes)
    {
        return toAjax(provinceService.deleteProvinceByCodes(codes));
    }
}
