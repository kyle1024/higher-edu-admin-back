package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ProductSku;
import com.ruoyi.system.service.IProductSkuService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品skuController
 *
 * @author ruoyi
 * @date 2022-03-15
 */
//@Api("商品模块")
@RestController
@RequestMapping("/system/sku")
public class ProductSkuController extends BaseController
{
    @Autowired
    private IProductSkuService productSkuService;

    @GetMapping("/page")
    @ApiOperation(value = "查询商品sku列表(分页)", httpMethod = "GET")
    public TableDataInfo page(ProductSku productSku)
    {
        startPage();
        List<ProductSku> list = productSkuService.selectProductSkuList(productSku);
        return getDataTable(list);
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询商品sku列表", httpMethod = "GET")
    public AjaxResult list(ProductSku productSku)
    {
        List<ProductSku> list = productSkuService.selectProductSkuList(productSku);
        return AjaxResult.success(list);
    }
    /**
     * 导出商品sku列表
     */
    @Log(title = "商品sku", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductSku productSku)
    {
        List<ProductSku> list = productSkuService.selectProductSkuList(productSku);
        ExcelUtil<ProductSku> util = new ExcelUtil<ProductSku>(ProductSku.class);
        util.exportExcel(response, list, "商品sku数据");
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取商品sku详细信息", httpMethod = "GET")
    public AjaxResult getInfo(@PathVariable("id") @ApiParam("id") Long id)
    {
        return AjaxResult.success(productSkuService.selectProductSkuById(id));
    }

    /**
     * 新增商品sku
     */
    @PreAuthorize("@ss.hasPermi('system:sku:add')")
    @Log(title = "商品sku", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductSku productSku)
    {
        return toAjax(productSkuService.insertProductSku(productSku));
    }

    /**
     * 修改商品sku
     */
    @Log(title = "商品sku", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductSku productSku)
    {
        return toAjax(productSkuService.updateProductSku(productSku));
    }

    /**
     * 删除商品sku
     */
    @Log(title = "商品sku", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
//        return toAjax(productSkuService.deleteProductSkuByIds(ids));
        return AjaxResult.success();
    }
}
