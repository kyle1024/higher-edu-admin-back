package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Major;
import com.ruoyi.system.service.IMajorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 专业Controller
 *
 * @author ruoyi
 * @date 2022-05-08
 */
@Api("专业模块")
@RestController
@RequestMapping("/system/major")
public class MajorController extends BaseController
{
    @Autowired
    private IMajorService majorService;

    /**
     * 查询专业列表
     */
//    @PreAuthorize("@ss.hasPermi('system:major:list')")
    @ApiOperation("分页查询专业列表")
    @GetMapping("/list")
    public TableDataInfo list(Major major)
    {
        startPage();
        List<Major> list = majorService.selectMajorList(major);
        return getDataTable(list);
    }

    /**
     * 查询专业列表
     */
//    @PreAuthorize("@ss.hasPermi('system:major:list')")
    @ApiOperation("查询专业全部列表")
    @GetMapping("/list/all")
    public AjaxResult listSimpleAll()
    {
        List<Major> list = majorService.selectMajorList(new Major());
        return AjaxResult.success(list);
    }

    /**
     * 导出专业列表
     */
    @PreAuthorize("@ss.hasPermi('system:major:export')")
    @Log(title = "专业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Major major)
    {
        List<Major> list = majorService.selectMajorList(major);
        ExcelUtil<Major> util = new ExcelUtil<Major>(Major.class);
        util.exportExcel(response, list, "专业数据");
    }

    /**
     * 导入专业列表
     */
    @Log(title = "专业", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:major:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Major> util = new ExcelUtil<Major>(Major.class);
        List<Major> majorList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = majorService.importMajor(majorList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    /**
     * 导入专业列表的模板下载
     */
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<Major> util = new ExcelUtil<Major>(Major.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 获取专业详细信息
     */
    @ApiOperation("获取专业详细信息")
    @PreAuthorize("@ss.hasPermi('system:major:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(majorService.selectMajorById(id));
    }

    /**
     * 新增专业
     */
//    @PreAuthorize("@ss.hasPermi('system:major:add')")
    @Log(title = "专业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Major major)
    {
        return toAjax(majorService.insertMajor(major));
    }

    /**
     * 修改专业
     */
    @PreAuthorize("@ss.hasPermi('system:major:edit')")
    @Log(title = "专业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Major major)
    {
        return toAjax(majorService.updateMajor(major));
    }

    /**
     * 删除专业
     */
    @PreAuthorize("@ss.hasPermi('system:major:remove')")
    @Log(title = "专业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(majorService.deleteMajorByIds(ids));
    }
}
