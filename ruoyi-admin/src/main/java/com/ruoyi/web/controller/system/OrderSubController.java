package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.dto.OrderSubDTO;
import com.ruoyi.common.utils.Assert;
import com.ruoyi.system.domain.ProductSku;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.OrderSub;
import com.ruoyi.system.service.IOrderSubService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户订单子Controller
 *
 * @author ruoyi
 * @date 2022-03-18
 */
//@Api("用户子订单")
@RestController
@RequestMapping("/system/order/sub")
public class OrderSubController extends BaseController
{
    @Autowired
    private IOrderSubService orderSubService;

    /**
     * 查询用户订单子列表
     */
    @GetMapping("/page")
    @ApiOperation(value = "转增列表(分页)", httpMethod = "GET")
    public TableDataInfo page(OrderSub orderSub)
    {
        startPage();
        List<ProductSku> list = orderSubService.selectUserOrderSubList(orderSub);
        return getDataTable(list);
    }

    @GetMapping("/list")
    @ApiOperation(value = "转增列表", httpMethod = "GET")
    public AjaxResult list(OrderSub orderSub)
    {
//        startPage();
        List<ProductSku> list = orderSubService.selectUserOrderSubList(orderSub);
        return AjaxResult.success(list);
    }

    /**
     * 导出用户订单子列表
     */
//    @PreAuthorize("@ss.hasPermi('system:sub:export')")
//    @Log(title = "用户订单子", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderSub orderSub)
    {
        List<OrderSub> list = orderSubService.selectOrderSubList(orderSub);
        ExcelUtil<OrderSub> util = new ExcelUtil<OrderSub>(OrderSub.class);
        util.exportExcel(response, list, "用户订单子数据");
    }

    /**
     * 获取用户订单子详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:sub:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(orderSubService.selectOrderSubById(id));
    }

    @PostMapping
    @ApiOperation(value = "转增接口", httpMethod = "POST")
    public AjaxResult add(@Validated @RequestBody OrderSubDTO orderSub)
    {
        Assert.notNull(orderSub.getUserId(), "没有要转赠的用户。");
        orderSubService.tranfer(orderSub);
        return AjaxResult.success();
    }


    @PostMapping("batch")
    @ApiOperation(value = "批量转增接口", httpMethod = "POST")
    public AjaxResult addBatch(@Validated @RequestBody OrderSubDTO orderSub)
    {
        Assert.notEmpty(orderSub.getUserIdList(), "没有要转赠的用户。");
        orderSubService.tranferBatch(orderSub);
        return AjaxResult.success();
    }

    /**
     * 修改用户订单子
     */
//    @PreAuthorize("@ss.hasPermi('system:sub:edit')")
//    @Log(title = "用户订单子", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderSub orderSub)
    {
        return toAjax(orderSubService.updateOrderSub(orderSub));
    }

    /**
     * 删除用户订单子
     */
//    @PreAuthorize("@ss.hasPermi('system:sub:remove')")
//    @Log(title = "用户订单子", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(orderSubService.deleteOrderSubByIds(ids));
    }
}
