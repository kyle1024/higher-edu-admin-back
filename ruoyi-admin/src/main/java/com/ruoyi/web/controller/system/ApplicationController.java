package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.vo.ApplicationVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Application;
import com.ruoyi.system.service.IApplicationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报名信息Controller
 *
 * @author ruoyi
 * @date 2022-05-08
 */
@Api("报名信息Api")
@RestController
@RequestMapping("/system/application")
public class ApplicationController extends BaseController
{
    @Autowired
    private IApplicationService applicationService;

    /**
     * 查询报名信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:application:list')")
    @GetMapping("/list")
    public TableDataInfo list(Application application)
    {
        startPage();
//        List<Application> list = applicationService.selectApplicationList(application);
        List<ApplicationVO> list = applicationService.selectApplicationList2(application);
        return getDataTable(list);
    }

    /**
     * 导出报名信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:application:export')")
    @Log(title = "报名信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Application application)
    {
        List<Application> list = applicationService.selectApplicationList(application);
        ExcelUtil<Application> util = new ExcelUtil<Application>(Application.class);
        util.exportExcel(response, list, "报名信息数据");
    }

    /**
     * 获取报名信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:application:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(applicationService.selectApplicationById(id));
    }

    /**
     * 新增报名信息
     */
//    @PreAuthorize("@ss.hasPermi('system:application:add')")
    @Log(title = "报名信息", businessType = BusinessType.INSERT)
    @ApiOperation("新增报名信息")
    @PostMapping
    public AjaxResult add(@RequestBody Application application)
    {
        return toAjax(applicationService.insertApplication(application));
    }

    /**
     * 修改报名信息
     */
    @PreAuthorize("@ss.hasPermi('system:application:edit')")
    @Log(title = "报名信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Application application)
    {
        return toAjax(applicationService.updateApplication(application));
    }

    /**
     * 删除报名信息
     */
    @PreAuthorize("@ss.hasPermi('system:application:remove')")
    @Log(title = "报名信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(applicationService.deleteApplicationByIds(ids));
    }
}
