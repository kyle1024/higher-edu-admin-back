package com.ruoyi.common.core.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 用户子订单
 *
 * @author WYQuuei
 * @time 2022/3/18 13:29
 */
@Data
@ApiModel("用户登录注册对象")
public class OrderSubDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="用户id", name = "userId")
    private Long userId;

    @ApiModelProperty(value = "用户id列表（批量）", name = "userIdList")
    private List<Long> userIdList;

    @NotNull
    @ApiModelProperty(value = "商品skuId", name = "skuId", required = true)
    private Long skuId;
}
