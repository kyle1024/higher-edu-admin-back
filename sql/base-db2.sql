/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : base-db

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 10/06/2022 21:34:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for application
-- ----------------------------
DROP TABLE IF EXISTS `application`;
CREATE TABLE `application`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `student_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '学生姓名',
  `area_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '区域code',
  `edu_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学历code',
  `major_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '专业code',
  `major_id` bigint NULL DEFAULT NULL COMMENT '专业id',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '报名信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of application
-- ----------------------------
INSERT INTO `application` VALUES (1, '吴谓', '110101', 'zhongzhuan', '2345', NULL, '13145678907', NULL, '2022-05-09 21:31:03', NULL);
INSERT INTO `application` VALUES (2, '张三', '120101', 'dazhuan', '12121', 212, '13105061452', '2022-05-08 15:59:06', '2022-05-09 21:35:46', NULL);
INSERT INTO `application` VALUES (3, '李紫', '110101', 'gaozhong', '12121', NULL, '15656565656', '2022-05-09 19:15:36', NULL, NULL);
INSERT INTO `application` VALUES (4, '王五', '110101', 'gaozhong', '12121', NULL, '15656565656', '2022-05-09 19:18:25', NULL, NULL);
INSERT INTO `application` VALUES (5, '云飞扬', '110101', 'gaozhong', '12121', NULL, '15656565656', '2022-05-09 19:19:02', NULL, NULL);
INSERT INTO `application` VALUES (6, '诸葛楚天', '370304', 'benke', '12121', NULL, '15656565656', '2022-05-09 19:21:52', NULL, NULL);
INSERT INTO `application` VALUES (7, '李四', '110101', 'none', '12121', NULL, '15656565656', '2022-05-09 19:33:07', NULL, NULL);
INSERT INTO `application` VALUES (8, '网易', '110101', 'dazhuan', '12121', NULL, '13100003434', '2022-05-09 19:39:42', NULL, NULL);

-- ----------------------------
-- Table structure for areas
-- ----------------------------
DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas`  (
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区县code',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区县名',
  `city_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市code',
  `province_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份code',
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of areas
-- ----------------------------
INSERT INTO `areas` VALUES ('110101', '东城区', '1101', '11');
INSERT INTO `areas` VALUES ('110102', '西城区', '1101', '11');
INSERT INTO `areas` VALUES ('110105', '朝阳区', '1101', '11');
INSERT INTO `areas` VALUES ('110106', '丰台区', '1101', '11');
INSERT INTO `areas` VALUES ('110107', '石景山区', '1101', '11');
INSERT INTO `areas` VALUES ('110108', '海淀区', '1101', '11');
INSERT INTO `areas` VALUES ('110109', '门头沟区', '1101', '11');
INSERT INTO `areas` VALUES ('110111', '房山区', '1101', '11');
INSERT INTO `areas` VALUES ('110112', '通州区', '1101', '11');
INSERT INTO `areas` VALUES ('110113', '顺义区', '1101', '11');
INSERT INTO `areas` VALUES ('110114', '昌平区', '1101', '11');
INSERT INTO `areas` VALUES ('110115', '大兴区', '1101', '11');
INSERT INTO `areas` VALUES ('110116', '怀柔区', '1101', '11');
INSERT INTO `areas` VALUES ('110117', '平谷区', '1101', '11');
INSERT INTO `areas` VALUES ('110118', '密云区', '1101', '11');
INSERT INTO `areas` VALUES ('110119', '延庆区', '1101', '11');
INSERT INTO `areas` VALUES ('120101', '和平区', '1201', '12');
INSERT INTO `areas` VALUES ('120102', '河东区', '1201', '12');
INSERT INTO `areas` VALUES ('120103', '河西区', '1201', '12');
INSERT INTO `areas` VALUES ('120104', '南开区', '1201', '12');
INSERT INTO `areas` VALUES ('120105', '河北区', '1201', '12');
INSERT INTO `areas` VALUES ('120106', '红桥区', '1201', '12');
INSERT INTO `areas` VALUES ('120110', '东丽区', '1201', '12');
INSERT INTO `areas` VALUES ('120111', '西青区', '1201', '12');
INSERT INTO `areas` VALUES ('120112', '津南区', '1201', '12');
INSERT INTO `areas` VALUES ('120113', '北辰区', '1201', '12');
INSERT INTO `areas` VALUES ('120114', '武清区', '1201', '12');
INSERT INTO `areas` VALUES ('120115', '宝坻区', '1201', '12');
INSERT INTO `areas` VALUES ('120116', '滨海新区', '1201', '12');
INSERT INTO `areas` VALUES ('120117', '宁河区', '1201', '12');
INSERT INTO `areas` VALUES ('120118', '静海区', '1201', '12');
INSERT INTO `areas` VALUES ('120119', '蓟州区', '1201', '12');
INSERT INTO `areas` VALUES ('130102', '长安区', '1301', '13');
INSERT INTO `areas` VALUES ('130104', '桥西区', '1301', '13');
INSERT INTO `areas` VALUES ('130105', '新华区', '1301', '13');
INSERT INTO `areas` VALUES ('130107', '井陉矿区', '1301', '13');
INSERT INTO `areas` VALUES ('130108', '裕华区', '1301', '13');
INSERT INTO `areas` VALUES ('130109', '藁城区', '1301', '13');
INSERT INTO `areas` VALUES ('130110', '鹿泉区', '1301', '13');
INSERT INTO `areas` VALUES ('130111', '栾城区', '1301', '13');
INSERT INTO `areas` VALUES ('130121', '井陉县', '1301', '13');
INSERT INTO `areas` VALUES ('130123', '正定县', '1301', '13');
INSERT INTO `areas` VALUES ('130125', '行唐县', '1301', '13');
INSERT INTO `areas` VALUES ('130126', '灵寿县', '1301', '13');
INSERT INTO `areas` VALUES ('130127', '高邑县', '1301', '13');
INSERT INTO `areas` VALUES ('130128', '深泽县', '1301', '13');
INSERT INTO `areas` VALUES ('130129', '赞皇县', '1301', '13');
INSERT INTO `areas` VALUES ('130130', '无极县', '1301', '13');
INSERT INTO `areas` VALUES ('130131', '平山县', '1301', '13');
INSERT INTO `areas` VALUES ('130132', '元氏县', '1301', '13');
INSERT INTO `areas` VALUES ('130133', '赵县', '1301', '13');
INSERT INTO `areas` VALUES ('130171', '石家庄高新技术产业开发区', '1301', '13');
INSERT INTO `areas` VALUES ('130172', '石家庄循环化工园区', '1301', '13');
INSERT INTO `areas` VALUES ('130181', '辛集市', '1301', '13');
INSERT INTO `areas` VALUES ('130183', '晋州市', '1301', '13');
INSERT INTO `areas` VALUES ('130184', '新乐市', '1301', '13');
INSERT INTO `areas` VALUES ('130202', '路南区', '1302', '13');
INSERT INTO `areas` VALUES ('130203', '路北区', '1302', '13');
INSERT INTO `areas` VALUES ('130204', '古冶区', '1302', '13');
INSERT INTO `areas` VALUES ('130205', '开平区', '1302', '13');
INSERT INTO `areas` VALUES ('130207', '丰南区', '1302', '13');
INSERT INTO `areas` VALUES ('130208', '丰润区', '1302', '13');
INSERT INTO `areas` VALUES ('130209', '曹妃甸区', '1302', '13');
INSERT INTO `areas` VALUES ('130224', '滦南县', '1302', '13');
INSERT INTO `areas` VALUES ('130225', '乐亭县', '1302', '13');
INSERT INTO `areas` VALUES ('130227', '迁西县', '1302', '13');
INSERT INTO `areas` VALUES ('130229', '玉田县', '1302', '13');
INSERT INTO `areas` VALUES ('130271', '河北唐山芦台经济开发区', '1302', '13');
INSERT INTO `areas` VALUES ('130272', '唐山市汉沽管理区', '1302', '13');
INSERT INTO `areas` VALUES ('130273', '唐山高新技术产业开发区', '1302', '13');
INSERT INTO `areas` VALUES ('130274', '河北唐山海港经济开发区', '1302', '13');
INSERT INTO `areas` VALUES ('130281', '遵化市', '1302', '13');
INSERT INTO `areas` VALUES ('130283', '迁安市', '1302', '13');
INSERT INTO `areas` VALUES ('130284', '滦州市', '1302', '13');
INSERT INTO `areas` VALUES ('130302', '海港区', '1303', '13');
INSERT INTO `areas` VALUES ('130303', '山海关区', '1303', '13');
INSERT INTO `areas` VALUES ('130304', '北戴河区', '1303', '13');
INSERT INTO `areas` VALUES ('130306', '抚宁区', '1303', '13');
INSERT INTO `areas` VALUES ('130321', '青龙满族自治县', '1303', '13');
INSERT INTO `areas` VALUES ('130322', '昌黎县', '1303', '13');
INSERT INTO `areas` VALUES ('130324', '卢龙县', '1303', '13');
INSERT INTO `areas` VALUES ('130371', '秦皇岛市经济技术开发区', '1303', '13');
INSERT INTO `areas` VALUES ('130372', '北戴河新区', '1303', '13');
INSERT INTO `areas` VALUES ('130402', '邯山区', '1304', '13');
INSERT INTO `areas` VALUES ('130403', '丛台区', '1304', '13');
INSERT INTO `areas` VALUES ('130404', '复兴区', '1304', '13');
INSERT INTO `areas` VALUES ('130406', '峰峰矿区', '1304', '13');
INSERT INTO `areas` VALUES ('130407', '肥乡区', '1304', '13');
INSERT INTO `areas` VALUES ('130408', '永年区', '1304', '13');
INSERT INTO `areas` VALUES ('130423', '临漳县', '1304', '13');
INSERT INTO `areas` VALUES ('130424', '成安县', '1304', '13');
INSERT INTO `areas` VALUES ('130425', '大名县', '1304', '13');
INSERT INTO `areas` VALUES ('130426', '涉县', '1304', '13');
INSERT INTO `areas` VALUES ('130427', '磁县', '1304', '13');
INSERT INTO `areas` VALUES ('130430', '邱县', '1304', '13');
INSERT INTO `areas` VALUES ('130431', '鸡泽县', '1304', '13');
INSERT INTO `areas` VALUES ('130432', '广平县', '1304', '13');
INSERT INTO `areas` VALUES ('130433', '馆陶县', '1304', '13');
INSERT INTO `areas` VALUES ('130434', '魏县', '1304', '13');
INSERT INTO `areas` VALUES ('130435', '曲周县', '1304', '13');
INSERT INTO `areas` VALUES ('130471', '邯郸经济技术开发区', '1304', '13');
INSERT INTO `areas` VALUES ('130473', '邯郸冀南新区', '1304', '13');
INSERT INTO `areas` VALUES ('130481', '武安市', '1304', '13');
INSERT INTO `areas` VALUES ('130502', '襄都区', '1305', '13');
INSERT INTO `areas` VALUES ('130503', '信都区', '1305', '13');
INSERT INTO `areas` VALUES ('130505', '任泽区', '1305', '13');
INSERT INTO `areas` VALUES ('130506', '南和区', '1305', '13');
INSERT INTO `areas` VALUES ('130522', '临城县', '1305', '13');
INSERT INTO `areas` VALUES ('130523', '内丘县', '1305', '13');
INSERT INTO `areas` VALUES ('130524', '柏乡县', '1305', '13');
INSERT INTO `areas` VALUES ('130525', '隆尧县', '1305', '13');
INSERT INTO `areas` VALUES ('130528', '宁晋县', '1305', '13');
INSERT INTO `areas` VALUES ('130529', '巨鹿县', '1305', '13');
INSERT INTO `areas` VALUES ('130530', '新河县', '1305', '13');
INSERT INTO `areas` VALUES ('130531', '广宗县', '1305', '13');
INSERT INTO `areas` VALUES ('130532', '平乡县', '1305', '13');
INSERT INTO `areas` VALUES ('130533', '威县', '1305', '13');
INSERT INTO `areas` VALUES ('130534', '清河县', '1305', '13');
INSERT INTO `areas` VALUES ('130535', '临西县', '1305', '13');
INSERT INTO `areas` VALUES ('130571', '河北邢台经济开发区', '1305', '13');
INSERT INTO `areas` VALUES ('130581', '南宫市', '1305', '13');
INSERT INTO `areas` VALUES ('130582', '沙河市', '1305', '13');
INSERT INTO `areas` VALUES ('130602', '竞秀区', '1306', '13');
INSERT INTO `areas` VALUES ('130606', '莲池区', '1306', '13');
INSERT INTO `areas` VALUES ('130607', '满城区', '1306', '13');
INSERT INTO `areas` VALUES ('130608', '清苑区', '1306', '13');
INSERT INTO `areas` VALUES ('130609', '徐水区', '1306', '13');
INSERT INTO `areas` VALUES ('130623', '涞水县', '1306', '13');
INSERT INTO `areas` VALUES ('130624', '阜平县', '1306', '13');
INSERT INTO `areas` VALUES ('130626', '定兴县', '1306', '13');
INSERT INTO `areas` VALUES ('130627', '唐县', '1306', '13');
INSERT INTO `areas` VALUES ('130628', '高阳县', '1306', '13');
INSERT INTO `areas` VALUES ('130629', '容城县', '1306', '13');
INSERT INTO `areas` VALUES ('130630', '涞源县', '1306', '13');
INSERT INTO `areas` VALUES ('130631', '望都县', '1306', '13');
INSERT INTO `areas` VALUES ('130632', '安新县', '1306', '13');
INSERT INTO `areas` VALUES ('130633', '易县', '1306', '13');
INSERT INTO `areas` VALUES ('130634', '曲阳县', '1306', '13');
INSERT INTO `areas` VALUES ('130635', '蠡县', '1306', '13');
INSERT INTO `areas` VALUES ('130636', '顺平县', '1306', '13');
INSERT INTO `areas` VALUES ('130637', '博野县', '1306', '13');
INSERT INTO `areas` VALUES ('130638', '雄县', '1306', '13');
INSERT INTO `areas` VALUES ('130671', '保定高新技术产业开发区', '1306', '13');
INSERT INTO `areas` VALUES ('130672', '保定白沟新城', '1306', '13');
INSERT INTO `areas` VALUES ('130681', '涿州市', '1306', '13');
INSERT INTO `areas` VALUES ('130682', '定州市', '1306', '13');
INSERT INTO `areas` VALUES ('130683', '安国市', '1306', '13');
INSERT INTO `areas` VALUES ('130684', '高碑店市', '1306', '13');
INSERT INTO `areas` VALUES ('130702', '桥东区', '1307', '13');
INSERT INTO `areas` VALUES ('130703', '桥西区', '1307', '13');
INSERT INTO `areas` VALUES ('130705', '宣化区', '1307', '13');
INSERT INTO `areas` VALUES ('130706', '下花园区', '1307', '13');
INSERT INTO `areas` VALUES ('130708', '万全区', '1307', '13');
INSERT INTO `areas` VALUES ('130709', '崇礼区', '1307', '13');
INSERT INTO `areas` VALUES ('130722', '张北县', '1307', '13');
INSERT INTO `areas` VALUES ('130723', '康保县', '1307', '13');
INSERT INTO `areas` VALUES ('130724', '沽源县', '1307', '13');
INSERT INTO `areas` VALUES ('130725', '尚义县', '1307', '13');
INSERT INTO `areas` VALUES ('130726', '蔚县', '1307', '13');
INSERT INTO `areas` VALUES ('130727', '阳原县', '1307', '13');
INSERT INTO `areas` VALUES ('130728', '怀安县', '1307', '13');
INSERT INTO `areas` VALUES ('130730', '怀来县', '1307', '13');
INSERT INTO `areas` VALUES ('130731', '涿鹿县', '1307', '13');
INSERT INTO `areas` VALUES ('130732', '赤城县', '1307', '13');
INSERT INTO `areas` VALUES ('130771', '张家口经济开发区', '1307', '13');
INSERT INTO `areas` VALUES ('130772', '张家口市察北管理区', '1307', '13');
INSERT INTO `areas` VALUES ('130773', '张家口市塞北管理区', '1307', '13');
INSERT INTO `areas` VALUES ('130802', '双桥区', '1308', '13');
INSERT INTO `areas` VALUES ('130803', '双滦区', '1308', '13');
INSERT INTO `areas` VALUES ('130804', '鹰手营子矿区', '1308', '13');
INSERT INTO `areas` VALUES ('130821', '承德县', '1308', '13');
INSERT INTO `areas` VALUES ('130822', '兴隆县', '1308', '13');
INSERT INTO `areas` VALUES ('130824', '滦平县', '1308', '13');
INSERT INTO `areas` VALUES ('130825', '隆化县', '1308', '13');
INSERT INTO `areas` VALUES ('130826', '丰宁满族自治县', '1308', '13');
INSERT INTO `areas` VALUES ('130827', '宽城满族自治县', '1308', '13');
INSERT INTO `areas` VALUES ('130828', '围场满族蒙古族自治县', '1308', '13');
INSERT INTO `areas` VALUES ('130871', '承德高新技术产业开发区', '1308', '13');
INSERT INTO `areas` VALUES ('130881', '平泉市', '1308', '13');
INSERT INTO `areas` VALUES ('130902', '新华区', '1309', '13');
INSERT INTO `areas` VALUES ('130903', '运河区', '1309', '13');
INSERT INTO `areas` VALUES ('130921', '沧县', '1309', '13');
INSERT INTO `areas` VALUES ('130922', '青县', '1309', '13');
INSERT INTO `areas` VALUES ('130923', '东光县', '1309', '13');
INSERT INTO `areas` VALUES ('130924', '海兴县', '1309', '13');
INSERT INTO `areas` VALUES ('130925', '盐山县', '1309', '13');
INSERT INTO `areas` VALUES ('130926', '肃宁县', '1309', '13');
INSERT INTO `areas` VALUES ('130927', '南皮县', '1309', '13');
INSERT INTO `areas` VALUES ('130928', '吴桥县', '1309', '13');
INSERT INTO `areas` VALUES ('130929', '献县', '1309', '13');
INSERT INTO `areas` VALUES ('130930', '孟村回族自治县', '1309', '13');
INSERT INTO `areas` VALUES ('130971', '河北沧州经济开发区', '1309', '13');
INSERT INTO `areas` VALUES ('130972', '沧州高新技术产业开发区', '1309', '13');
INSERT INTO `areas` VALUES ('130973', '沧州渤海新区', '1309', '13');
INSERT INTO `areas` VALUES ('130981', '泊头市', '1309', '13');
INSERT INTO `areas` VALUES ('130982', '任丘市', '1309', '13');
INSERT INTO `areas` VALUES ('130983', '黄骅市', '1309', '13');
INSERT INTO `areas` VALUES ('130984', '河间市', '1309', '13');
INSERT INTO `areas` VALUES ('131002', '安次区', '1310', '13');
INSERT INTO `areas` VALUES ('131003', '广阳区', '1310', '13');
INSERT INTO `areas` VALUES ('131022', '固安县', '1310', '13');
INSERT INTO `areas` VALUES ('131023', '永清县', '1310', '13');
INSERT INTO `areas` VALUES ('131024', '香河县', '1310', '13');
INSERT INTO `areas` VALUES ('131025', '大城县', '1310', '13');
INSERT INTO `areas` VALUES ('131026', '文安县', '1310', '13');
INSERT INTO `areas` VALUES ('131028', '大厂回族自治县', '1310', '13');
INSERT INTO `areas` VALUES ('131071', '廊坊经济技术开发区', '1310', '13');
INSERT INTO `areas` VALUES ('131081', '霸州市', '1310', '13');
INSERT INTO `areas` VALUES ('131082', '三河市', '1310', '13');
INSERT INTO `areas` VALUES ('131102', '桃城区', '1311', '13');
INSERT INTO `areas` VALUES ('131103', '冀州区', '1311', '13');
INSERT INTO `areas` VALUES ('131121', '枣强县', '1311', '13');
INSERT INTO `areas` VALUES ('131122', '武邑县', '1311', '13');
INSERT INTO `areas` VALUES ('131123', '武强县', '1311', '13');
INSERT INTO `areas` VALUES ('131124', '饶阳县', '1311', '13');
INSERT INTO `areas` VALUES ('131125', '安平县', '1311', '13');
INSERT INTO `areas` VALUES ('131126', '故城县', '1311', '13');
INSERT INTO `areas` VALUES ('131127', '景县', '1311', '13');
INSERT INTO `areas` VALUES ('131128', '阜城县', '1311', '13');
INSERT INTO `areas` VALUES ('131171', '河北衡水高新技术产业开发区', '1311', '13');
INSERT INTO `areas` VALUES ('131172', '衡水滨湖新区', '1311', '13');
INSERT INTO `areas` VALUES ('131182', '深州市', '1311', '13');
INSERT INTO `areas` VALUES ('140105', '小店区', '1401', '14');
INSERT INTO `areas` VALUES ('140106', '迎泽区', '1401', '14');
INSERT INTO `areas` VALUES ('140107', '杏花岭区', '1401', '14');
INSERT INTO `areas` VALUES ('140108', '尖草坪区', '1401', '14');
INSERT INTO `areas` VALUES ('140109', '万柏林区', '1401', '14');
INSERT INTO `areas` VALUES ('140110', '晋源区', '1401', '14');
INSERT INTO `areas` VALUES ('140121', '清徐县', '1401', '14');
INSERT INTO `areas` VALUES ('140122', '阳曲县', '1401', '14');
INSERT INTO `areas` VALUES ('140123', '娄烦县', '1401', '14');
INSERT INTO `areas` VALUES ('140171', '山西转型综合改革示范区', '1401', '14');
INSERT INTO `areas` VALUES ('140181', '古交市', '1401', '14');
INSERT INTO `areas` VALUES ('140212', '新荣区', '1402', '14');
INSERT INTO `areas` VALUES ('140213', '平城区', '1402', '14');
INSERT INTO `areas` VALUES ('140214', '云冈区', '1402', '14');
INSERT INTO `areas` VALUES ('140215', '云州区', '1402', '14');
INSERT INTO `areas` VALUES ('140221', '阳高县', '1402', '14');
INSERT INTO `areas` VALUES ('140222', '天镇县', '1402', '14');
INSERT INTO `areas` VALUES ('140223', '广灵县', '1402', '14');
INSERT INTO `areas` VALUES ('140224', '灵丘县', '1402', '14');
INSERT INTO `areas` VALUES ('140225', '浑源县', '1402', '14');
INSERT INTO `areas` VALUES ('140226', '左云县', '1402', '14');
INSERT INTO `areas` VALUES ('140271', '山西大同经济开发区', '1402', '14');
INSERT INTO `areas` VALUES ('140302', '城区', '1403', '14');
INSERT INTO `areas` VALUES ('140303', '矿区', '1403', '14');
INSERT INTO `areas` VALUES ('140311', '郊区', '1403', '14');
INSERT INTO `areas` VALUES ('140321', '平定县', '1403', '14');
INSERT INTO `areas` VALUES ('140322', '盂县', '1403', '14');
INSERT INTO `areas` VALUES ('140403', '潞州区', '1404', '14');
INSERT INTO `areas` VALUES ('140404', '上党区', '1404', '14');
INSERT INTO `areas` VALUES ('140405', '屯留区', '1404', '14');
INSERT INTO `areas` VALUES ('140406', '潞城区', '1404', '14');
INSERT INTO `areas` VALUES ('140423', '襄垣县', '1404', '14');
INSERT INTO `areas` VALUES ('140425', '平顺县', '1404', '14');
INSERT INTO `areas` VALUES ('140426', '黎城县', '1404', '14');
INSERT INTO `areas` VALUES ('140427', '壶关县', '1404', '14');
INSERT INTO `areas` VALUES ('140428', '长子县', '1404', '14');
INSERT INTO `areas` VALUES ('140429', '武乡县', '1404', '14');
INSERT INTO `areas` VALUES ('140430', '沁县', '1404', '14');
INSERT INTO `areas` VALUES ('140431', '沁源县', '1404', '14');
INSERT INTO `areas` VALUES ('140471', '山西长治高新技术产业园区', '1404', '14');
INSERT INTO `areas` VALUES ('140502', '城区', '1405', '14');
INSERT INTO `areas` VALUES ('140521', '沁水县', '1405', '14');
INSERT INTO `areas` VALUES ('140522', '阳城县', '1405', '14');
INSERT INTO `areas` VALUES ('140524', '陵川县', '1405', '14');
INSERT INTO `areas` VALUES ('140525', '泽州县', '1405', '14');
INSERT INTO `areas` VALUES ('140581', '高平市', '1405', '14');
INSERT INTO `areas` VALUES ('140602', '朔城区', '1406', '14');
INSERT INTO `areas` VALUES ('140603', '平鲁区', '1406', '14');
INSERT INTO `areas` VALUES ('140621', '山阴县', '1406', '14');
INSERT INTO `areas` VALUES ('140622', '应县', '1406', '14');
INSERT INTO `areas` VALUES ('140623', '右玉县', '1406', '14');
INSERT INTO `areas` VALUES ('140671', '山西朔州经济开发区', '1406', '14');
INSERT INTO `areas` VALUES ('140681', '怀仁市', '1406', '14');
INSERT INTO `areas` VALUES ('140702', '榆次区', '1407', '14');
INSERT INTO `areas` VALUES ('140703', '太谷区', '1407', '14');
INSERT INTO `areas` VALUES ('140721', '榆社县', '1407', '14');
INSERT INTO `areas` VALUES ('140722', '左权县', '1407', '14');
INSERT INTO `areas` VALUES ('140723', '和顺县', '1407', '14');
INSERT INTO `areas` VALUES ('140724', '昔阳县', '1407', '14');
INSERT INTO `areas` VALUES ('140725', '寿阳县', '1407', '14');
INSERT INTO `areas` VALUES ('140727', '祁县', '1407', '14');
INSERT INTO `areas` VALUES ('140728', '平遥县', '1407', '14');
INSERT INTO `areas` VALUES ('140729', '灵石县', '1407', '14');
INSERT INTO `areas` VALUES ('140781', '介休市', '1407', '14');
INSERT INTO `areas` VALUES ('140802', '盐湖区', '1408', '14');
INSERT INTO `areas` VALUES ('140821', '临猗县', '1408', '14');
INSERT INTO `areas` VALUES ('140822', '万荣县', '1408', '14');
INSERT INTO `areas` VALUES ('140823', '闻喜县', '1408', '14');
INSERT INTO `areas` VALUES ('140824', '稷山县', '1408', '14');
INSERT INTO `areas` VALUES ('140825', '新绛县', '1408', '14');
INSERT INTO `areas` VALUES ('140826', '绛县', '1408', '14');
INSERT INTO `areas` VALUES ('140827', '垣曲县', '1408', '14');
INSERT INTO `areas` VALUES ('140828', '夏县', '1408', '14');
INSERT INTO `areas` VALUES ('140829', '平陆县', '1408', '14');
INSERT INTO `areas` VALUES ('140830', '芮城县', '1408', '14');
INSERT INTO `areas` VALUES ('140881', '永济市', '1408', '14');
INSERT INTO `areas` VALUES ('140882', '河津市', '1408', '14');
INSERT INTO `areas` VALUES ('140902', '忻府区', '1409', '14');
INSERT INTO `areas` VALUES ('140921', '定襄县', '1409', '14');
INSERT INTO `areas` VALUES ('140922', '五台县', '1409', '14');
INSERT INTO `areas` VALUES ('140923', '代县', '1409', '14');
INSERT INTO `areas` VALUES ('140924', '繁峙县', '1409', '14');
INSERT INTO `areas` VALUES ('140925', '宁武县', '1409', '14');
INSERT INTO `areas` VALUES ('140926', '静乐县', '1409', '14');
INSERT INTO `areas` VALUES ('140927', '神池县', '1409', '14');
INSERT INTO `areas` VALUES ('140928', '五寨县', '1409', '14');
INSERT INTO `areas` VALUES ('140929', '岢岚县', '1409', '14');
INSERT INTO `areas` VALUES ('140930', '河曲县', '1409', '14');
INSERT INTO `areas` VALUES ('140931', '保德县', '1409', '14');
INSERT INTO `areas` VALUES ('140932', '偏关县', '1409', '14');
INSERT INTO `areas` VALUES ('140971', '五台山风景名胜区', '1409', '14');
INSERT INTO `areas` VALUES ('140981', '原平市', '1409', '14');
INSERT INTO `areas` VALUES ('141002', '尧都区', '1410', '14');
INSERT INTO `areas` VALUES ('141021', '曲沃县', '1410', '14');
INSERT INTO `areas` VALUES ('141022', '翼城县', '1410', '14');
INSERT INTO `areas` VALUES ('141023', '襄汾县', '1410', '14');
INSERT INTO `areas` VALUES ('141024', '洪洞县', '1410', '14');
INSERT INTO `areas` VALUES ('141025', '古县', '1410', '14');
INSERT INTO `areas` VALUES ('141026', '安泽县', '1410', '14');
INSERT INTO `areas` VALUES ('141027', '浮山县', '1410', '14');
INSERT INTO `areas` VALUES ('141028', '吉县', '1410', '14');
INSERT INTO `areas` VALUES ('141029', '乡宁县', '1410', '14');
INSERT INTO `areas` VALUES ('141030', '大宁县', '1410', '14');
INSERT INTO `areas` VALUES ('141031', '隰县', '1410', '14');
INSERT INTO `areas` VALUES ('141032', '永和县', '1410', '14');
INSERT INTO `areas` VALUES ('141033', '蒲县', '1410', '14');
INSERT INTO `areas` VALUES ('141034', '汾西县', '1410', '14');
INSERT INTO `areas` VALUES ('141081', '侯马市', '1410', '14');
INSERT INTO `areas` VALUES ('141082', '霍州市', '1410', '14');
INSERT INTO `areas` VALUES ('141102', '离石区', '1411', '14');
INSERT INTO `areas` VALUES ('141121', '文水县', '1411', '14');
INSERT INTO `areas` VALUES ('141122', '交城县', '1411', '14');
INSERT INTO `areas` VALUES ('141123', '兴县', '1411', '14');
INSERT INTO `areas` VALUES ('141124', '临县', '1411', '14');
INSERT INTO `areas` VALUES ('141125', '柳林县', '1411', '14');
INSERT INTO `areas` VALUES ('141126', '石楼县', '1411', '14');
INSERT INTO `areas` VALUES ('141127', '岚县', '1411', '14');
INSERT INTO `areas` VALUES ('141128', '方山县', '1411', '14');
INSERT INTO `areas` VALUES ('141129', '中阳县', '1411', '14');
INSERT INTO `areas` VALUES ('141130', '交口县', '1411', '14');
INSERT INTO `areas` VALUES ('141181', '孝义市', '1411', '14');
INSERT INTO `areas` VALUES ('141182', '汾阳市', '1411', '14');
INSERT INTO `areas` VALUES ('150102', '新城区', '1501', '15');
INSERT INTO `areas` VALUES ('150103', '回民区', '1501', '15');
INSERT INTO `areas` VALUES ('150104', '玉泉区', '1501', '15');
INSERT INTO `areas` VALUES ('150105', '赛罕区', '1501', '15');
INSERT INTO `areas` VALUES ('150121', '土默特左旗', '1501', '15');
INSERT INTO `areas` VALUES ('150122', '托克托县', '1501', '15');
INSERT INTO `areas` VALUES ('150123', '和林格尔县', '1501', '15');
INSERT INTO `areas` VALUES ('150124', '清水河县', '1501', '15');
INSERT INTO `areas` VALUES ('150125', '武川县', '1501', '15');
INSERT INTO `areas` VALUES ('150172', '呼和浩特经济技术开发区', '1501', '15');
INSERT INTO `areas` VALUES ('150202', '东河区', '1502', '15');
INSERT INTO `areas` VALUES ('150203', '昆都仑区', '1502', '15');
INSERT INTO `areas` VALUES ('150204', '青山区', '1502', '15');
INSERT INTO `areas` VALUES ('150205', '石拐区', '1502', '15');
INSERT INTO `areas` VALUES ('150206', '白云鄂博矿区', '1502', '15');
INSERT INTO `areas` VALUES ('150207', '九原区', '1502', '15');
INSERT INTO `areas` VALUES ('150221', '土默特右旗', '1502', '15');
INSERT INTO `areas` VALUES ('150222', '固阳县', '1502', '15');
INSERT INTO `areas` VALUES ('150223', '达尔罕茂明安联合旗', '1502', '15');
INSERT INTO `areas` VALUES ('150271', '包头稀土高新技术产业开发区', '1502', '15');
INSERT INTO `areas` VALUES ('150302', '海勃湾区', '1503', '15');
INSERT INTO `areas` VALUES ('150303', '海南区', '1503', '15');
INSERT INTO `areas` VALUES ('150304', '乌达区', '1503', '15');
INSERT INTO `areas` VALUES ('150402', '红山区', '1504', '15');
INSERT INTO `areas` VALUES ('150403', '元宝山区', '1504', '15');
INSERT INTO `areas` VALUES ('150404', '松山区', '1504', '15');
INSERT INTO `areas` VALUES ('150421', '阿鲁科尔沁旗', '1504', '15');
INSERT INTO `areas` VALUES ('150422', '巴林左旗', '1504', '15');
INSERT INTO `areas` VALUES ('150423', '巴林右旗', '1504', '15');
INSERT INTO `areas` VALUES ('150424', '林西县', '1504', '15');
INSERT INTO `areas` VALUES ('150425', '克什克腾旗', '1504', '15');
INSERT INTO `areas` VALUES ('150426', '翁牛特旗', '1504', '15');
INSERT INTO `areas` VALUES ('150428', '喀喇沁旗', '1504', '15');
INSERT INTO `areas` VALUES ('150429', '宁城县', '1504', '15');
INSERT INTO `areas` VALUES ('150430', '敖汉旗', '1504', '15');
INSERT INTO `areas` VALUES ('150502', '科尔沁区', '1505', '15');
INSERT INTO `areas` VALUES ('150521', '科尔沁左翼中旗', '1505', '15');
INSERT INTO `areas` VALUES ('150522', '科尔沁左翼后旗', '1505', '15');
INSERT INTO `areas` VALUES ('150523', '开鲁县', '1505', '15');
INSERT INTO `areas` VALUES ('150524', '库伦旗', '1505', '15');
INSERT INTO `areas` VALUES ('150525', '奈曼旗', '1505', '15');
INSERT INTO `areas` VALUES ('150526', '扎鲁特旗', '1505', '15');
INSERT INTO `areas` VALUES ('150571', '通辽经济技术开发区', '1505', '15');
INSERT INTO `areas` VALUES ('150581', '霍林郭勒市', '1505', '15');
INSERT INTO `areas` VALUES ('150602', '东胜区', '1506', '15');
INSERT INTO `areas` VALUES ('150603', '康巴什区', '1506', '15');
INSERT INTO `areas` VALUES ('150621', '达拉特旗', '1506', '15');
INSERT INTO `areas` VALUES ('150622', '准格尔旗', '1506', '15');
INSERT INTO `areas` VALUES ('150623', '鄂托克前旗', '1506', '15');
INSERT INTO `areas` VALUES ('150624', '鄂托克旗', '1506', '15');
INSERT INTO `areas` VALUES ('150625', '杭锦旗', '1506', '15');
INSERT INTO `areas` VALUES ('150626', '乌审旗', '1506', '15');
INSERT INTO `areas` VALUES ('150627', '伊金霍洛旗', '1506', '15');
INSERT INTO `areas` VALUES ('150702', '海拉尔区', '1507', '15');
INSERT INTO `areas` VALUES ('150703', '扎赉诺尔区', '1507', '15');
INSERT INTO `areas` VALUES ('150721', '阿荣旗', '1507', '15');
INSERT INTO `areas` VALUES ('150722', '莫力达瓦达斡尔族自治旗', '1507', '15');
INSERT INTO `areas` VALUES ('150723', '鄂伦春自治旗', '1507', '15');
INSERT INTO `areas` VALUES ('150724', '鄂温克族自治旗', '1507', '15');
INSERT INTO `areas` VALUES ('150725', '陈巴尔虎旗', '1507', '15');
INSERT INTO `areas` VALUES ('150726', '新巴尔虎左旗', '1507', '15');
INSERT INTO `areas` VALUES ('150727', '新巴尔虎右旗', '1507', '15');
INSERT INTO `areas` VALUES ('150781', '满洲里市', '1507', '15');
INSERT INTO `areas` VALUES ('150782', '牙克石市', '1507', '15');
INSERT INTO `areas` VALUES ('150783', '扎兰屯市', '1507', '15');
INSERT INTO `areas` VALUES ('150784', '额尔古纳市', '1507', '15');
INSERT INTO `areas` VALUES ('150785', '根河市', '1507', '15');
INSERT INTO `areas` VALUES ('150802', '临河区', '1508', '15');
INSERT INTO `areas` VALUES ('150821', '五原县', '1508', '15');
INSERT INTO `areas` VALUES ('150822', '磴口县', '1508', '15');
INSERT INTO `areas` VALUES ('150823', '乌拉特前旗', '1508', '15');
INSERT INTO `areas` VALUES ('150824', '乌拉特中旗', '1508', '15');
INSERT INTO `areas` VALUES ('150825', '乌拉特后旗', '1508', '15');
INSERT INTO `areas` VALUES ('150826', '杭锦后旗', '1508', '15');
INSERT INTO `areas` VALUES ('150902', '集宁区', '1509', '15');
INSERT INTO `areas` VALUES ('150921', '卓资县', '1509', '15');
INSERT INTO `areas` VALUES ('150922', '化德县', '1509', '15');
INSERT INTO `areas` VALUES ('150923', '商都县', '1509', '15');
INSERT INTO `areas` VALUES ('150924', '兴和县', '1509', '15');
INSERT INTO `areas` VALUES ('150925', '凉城县', '1509', '15');
INSERT INTO `areas` VALUES ('150926', '察哈尔右翼前旗', '1509', '15');
INSERT INTO `areas` VALUES ('150927', '察哈尔右翼中旗', '1509', '15');
INSERT INTO `areas` VALUES ('150928', '察哈尔右翼后旗', '1509', '15');
INSERT INTO `areas` VALUES ('150929', '四子王旗', '1509', '15');
INSERT INTO `areas` VALUES ('150981', '丰镇市', '1509', '15');
INSERT INTO `areas` VALUES ('152201', '乌兰浩特市', '1522', '15');
INSERT INTO `areas` VALUES ('152202', '阿尔山市', '1522', '15');
INSERT INTO `areas` VALUES ('152221', '科尔沁右翼前旗', '1522', '15');
INSERT INTO `areas` VALUES ('152222', '科尔沁右翼中旗', '1522', '15');
INSERT INTO `areas` VALUES ('152223', '扎赉特旗', '1522', '15');
INSERT INTO `areas` VALUES ('152224', '突泉县', '1522', '15');
INSERT INTO `areas` VALUES ('152501', '二连浩特市', '1525', '15');
INSERT INTO `areas` VALUES ('152502', '锡林浩特市', '1525', '15');
INSERT INTO `areas` VALUES ('152522', '阿巴嘎旗', '1525', '15');
INSERT INTO `areas` VALUES ('152523', '苏尼特左旗', '1525', '15');
INSERT INTO `areas` VALUES ('152524', '苏尼特右旗', '1525', '15');
INSERT INTO `areas` VALUES ('152525', '东乌珠穆沁旗', '1525', '15');
INSERT INTO `areas` VALUES ('152526', '西乌珠穆沁旗', '1525', '15');
INSERT INTO `areas` VALUES ('152527', '太仆寺旗', '1525', '15');
INSERT INTO `areas` VALUES ('152528', '镶黄旗', '1525', '15');
INSERT INTO `areas` VALUES ('152529', '正镶白旗', '1525', '15');
INSERT INTO `areas` VALUES ('152530', '正蓝旗', '1525', '15');
INSERT INTO `areas` VALUES ('152531', '多伦县', '1525', '15');
INSERT INTO `areas` VALUES ('152571', '乌拉盖管委会', '1525', '15');
INSERT INTO `areas` VALUES ('152921', '阿拉善左旗', '1529', '15');
INSERT INTO `areas` VALUES ('152922', '阿拉善右旗', '1529', '15');
INSERT INTO `areas` VALUES ('152923', '额济纳旗', '1529', '15');
INSERT INTO `areas` VALUES ('152971', '内蒙古阿拉善高新技术产业开发区', '1529', '15');
INSERT INTO `areas` VALUES ('210102', '和平区', '2101', '21');
INSERT INTO `areas` VALUES ('210103', '沈河区', '2101', '21');
INSERT INTO `areas` VALUES ('210104', '大东区', '2101', '21');
INSERT INTO `areas` VALUES ('210105', '皇姑区', '2101', '21');
INSERT INTO `areas` VALUES ('210106', '铁西区', '2101', '21');
INSERT INTO `areas` VALUES ('210111', '苏家屯区', '2101', '21');
INSERT INTO `areas` VALUES ('210112', '浑南区', '2101', '21');
INSERT INTO `areas` VALUES ('210113', '沈北新区', '2101', '21');
INSERT INTO `areas` VALUES ('210114', '于洪区', '2101', '21');
INSERT INTO `areas` VALUES ('210115', '辽中区', '2101', '21');
INSERT INTO `areas` VALUES ('210123', '康平县', '2101', '21');
INSERT INTO `areas` VALUES ('210124', '法库县', '2101', '21');
INSERT INTO `areas` VALUES ('210181', '新民市', '2101', '21');
INSERT INTO `areas` VALUES ('210202', '中山区', '2102', '21');
INSERT INTO `areas` VALUES ('210203', '西岗区', '2102', '21');
INSERT INTO `areas` VALUES ('210204', '沙河口区', '2102', '21');
INSERT INTO `areas` VALUES ('210211', '甘井子区', '2102', '21');
INSERT INTO `areas` VALUES ('210212', '旅顺口区', '2102', '21');
INSERT INTO `areas` VALUES ('210213', '金州区', '2102', '21');
INSERT INTO `areas` VALUES ('210214', '普兰店区', '2102', '21');
INSERT INTO `areas` VALUES ('210224', '长海县', '2102', '21');
INSERT INTO `areas` VALUES ('210281', '瓦房店市', '2102', '21');
INSERT INTO `areas` VALUES ('210283', '庄河市', '2102', '21');
INSERT INTO `areas` VALUES ('210302', '铁东区', '2103', '21');
INSERT INTO `areas` VALUES ('210303', '铁西区', '2103', '21');
INSERT INTO `areas` VALUES ('210304', '立山区', '2103', '21');
INSERT INTO `areas` VALUES ('210311', '千山区', '2103', '21');
INSERT INTO `areas` VALUES ('210321', '台安县', '2103', '21');
INSERT INTO `areas` VALUES ('210323', '岫岩满族自治县', '2103', '21');
INSERT INTO `areas` VALUES ('210381', '海城市', '2103', '21');
INSERT INTO `areas` VALUES ('210402', '新抚区', '2104', '21');
INSERT INTO `areas` VALUES ('210403', '东洲区', '2104', '21');
INSERT INTO `areas` VALUES ('210404', '望花区', '2104', '21');
INSERT INTO `areas` VALUES ('210411', '顺城区', '2104', '21');
INSERT INTO `areas` VALUES ('210421', '抚顺县', '2104', '21');
INSERT INTO `areas` VALUES ('210422', '新宾满族自治县', '2104', '21');
INSERT INTO `areas` VALUES ('210423', '清原满族自治县', '2104', '21');
INSERT INTO `areas` VALUES ('210502', '平山区', '2105', '21');
INSERT INTO `areas` VALUES ('210503', '溪湖区', '2105', '21');
INSERT INTO `areas` VALUES ('210504', '明山区', '2105', '21');
INSERT INTO `areas` VALUES ('210505', '南芬区', '2105', '21');
INSERT INTO `areas` VALUES ('210521', '本溪满族自治县', '2105', '21');
INSERT INTO `areas` VALUES ('210522', '桓仁满族自治县', '2105', '21');
INSERT INTO `areas` VALUES ('210602', '元宝区', '2106', '21');
INSERT INTO `areas` VALUES ('210603', '振兴区', '2106', '21');
INSERT INTO `areas` VALUES ('210604', '振安区', '2106', '21');
INSERT INTO `areas` VALUES ('210624', '宽甸满族自治县', '2106', '21');
INSERT INTO `areas` VALUES ('210681', '东港市', '2106', '21');
INSERT INTO `areas` VALUES ('210682', '凤城市', '2106', '21');
INSERT INTO `areas` VALUES ('210702', '古塔区', '2107', '21');
INSERT INTO `areas` VALUES ('210703', '凌河区', '2107', '21');
INSERT INTO `areas` VALUES ('210711', '太和区', '2107', '21');
INSERT INTO `areas` VALUES ('210726', '黑山县', '2107', '21');
INSERT INTO `areas` VALUES ('210727', '义县', '2107', '21');
INSERT INTO `areas` VALUES ('210781', '凌海市', '2107', '21');
INSERT INTO `areas` VALUES ('210782', '北镇市', '2107', '21');
INSERT INTO `areas` VALUES ('210802', '站前区', '2108', '21');
INSERT INTO `areas` VALUES ('210803', '西市区', '2108', '21');
INSERT INTO `areas` VALUES ('210804', '鲅鱼圈区', '2108', '21');
INSERT INTO `areas` VALUES ('210811', '老边区', '2108', '21');
INSERT INTO `areas` VALUES ('210881', '盖州市', '2108', '21');
INSERT INTO `areas` VALUES ('210882', '大石桥市', '2108', '21');
INSERT INTO `areas` VALUES ('210902', '海州区', '2109', '21');
INSERT INTO `areas` VALUES ('210903', '新邱区', '2109', '21');
INSERT INTO `areas` VALUES ('210904', '太平区', '2109', '21');
INSERT INTO `areas` VALUES ('210905', '清河门区', '2109', '21');
INSERT INTO `areas` VALUES ('210911', '细河区', '2109', '21');
INSERT INTO `areas` VALUES ('210921', '阜新蒙古族自治县', '2109', '21');
INSERT INTO `areas` VALUES ('210922', '彰武县', '2109', '21');
INSERT INTO `areas` VALUES ('211002', '白塔区', '2110', '21');
INSERT INTO `areas` VALUES ('211003', '文圣区', '2110', '21');
INSERT INTO `areas` VALUES ('211004', '宏伟区', '2110', '21');
INSERT INTO `areas` VALUES ('211005', '弓长岭区', '2110', '21');
INSERT INTO `areas` VALUES ('211011', '太子河区', '2110', '21');
INSERT INTO `areas` VALUES ('211021', '辽阳县', '2110', '21');
INSERT INTO `areas` VALUES ('211081', '灯塔市', '2110', '21');
INSERT INTO `areas` VALUES ('211102', '双台子区', '2111', '21');
INSERT INTO `areas` VALUES ('211103', '兴隆台区', '2111', '21');
INSERT INTO `areas` VALUES ('211104', '大洼区', '2111', '21');
INSERT INTO `areas` VALUES ('211122', '盘山县', '2111', '21');
INSERT INTO `areas` VALUES ('211202', '银州区', '2112', '21');
INSERT INTO `areas` VALUES ('211204', '清河区', '2112', '21');
INSERT INTO `areas` VALUES ('211221', '铁岭县', '2112', '21');
INSERT INTO `areas` VALUES ('211223', '西丰县', '2112', '21');
INSERT INTO `areas` VALUES ('211224', '昌图县', '2112', '21');
INSERT INTO `areas` VALUES ('211281', '调兵山市', '2112', '21');
INSERT INTO `areas` VALUES ('211282', '开原市', '2112', '21');
INSERT INTO `areas` VALUES ('211302', '双塔区', '2113', '21');
INSERT INTO `areas` VALUES ('211303', '龙城区', '2113', '21');
INSERT INTO `areas` VALUES ('211321', '朝阳县', '2113', '21');
INSERT INTO `areas` VALUES ('211322', '建平县', '2113', '21');
INSERT INTO `areas` VALUES ('211324', '喀喇沁左翼蒙古族自治县', '2113', '21');
INSERT INTO `areas` VALUES ('211381', '北票市', '2113', '21');
INSERT INTO `areas` VALUES ('211382', '凌源市', '2113', '21');
INSERT INTO `areas` VALUES ('211402', '连山区', '2114', '21');
INSERT INTO `areas` VALUES ('211403', '龙港区', '2114', '21');
INSERT INTO `areas` VALUES ('211404', '南票区', '2114', '21');
INSERT INTO `areas` VALUES ('211421', '绥中县', '2114', '21');
INSERT INTO `areas` VALUES ('211422', '建昌县', '2114', '21');
INSERT INTO `areas` VALUES ('211481', '兴城市', '2114', '21');
INSERT INTO `areas` VALUES ('220102', '南关区', '2201', '22');
INSERT INTO `areas` VALUES ('220103', '宽城区', '2201', '22');
INSERT INTO `areas` VALUES ('220104', '朝阳区', '2201', '22');
INSERT INTO `areas` VALUES ('220105', '二道区', '2201', '22');
INSERT INTO `areas` VALUES ('220106', '绿园区', '2201', '22');
INSERT INTO `areas` VALUES ('220112', '双阳区', '2201', '22');
INSERT INTO `areas` VALUES ('220113', '九台区', '2201', '22');
INSERT INTO `areas` VALUES ('220122', '农安县', '2201', '22');
INSERT INTO `areas` VALUES ('220171', '长春经济技术开发区', '2201', '22');
INSERT INTO `areas` VALUES ('220172', '长春净月高新技术产业开发区', '2201', '22');
INSERT INTO `areas` VALUES ('220173', '长春高新技术产业开发区', '2201', '22');
INSERT INTO `areas` VALUES ('220174', '长春汽车经济技术开发区', '2201', '22');
INSERT INTO `areas` VALUES ('220182', '榆树市', '2201', '22');
INSERT INTO `areas` VALUES ('220183', '德惠市', '2201', '22');
INSERT INTO `areas` VALUES ('220184', '公主岭市', '2201', '22');
INSERT INTO `areas` VALUES ('220202', '昌邑区', '2202', '22');
INSERT INTO `areas` VALUES ('220203', '龙潭区', '2202', '22');
INSERT INTO `areas` VALUES ('220204', '船营区', '2202', '22');
INSERT INTO `areas` VALUES ('220211', '丰满区', '2202', '22');
INSERT INTO `areas` VALUES ('220221', '永吉县', '2202', '22');
INSERT INTO `areas` VALUES ('220271', '吉林经济开发区', '2202', '22');
INSERT INTO `areas` VALUES ('220272', '吉林高新技术产业开发区', '2202', '22');
INSERT INTO `areas` VALUES ('220273', '吉林中国新加坡食品区', '2202', '22');
INSERT INTO `areas` VALUES ('220281', '蛟河市', '2202', '22');
INSERT INTO `areas` VALUES ('220282', '桦甸市', '2202', '22');
INSERT INTO `areas` VALUES ('220283', '舒兰市', '2202', '22');
INSERT INTO `areas` VALUES ('220284', '磐石市', '2202', '22');
INSERT INTO `areas` VALUES ('220302', '铁西区', '2203', '22');
INSERT INTO `areas` VALUES ('220303', '铁东区', '2203', '22');
INSERT INTO `areas` VALUES ('220322', '梨树县', '2203', '22');
INSERT INTO `areas` VALUES ('220323', '伊通满族自治县', '2203', '22');
INSERT INTO `areas` VALUES ('220382', '双辽市', '2203', '22');
INSERT INTO `areas` VALUES ('220402', '龙山区', '2204', '22');
INSERT INTO `areas` VALUES ('220403', '西安区', '2204', '22');
INSERT INTO `areas` VALUES ('220421', '东丰县', '2204', '22');
INSERT INTO `areas` VALUES ('220422', '东辽县', '2204', '22');
INSERT INTO `areas` VALUES ('220502', '东昌区', '2205', '22');
INSERT INTO `areas` VALUES ('220503', '二道江区', '2205', '22');
INSERT INTO `areas` VALUES ('220521', '通化县', '2205', '22');
INSERT INTO `areas` VALUES ('220523', '辉南县', '2205', '22');
INSERT INTO `areas` VALUES ('220524', '柳河县', '2205', '22');
INSERT INTO `areas` VALUES ('220581', '梅河口市', '2205', '22');
INSERT INTO `areas` VALUES ('220582', '集安市', '2205', '22');
INSERT INTO `areas` VALUES ('220602', '浑江区', '2206', '22');
INSERT INTO `areas` VALUES ('220605', '江源区', '2206', '22');
INSERT INTO `areas` VALUES ('220621', '抚松县', '2206', '22');
INSERT INTO `areas` VALUES ('220622', '靖宇县', '2206', '22');
INSERT INTO `areas` VALUES ('220623', '长白朝鲜族自治县', '2206', '22');
INSERT INTO `areas` VALUES ('220681', '临江市', '2206', '22');
INSERT INTO `areas` VALUES ('220702', '宁江区', '2207', '22');
INSERT INTO `areas` VALUES ('220721', '前郭尔罗斯蒙古族自治县', '2207', '22');
INSERT INTO `areas` VALUES ('220722', '长岭县', '2207', '22');
INSERT INTO `areas` VALUES ('220723', '乾安县', '2207', '22');
INSERT INTO `areas` VALUES ('220771', '吉林松原经济开发区', '2207', '22');
INSERT INTO `areas` VALUES ('220781', '扶余市', '2207', '22');
INSERT INTO `areas` VALUES ('220802', '洮北区', '2208', '22');
INSERT INTO `areas` VALUES ('220821', '镇赉县', '2208', '22');
INSERT INTO `areas` VALUES ('220822', '通榆县', '2208', '22');
INSERT INTO `areas` VALUES ('220871', '吉林白城经济开发区', '2208', '22');
INSERT INTO `areas` VALUES ('220881', '洮南市', '2208', '22');
INSERT INTO `areas` VALUES ('220882', '大安市', '2208', '22');
INSERT INTO `areas` VALUES ('222401', '延吉市', '2224', '22');
INSERT INTO `areas` VALUES ('222402', '图们市', '2224', '22');
INSERT INTO `areas` VALUES ('222403', '敦化市', '2224', '22');
INSERT INTO `areas` VALUES ('222404', '珲春市', '2224', '22');
INSERT INTO `areas` VALUES ('222405', '龙井市', '2224', '22');
INSERT INTO `areas` VALUES ('222406', '和龙市', '2224', '22');
INSERT INTO `areas` VALUES ('222424', '汪清县', '2224', '22');
INSERT INTO `areas` VALUES ('222426', '安图县', '2224', '22');
INSERT INTO `areas` VALUES ('230102', '道里区', '2301', '23');
INSERT INTO `areas` VALUES ('230103', '南岗区', '2301', '23');
INSERT INTO `areas` VALUES ('230104', '道外区', '2301', '23');
INSERT INTO `areas` VALUES ('230108', '平房区', '2301', '23');
INSERT INTO `areas` VALUES ('230109', '松北区', '2301', '23');
INSERT INTO `areas` VALUES ('230110', '香坊区', '2301', '23');
INSERT INTO `areas` VALUES ('230111', '呼兰区', '2301', '23');
INSERT INTO `areas` VALUES ('230112', '阿城区', '2301', '23');
INSERT INTO `areas` VALUES ('230113', '双城区', '2301', '23');
INSERT INTO `areas` VALUES ('230123', '依兰县', '2301', '23');
INSERT INTO `areas` VALUES ('230124', '方正县', '2301', '23');
INSERT INTO `areas` VALUES ('230125', '宾县', '2301', '23');
INSERT INTO `areas` VALUES ('230126', '巴彦县', '2301', '23');
INSERT INTO `areas` VALUES ('230127', '木兰县', '2301', '23');
INSERT INTO `areas` VALUES ('230128', '通河县', '2301', '23');
INSERT INTO `areas` VALUES ('230129', '延寿县', '2301', '23');
INSERT INTO `areas` VALUES ('230183', '尚志市', '2301', '23');
INSERT INTO `areas` VALUES ('230184', '五常市', '2301', '23');
INSERT INTO `areas` VALUES ('230202', '龙沙区', '2302', '23');
INSERT INTO `areas` VALUES ('230203', '建华区', '2302', '23');
INSERT INTO `areas` VALUES ('230204', '铁锋区', '2302', '23');
INSERT INTO `areas` VALUES ('230205', '昂昂溪区', '2302', '23');
INSERT INTO `areas` VALUES ('230206', '富拉尔基区', '2302', '23');
INSERT INTO `areas` VALUES ('230207', '碾子山区', '2302', '23');
INSERT INTO `areas` VALUES ('230208', '梅里斯达斡尔族区', '2302', '23');
INSERT INTO `areas` VALUES ('230221', '龙江县', '2302', '23');
INSERT INTO `areas` VALUES ('230223', '依安县', '2302', '23');
INSERT INTO `areas` VALUES ('230224', '泰来县', '2302', '23');
INSERT INTO `areas` VALUES ('230225', '甘南县', '2302', '23');
INSERT INTO `areas` VALUES ('230227', '富裕县', '2302', '23');
INSERT INTO `areas` VALUES ('230229', '克山县', '2302', '23');
INSERT INTO `areas` VALUES ('230230', '克东县', '2302', '23');
INSERT INTO `areas` VALUES ('230231', '拜泉县', '2302', '23');
INSERT INTO `areas` VALUES ('230281', '讷河市', '2302', '23');
INSERT INTO `areas` VALUES ('230302', '鸡冠区', '2303', '23');
INSERT INTO `areas` VALUES ('230303', '恒山区', '2303', '23');
INSERT INTO `areas` VALUES ('230304', '滴道区', '2303', '23');
INSERT INTO `areas` VALUES ('230305', '梨树区', '2303', '23');
INSERT INTO `areas` VALUES ('230306', '城子河区', '2303', '23');
INSERT INTO `areas` VALUES ('230307', '麻山区', '2303', '23');
INSERT INTO `areas` VALUES ('230321', '鸡东县', '2303', '23');
INSERT INTO `areas` VALUES ('230381', '虎林市', '2303', '23');
INSERT INTO `areas` VALUES ('230382', '密山市', '2303', '23');
INSERT INTO `areas` VALUES ('230402', '向阳区', '2304', '23');
INSERT INTO `areas` VALUES ('230403', '工农区', '2304', '23');
INSERT INTO `areas` VALUES ('230404', '南山区', '2304', '23');
INSERT INTO `areas` VALUES ('230405', '兴安区', '2304', '23');
INSERT INTO `areas` VALUES ('230406', '东山区', '2304', '23');
INSERT INTO `areas` VALUES ('230407', '兴山区', '2304', '23');
INSERT INTO `areas` VALUES ('230421', '萝北县', '2304', '23');
INSERT INTO `areas` VALUES ('230422', '绥滨县', '2304', '23');
INSERT INTO `areas` VALUES ('230502', '尖山区', '2305', '23');
INSERT INTO `areas` VALUES ('230503', '岭东区', '2305', '23');
INSERT INTO `areas` VALUES ('230505', '四方台区', '2305', '23');
INSERT INTO `areas` VALUES ('230506', '宝山区', '2305', '23');
INSERT INTO `areas` VALUES ('230521', '集贤县', '2305', '23');
INSERT INTO `areas` VALUES ('230522', '友谊县', '2305', '23');
INSERT INTO `areas` VALUES ('230523', '宝清县', '2305', '23');
INSERT INTO `areas` VALUES ('230524', '饶河县', '2305', '23');
INSERT INTO `areas` VALUES ('230602', '萨尔图区', '2306', '23');
INSERT INTO `areas` VALUES ('230603', '龙凤区', '2306', '23');
INSERT INTO `areas` VALUES ('230604', '让胡路区', '2306', '23');
INSERT INTO `areas` VALUES ('230605', '红岗区', '2306', '23');
INSERT INTO `areas` VALUES ('230606', '大同区', '2306', '23');
INSERT INTO `areas` VALUES ('230621', '肇州县', '2306', '23');
INSERT INTO `areas` VALUES ('230622', '肇源县', '2306', '23');
INSERT INTO `areas` VALUES ('230623', '林甸县', '2306', '23');
INSERT INTO `areas` VALUES ('230624', '杜尔伯特蒙古族自治县', '2306', '23');
INSERT INTO `areas` VALUES ('230671', '大庆高新技术产业开发区', '2306', '23');
INSERT INTO `areas` VALUES ('230717', '伊美区', '2307', '23');
INSERT INTO `areas` VALUES ('230718', '乌翠区', '2307', '23');
INSERT INTO `areas` VALUES ('230719', '友好区', '2307', '23');
INSERT INTO `areas` VALUES ('230722', '嘉荫县', '2307', '23');
INSERT INTO `areas` VALUES ('230723', '汤旺县', '2307', '23');
INSERT INTO `areas` VALUES ('230724', '丰林县', '2307', '23');
INSERT INTO `areas` VALUES ('230725', '大箐山县', '2307', '23');
INSERT INTO `areas` VALUES ('230726', '南岔县', '2307', '23');
INSERT INTO `areas` VALUES ('230751', '金林区', '2307', '23');
INSERT INTO `areas` VALUES ('230781', '铁力市', '2307', '23');
INSERT INTO `areas` VALUES ('230803', '向阳区', '2308', '23');
INSERT INTO `areas` VALUES ('230804', '前进区', '2308', '23');
INSERT INTO `areas` VALUES ('230805', '东风区', '2308', '23');
INSERT INTO `areas` VALUES ('230811', '郊区', '2308', '23');
INSERT INTO `areas` VALUES ('230822', '桦南县', '2308', '23');
INSERT INTO `areas` VALUES ('230826', '桦川县', '2308', '23');
INSERT INTO `areas` VALUES ('230828', '汤原县', '2308', '23');
INSERT INTO `areas` VALUES ('230881', '同江市', '2308', '23');
INSERT INTO `areas` VALUES ('230882', '富锦市', '2308', '23');
INSERT INTO `areas` VALUES ('230883', '抚远市', '2308', '23');
INSERT INTO `areas` VALUES ('230902', '新兴区', '2309', '23');
INSERT INTO `areas` VALUES ('230903', '桃山区', '2309', '23');
INSERT INTO `areas` VALUES ('230904', '茄子河区', '2309', '23');
INSERT INTO `areas` VALUES ('230921', '勃利县', '2309', '23');
INSERT INTO `areas` VALUES ('231002', '东安区', '2310', '23');
INSERT INTO `areas` VALUES ('231003', '阳明区', '2310', '23');
INSERT INTO `areas` VALUES ('231004', '爱民区', '2310', '23');
INSERT INTO `areas` VALUES ('231005', '西安区', '2310', '23');
INSERT INTO `areas` VALUES ('231025', '林口县', '2310', '23');
INSERT INTO `areas` VALUES ('231071', '牡丹江经济技术开发区', '2310', '23');
INSERT INTO `areas` VALUES ('231081', '绥芬河市', '2310', '23');
INSERT INTO `areas` VALUES ('231083', '海林市', '2310', '23');
INSERT INTO `areas` VALUES ('231084', '宁安市', '2310', '23');
INSERT INTO `areas` VALUES ('231085', '穆棱市', '2310', '23');
INSERT INTO `areas` VALUES ('231086', '东宁市', '2310', '23');
INSERT INTO `areas` VALUES ('231102', '爱辉区', '2311', '23');
INSERT INTO `areas` VALUES ('231123', '逊克县', '2311', '23');
INSERT INTO `areas` VALUES ('231124', '孙吴县', '2311', '23');
INSERT INTO `areas` VALUES ('231181', '北安市', '2311', '23');
INSERT INTO `areas` VALUES ('231182', '五大连池市', '2311', '23');
INSERT INTO `areas` VALUES ('231183', '嫩江市', '2311', '23');
INSERT INTO `areas` VALUES ('231202', '北林区', '2312', '23');
INSERT INTO `areas` VALUES ('231221', '望奎县', '2312', '23');
INSERT INTO `areas` VALUES ('231222', '兰西县', '2312', '23');
INSERT INTO `areas` VALUES ('231223', '青冈县', '2312', '23');
INSERT INTO `areas` VALUES ('231224', '庆安县', '2312', '23');
INSERT INTO `areas` VALUES ('231225', '明水县', '2312', '23');
INSERT INTO `areas` VALUES ('231226', '绥棱县', '2312', '23');
INSERT INTO `areas` VALUES ('231281', '安达市', '2312', '23');
INSERT INTO `areas` VALUES ('231282', '肇东市', '2312', '23');
INSERT INTO `areas` VALUES ('231283', '海伦市', '2312', '23');
INSERT INTO `areas` VALUES ('232701', '漠河市', '2327', '23');
INSERT INTO `areas` VALUES ('232721', '呼玛县', '2327', '23');
INSERT INTO `areas` VALUES ('232722', '塔河县', '2327', '23');
INSERT INTO `areas` VALUES ('232761', '加格达奇区', '2327', '23');
INSERT INTO `areas` VALUES ('232762', '松岭区', '2327', '23');
INSERT INTO `areas` VALUES ('232763', '新林区', '2327', '23');
INSERT INTO `areas` VALUES ('232764', '呼中区', '2327', '23');
INSERT INTO `areas` VALUES ('310101', '黄浦区', '3101', '31');
INSERT INTO `areas` VALUES ('310104', '徐汇区', '3101', '31');
INSERT INTO `areas` VALUES ('310105', '长宁区', '3101', '31');
INSERT INTO `areas` VALUES ('310106', '静安区', '3101', '31');
INSERT INTO `areas` VALUES ('310107', '普陀区', '3101', '31');
INSERT INTO `areas` VALUES ('310109', '虹口区', '3101', '31');
INSERT INTO `areas` VALUES ('310110', '杨浦区', '3101', '31');
INSERT INTO `areas` VALUES ('310112', '闵行区', '3101', '31');
INSERT INTO `areas` VALUES ('310113', '宝山区', '3101', '31');
INSERT INTO `areas` VALUES ('310114', '嘉定区', '3101', '31');
INSERT INTO `areas` VALUES ('310115', '浦东新区', '3101', '31');
INSERT INTO `areas` VALUES ('310116', '金山区', '3101', '31');
INSERT INTO `areas` VALUES ('310117', '松江区', '3101', '31');
INSERT INTO `areas` VALUES ('310118', '青浦区', '3101', '31');
INSERT INTO `areas` VALUES ('310120', '奉贤区', '3101', '31');
INSERT INTO `areas` VALUES ('310151', '崇明区', '3101', '31');
INSERT INTO `areas` VALUES ('320102', '玄武区', '3201', '32');
INSERT INTO `areas` VALUES ('320104', '秦淮区', '3201', '32');
INSERT INTO `areas` VALUES ('320105', '建邺区', '3201', '32');
INSERT INTO `areas` VALUES ('320106', '鼓楼区', '3201', '32');
INSERT INTO `areas` VALUES ('320111', '浦口区', '3201', '32');
INSERT INTO `areas` VALUES ('320113', '栖霞区', '3201', '32');
INSERT INTO `areas` VALUES ('320114', '雨花台区', '3201', '32');
INSERT INTO `areas` VALUES ('320115', '江宁区', '3201', '32');
INSERT INTO `areas` VALUES ('320116', '六合区', '3201', '32');
INSERT INTO `areas` VALUES ('320117', '溧水区', '3201', '32');
INSERT INTO `areas` VALUES ('320118', '高淳区', '3201', '32');
INSERT INTO `areas` VALUES ('320205', '锡山区', '3202', '32');
INSERT INTO `areas` VALUES ('320206', '惠山区', '3202', '32');
INSERT INTO `areas` VALUES ('320211', '滨湖区', '3202', '32');
INSERT INTO `areas` VALUES ('320213', '梁溪区', '3202', '32');
INSERT INTO `areas` VALUES ('320214', '新吴区', '3202', '32');
INSERT INTO `areas` VALUES ('320281', '江阴市', '3202', '32');
INSERT INTO `areas` VALUES ('320282', '宜兴市', '3202', '32');
INSERT INTO `areas` VALUES ('320302', '鼓楼区', '3203', '32');
INSERT INTO `areas` VALUES ('320303', '云龙区', '3203', '32');
INSERT INTO `areas` VALUES ('320305', '贾汪区', '3203', '32');
INSERT INTO `areas` VALUES ('320311', '泉山区', '3203', '32');
INSERT INTO `areas` VALUES ('320312', '铜山区', '3203', '32');
INSERT INTO `areas` VALUES ('320321', '丰县', '3203', '32');
INSERT INTO `areas` VALUES ('320322', '沛县', '3203', '32');
INSERT INTO `areas` VALUES ('320324', '睢宁县', '3203', '32');
INSERT INTO `areas` VALUES ('320371', '徐州经济技术开发区', '3203', '32');
INSERT INTO `areas` VALUES ('320381', '新沂市', '3203', '32');
INSERT INTO `areas` VALUES ('320382', '邳州市', '3203', '32');
INSERT INTO `areas` VALUES ('320402', '天宁区', '3204', '32');
INSERT INTO `areas` VALUES ('320404', '钟楼区', '3204', '32');
INSERT INTO `areas` VALUES ('320411', '新北区', '3204', '32');
INSERT INTO `areas` VALUES ('320412', '武进区', '3204', '32');
INSERT INTO `areas` VALUES ('320413', '金坛区', '3204', '32');
INSERT INTO `areas` VALUES ('320481', '溧阳市', '3204', '32');
INSERT INTO `areas` VALUES ('320505', '虎丘区', '3205', '32');
INSERT INTO `areas` VALUES ('320506', '吴中区', '3205', '32');
INSERT INTO `areas` VALUES ('320507', '相城区', '3205', '32');
INSERT INTO `areas` VALUES ('320508', '姑苏区', '3205', '32');
INSERT INTO `areas` VALUES ('320509', '吴江区', '3205', '32');
INSERT INTO `areas` VALUES ('320571', '苏州工业园区', '3205', '32');
INSERT INTO `areas` VALUES ('320581', '常熟市', '3205', '32');
INSERT INTO `areas` VALUES ('320582', '张家港市', '3205', '32');
INSERT INTO `areas` VALUES ('320583', '昆山市', '3205', '32');
INSERT INTO `areas` VALUES ('320585', '太仓市', '3205', '32');
INSERT INTO `areas` VALUES ('320612', '通州区', '3206', '32');
INSERT INTO `areas` VALUES ('320613', '崇川区', '3206', '32');
INSERT INTO `areas` VALUES ('320614', '海门区', '3206', '32');
INSERT INTO `areas` VALUES ('320623', '如东县', '3206', '32');
INSERT INTO `areas` VALUES ('320671', '南通经济技术开发区', '3206', '32');
INSERT INTO `areas` VALUES ('320681', '启东市', '3206', '32');
INSERT INTO `areas` VALUES ('320682', '如皋市', '3206', '32');
INSERT INTO `areas` VALUES ('320685', '海安市', '3206', '32');
INSERT INTO `areas` VALUES ('320703', '连云区', '3207', '32');
INSERT INTO `areas` VALUES ('320706', '海州区', '3207', '32');
INSERT INTO `areas` VALUES ('320707', '赣榆区', '3207', '32');
INSERT INTO `areas` VALUES ('320722', '东海县', '3207', '32');
INSERT INTO `areas` VALUES ('320723', '灌云县', '3207', '32');
INSERT INTO `areas` VALUES ('320724', '灌南县', '3207', '32');
INSERT INTO `areas` VALUES ('320771', '连云港经济技术开发区', '3207', '32');
INSERT INTO `areas` VALUES ('320772', '连云港高新技术产业开发区', '3207', '32');
INSERT INTO `areas` VALUES ('320803', '淮安区', '3208', '32');
INSERT INTO `areas` VALUES ('320804', '淮阴区', '3208', '32');
INSERT INTO `areas` VALUES ('320812', '清江浦区', '3208', '32');
INSERT INTO `areas` VALUES ('320813', '洪泽区', '3208', '32');
INSERT INTO `areas` VALUES ('320826', '涟水县', '3208', '32');
INSERT INTO `areas` VALUES ('320830', '盱眙县', '3208', '32');
INSERT INTO `areas` VALUES ('320831', '金湖县', '3208', '32');
INSERT INTO `areas` VALUES ('320871', '淮安经济技术开发区', '3208', '32');
INSERT INTO `areas` VALUES ('320902', '亭湖区', '3209', '32');
INSERT INTO `areas` VALUES ('320903', '盐都区', '3209', '32');
INSERT INTO `areas` VALUES ('320904', '大丰区', '3209', '32');
INSERT INTO `areas` VALUES ('320921', '响水县', '3209', '32');
INSERT INTO `areas` VALUES ('320922', '滨海县', '3209', '32');
INSERT INTO `areas` VALUES ('320923', '阜宁县', '3209', '32');
INSERT INTO `areas` VALUES ('320924', '射阳县', '3209', '32');
INSERT INTO `areas` VALUES ('320925', '建湖县', '3209', '32');
INSERT INTO `areas` VALUES ('320971', '盐城经济技术开发区', '3209', '32');
INSERT INTO `areas` VALUES ('320981', '东台市', '3209', '32');
INSERT INTO `areas` VALUES ('321002', '广陵区', '3210', '32');
INSERT INTO `areas` VALUES ('321003', '邗江区', '3210', '32');
INSERT INTO `areas` VALUES ('321012', '江都区', '3210', '32');
INSERT INTO `areas` VALUES ('321023', '宝应县', '3210', '32');
INSERT INTO `areas` VALUES ('321071', '扬州经济技术开发区', '3210', '32');
INSERT INTO `areas` VALUES ('321081', '仪征市', '3210', '32');
INSERT INTO `areas` VALUES ('321084', '高邮市', '3210', '32');
INSERT INTO `areas` VALUES ('321102', '京口区', '3211', '32');
INSERT INTO `areas` VALUES ('321111', '润州区', '3211', '32');
INSERT INTO `areas` VALUES ('321112', '丹徒区', '3211', '32');
INSERT INTO `areas` VALUES ('321171', '镇江新区', '3211', '32');
INSERT INTO `areas` VALUES ('321181', '丹阳市', '3211', '32');
INSERT INTO `areas` VALUES ('321182', '扬中市', '3211', '32');
INSERT INTO `areas` VALUES ('321183', '句容市', '3211', '32');
INSERT INTO `areas` VALUES ('321202', '海陵区', '3212', '32');
INSERT INTO `areas` VALUES ('321203', '高港区', '3212', '32');
INSERT INTO `areas` VALUES ('321204', '姜堰区', '3212', '32');
INSERT INTO `areas` VALUES ('321271', '泰州医药高新技术产业开发区', '3212', '32');
INSERT INTO `areas` VALUES ('321281', '兴化市', '3212', '32');
INSERT INTO `areas` VALUES ('321282', '靖江市', '3212', '32');
INSERT INTO `areas` VALUES ('321283', '泰兴市', '3212', '32');
INSERT INTO `areas` VALUES ('321302', '宿城区', '3213', '32');
INSERT INTO `areas` VALUES ('321311', '宿豫区', '3213', '32');
INSERT INTO `areas` VALUES ('321322', '沭阳县', '3213', '32');
INSERT INTO `areas` VALUES ('321323', '泗阳县', '3213', '32');
INSERT INTO `areas` VALUES ('321324', '泗洪县', '3213', '32');
INSERT INTO `areas` VALUES ('321371', '宿迁经济技术开发区', '3213', '32');
INSERT INTO `areas` VALUES ('330102', '上城区', '3301', '33');
INSERT INTO `areas` VALUES ('330105', '拱墅区', '3301', '33');
INSERT INTO `areas` VALUES ('330106', '西湖区', '3301', '33');
INSERT INTO `areas` VALUES ('330108', '滨江区', '3301', '33');
INSERT INTO `areas` VALUES ('330109', '萧山区', '3301', '33');
INSERT INTO `areas` VALUES ('330110', '余杭区', '3301', '33');
INSERT INTO `areas` VALUES ('330111', '富阳区', '3301', '33');
INSERT INTO `areas` VALUES ('330112', '临安区', '3301', '33');
INSERT INTO `areas` VALUES ('330113', '临平区', '3301', '33');
INSERT INTO `areas` VALUES ('330114', '钱塘区', '3301', '33');
INSERT INTO `areas` VALUES ('330122', '桐庐县', '3301', '33');
INSERT INTO `areas` VALUES ('330127', '淳安县', '3301', '33');
INSERT INTO `areas` VALUES ('330182', '建德市', '3301', '33');
INSERT INTO `areas` VALUES ('330203', '海曙区', '3302', '33');
INSERT INTO `areas` VALUES ('330205', '江北区', '3302', '33');
INSERT INTO `areas` VALUES ('330206', '北仑区', '3302', '33');
INSERT INTO `areas` VALUES ('330211', '镇海区', '3302', '33');
INSERT INTO `areas` VALUES ('330212', '鄞州区', '3302', '33');
INSERT INTO `areas` VALUES ('330213', '奉化区', '3302', '33');
INSERT INTO `areas` VALUES ('330225', '象山县', '3302', '33');
INSERT INTO `areas` VALUES ('330226', '宁海县', '3302', '33');
INSERT INTO `areas` VALUES ('330281', '余姚市', '3302', '33');
INSERT INTO `areas` VALUES ('330282', '慈溪市', '3302', '33');
INSERT INTO `areas` VALUES ('330302', '鹿城区', '3303', '33');
INSERT INTO `areas` VALUES ('330303', '龙湾区', '3303', '33');
INSERT INTO `areas` VALUES ('330304', '瓯海区', '3303', '33');
INSERT INTO `areas` VALUES ('330305', '洞头区', '3303', '33');
INSERT INTO `areas` VALUES ('330324', '永嘉县', '3303', '33');
INSERT INTO `areas` VALUES ('330326', '平阳县', '3303', '33');
INSERT INTO `areas` VALUES ('330327', '苍南县', '3303', '33');
INSERT INTO `areas` VALUES ('330328', '文成县', '3303', '33');
INSERT INTO `areas` VALUES ('330329', '泰顺县', '3303', '33');
INSERT INTO `areas` VALUES ('330371', '温州经济技术开发区', '3303', '33');
INSERT INTO `areas` VALUES ('330381', '瑞安市', '3303', '33');
INSERT INTO `areas` VALUES ('330382', '乐清市', '3303', '33');
INSERT INTO `areas` VALUES ('330383', '龙港市', '3303', '33');
INSERT INTO `areas` VALUES ('330402', '南湖区', '3304', '33');
INSERT INTO `areas` VALUES ('330411', '秀洲区', '3304', '33');
INSERT INTO `areas` VALUES ('330421', '嘉善县', '3304', '33');
INSERT INTO `areas` VALUES ('330424', '海盐县', '3304', '33');
INSERT INTO `areas` VALUES ('330481', '海宁市', '3304', '33');
INSERT INTO `areas` VALUES ('330482', '平湖市', '3304', '33');
INSERT INTO `areas` VALUES ('330483', '桐乡市', '3304', '33');
INSERT INTO `areas` VALUES ('330502', '吴兴区', '3305', '33');
INSERT INTO `areas` VALUES ('330503', '南浔区', '3305', '33');
INSERT INTO `areas` VALUES ('330521', '德清县', '3305', '33');
INSERT INTO `areas` VALUES ('330522', '长兴县', '3305', '33');
INSERT INTO `areas` VALUES ('330523', '安吉县', '3305', '33');
INSERT INTO `areas` VALUES ('330602', '越城区', '3306', '33');
INSERT INTO `areas` VALUES ('330603', '柯桥区', '3306', '33');
INSERT INTO `areas` VALUES ('330604', '上虞区', '3306', '33');
INSERT INTO `areas` VALUES ('330624', '新昌县', '3306', '33');
INSERT INTO `areas` VALUES ('330681', '诸暨市', '3306', '33');
INSERT INTO `areas` VALUES ('330683', '嵊州市', '3306', '33');
INSERT INTO `areas` VALUES ('330702', '婺城区', '3307', '33');
INSERT INTO `areas` VALUES ('330703', '金东区', '3307', '33');
INSERT INTO `areas` VALUES ('330723', '武义县', '3307', '33');
INSERT INTO `areas` VALUES ('330726', '浦江县', '3307', '33');
INSERT INTO `areas` VALUES ('330727', '磐安县', '3307', '33');
INSERT INTO `areas` VALUES ('330781', '兰溪市', '3307', '33');
INSERT INTO `areas` VALUES ('330782', '义乌市', '3307', '33');
INSERT INTO `areas` VALUES ('330783', '东阳市', '3307', '33');
INSERT INTO `areas` VALUES ('330784', '永康市', '3307', '33');
INSERT INTO `areas` VALUES ('330802', '柯城区', '3308', '33');
INSERT INTO `areas` VALUES ('330803', '衢江区', '3308', '33');
INSERT INTO `areas` VALUES ('330822', '常山县', '3308', '33');
INSERT INTO `areas` VALUES ('330824', '开化县', '3308', '33');
INSERT INTO `areas` VALUES ('330825', '龙游县', '3308', '33');
INSERT INTO `areas` VALUES ('330881', '江山市', '3308', '33');
INSERT INTO `areas` VALUES ('330902', '定海区', '3309', '33');
INSERT INTO `areas` VALUES ('330903', '普陀区', '3309', '33');
INSERT INTO `areas` VALUES ('330921', '岱山县', '3309', '33');
INSERT INTO `areas` VALUES ('330922', '嵊泗县', '3309', '33');
INSERT INTO `areas` VALUES ('331002', '椒江区', '3310', '33');
INSERT INTO `areas` VALUES ('331003', '黄岩区', '3310', '33');
INSERT INTO `areas` VALUES ('331004', '路桥区', '3310', '33');
INSERT INTO `areas` VALUES ('331022', '三门县', '3310', '33');
INSERT INTO `areas` VALUES ('331023', '天台县', '3310', '33');
INSERT INTO `areas` VALUES ('331024', '仙居县', '3310', '33');
INSERT INTO `areas` VALUES ('331081', '温岭市', '3310', '33');
INSERT INTO `areas` VALUES ('331082', '临海市', '3310', '33');
INSERT INTO `areas` VALUES ('331083', '玉环市', '3310', '33');
INSERT INTO `areas` VALUES ('331102', '莲都区', '3311', '33');
INSERT INTO `areas` VALUES ('331121', '青田县', '3311', '33');
INSERT INTO `areas` VALUES ('331122', '缙云县', '3311', '33');
INSERT INTO `areas` VALUES ('331123', '遂昌县', '3311', '33');
INSERT INTO `areas` VALUES ('331124', '松阳县', '3311', '33');
INSERT INTO `areas` VALUES ('331125', '云和县', '3311', '33');
INSERT INTO `areas` VALUES ('331126', '庆元县', '3311', '33');
INSERT INTO `areas` VALUES ('331127', '景宁畲族自治县', '3311', '33');
INSERT INTO `areas` VALUES ('331181', '龙泉市', '3311', '33');
INSERT INTO `areas` VALUES ('340102', '瑶海区', '3401', '34');
INSERT INTO `areas` VALUES ('340103', '庐阳区', '3401', '34');
INSERT INTO `areas` VALUES ('340104', '蜀山区', '3401', '34');
INSERT INTO `areas` VALUES ('340111', '包河区', '3401', '34');
INSERT INTO `areas` VALUES ('340121', '长丰县', '3401', '34');
INSERT INTO `areas` VALUES ('340122', '肥东县', '3401', '34');
INSERT INTO `areas` VALUES ('340123', '肥西县', '3401', '34');
INSERT INTO `areas` VALUES ('340124', '庐江县', '3401', '34');
INSERT INTO `areas` VALUES ('340171', '合肥高新技术产业开发区', '3401', '34');
INSERT INTO `areas` VALUES ('340172', '合肥经济技术开发区', '3401', '34');
INSERT INTO `areas` VALUES ('340173', '合肥新站高新技术产业开发区', '3401', '34');
INSERT INTO `areas` VALUES ('340181', '巢湖市', '3401', '34');
INSERT INTO `areas` VALUES ('340202', '镜湖区', '3402', '34');
INSERT INTO `areas` VALUES ('340207', '鸠江区', '3402', '34');
INSERT INTO `areas` VALUES ('340209', '弋江区', '3402', '34');
INSERT INTO `areas` VALUES ('340210', '湾沚区', '3402', '34');
INSERT INTO `areas` VALUES ('340212', '繁昌区', '3402', '34');
INSERT INTO `areas` VALUES ('340223', '南陵县', '3402', '34');
INSERT INTO `areas` VALUES ('340271', '芜湖经济技术开发区', '3402', '34');
INSERT INTO `areas` VALUES ('340272', '安徽芜湖三山经济开发区', '3402', '34');
INSERT INTO `areas` VALUES ('340281', '无为市', '3402', '34');
INSERT INTO `areas` VALUES ('340302', '龙子湖区', '3403', '34');
INSERT INTO `areas` VALUES ('340303', '蚌山区', '3403', '34');
INSERT INTO `areas` VALUES ('340304', '禹会区', '3403', '34');
INSERT INTO `areas` VALUES ('340311', '淮上区', '3403', '34');
INSERT INTO `areas` VALUES ('340321', '怀远县', '3403', '34');
INSERT INTO `areas` VALUES ('340322', '五河县', '3403', '34');
INSERT INTO `areas` VALUES ('340323', '固镇县', '3403', '34');
INSERT INTO `areas` VALUES ('340371', '蚌埠市高新技术开发区', '3403', '34');
INSERT INTO `areas` VALUES ('340372', '蚌埠市经济开发区', '3403', '34');
INSERT INTO `areas` VALUES ('340402', '大通区', '3404', '34');
INSERT INTO `areas` VALUES ('340403', '田家庵区', '3404', '34');
INSERT INTO `areas` VALUES ('340404', '谢家集区', '3404', '34');
INSERT INTO `areas` VALUES ('340405', '八公山区', '3404', '34');
INSERT INTO `areas` VALUES ('340406', '潘集区', '3404', '34');
INSERT INTO `areas` VALUES ('340421', '凤台县', '3404', '34');
INSERT INTO `areas` VALUES ('340422', '寿县', '3404', '34');
INSERT INTO `areas` VALUES ('340503', '花山区', '3405', '34');
INSERT INTO `areas` VALUES ('340504', '雨山区', '3405', '34');
INSERT INTO `areas` VALUES ('340506', '博望区', '3405', '34');
INSERT INTO `areas` VALUES ('340521', '当涂县', '3405', '34');
INSERT INTO `areas` VALUES ('340522', '含山县', '3405', '34');
INSERT INTO `areas` VALUES ('340523', '和县', '3405', '34');
INSERT INTO `areas` VALUES ('340602', '杜集区', '3406', '34');
INSERT INTO `areas` VALUES ('340603', '相山区', '3406', '34');
INSERT INTO `areas` VALUES ('340604', '烈山区', '3406', '34');
INSERT INTO `areas` VALUES ('340621', '濉溪县', '3406', '34');
INSERT INTO `areas` VALUES ('340705', '铜官区', '3407', '34');
INSERT INTO `areas` VALUES ('340706', '义安区', '3407', '34');
INSERT INTO `areas` VALUES ('340711', '郊区', '3407', '34');
INSERT INTO `areas` VALUES ('340722', '枞阳县', '3407', '34');
INSERT INTO `areas` VALUES ('340802', '迎江区', '3408', '34');
INSERT INTO `areas` VALUES ('340803', '大观区', '3408', '34');
INSERT INTO `areas` VALUES ('340811', '宜秀区', '3408', '34');
INSERT INTO `areas` VALUES ('340822', '怀宁县', '3408', '34');
INSERT INTO `areas` VALUES ('340825', '太湖县', '3408', '34');
INSERT INTO `areas` VALUES ('340826', '宿松县', '3408', '34');
INSERT INTO `areas` VALUES ('340827', '望江县', '3408', '34');
INSERT INTO `areas` VALUES ('340828', '岳西县', '3408', '34');
INSERT INTO `areas` VALUES ('340871', '安徽安庆经济开发区', '3408', '34');
INSERT INTO `areas` VALUES ('340881', '桐城市', '3408', '34');
INSERT INTO `areas` VALUES ('340882', '潜山市', '3408', '34');
INSERT INTO `areas` VALUES ('341002', '屯溪区', '3410', '34');
INSERT INTO `areas` VALUES ('341003', '黄山区', '3410', '34');
INSERT INTO `areas` VALUES ('341004', '徽州区', '3410', '34');
INSERT INTO `areas` VALUES ('341021', '歙县', '3410', '34');
INSERT INTO `areas` VALUES ('341022', '休宁县', '3410', '34');
INSERT INTO `areas` VALUES ('341023', '黟县', '3410', '34');
INSERT INTO `areas` VALUES ('341024', '祁门县', '3410', '34');
INSERT INTO `areas` VALUES ('341102', '琅琊区', '3411', '34');
INSERT INTO `areas` VALUES ('341103', '南谯区', '3411', '34');
INSERT INTO `areas` VALUES ('341122', '来安县', '3411', '34');
INSERT INTO `areas` VALUES ('341124', '全椒县', '3411', '34');
INSERT INTO `areas` VALUES ('341125', '定远县', '3411', '34');
INSERT INTO `areas` VALUES ('341126', '凤阳县', '3411', '34');
INSERT INTO `areas` VALUES ('341171', '中新苏滁高新技术产业开发区', '3411', '34');
INSERT INTO `areas` VALUES ('341172', '滁州经济技术开发区', '3411', '34');
INSERT INTO `areas` VALUES ('341181', '天长市', '3411', '34');
INSERT INTO `areas` VALUES ('341182', '明光市', '3411', '34');
INSERT INTO `areas` VALUES ('341202', '颍州区', '3412', '34');
INSERT INTO `areas` VALUES ('341203', '颍东区', '3412', '34');
INSERT INTO `areas` VALUES ('341204', '颍泉区', '3412', '34');
INSERT INTO `areas` VALUES ('341221', '临泉县', '3412', '34');
INSERT INTO `areas` VALUES ('341222', '太和县', '3412', '34');
INSERT INTO `areas` VALUES ('341225', '阜南县', '3412', '34');
INSERT INTO `areas` VALUES ('341226', '颍上县', '3412', '34');
INSERT INTO `areas` VALUES ('341271', '阜阳合肥现代产业园区', '3412', '34');
INSERT INTO `areas` VALUES ('341272', '阜阳经济技术开发区', '3412', '34');
INSERT INTO `areas` VALUES ('341282', '界首市', '3412', '34');
INSERT INTO `areas` VALUES ('341302', '埇桥区', '3413', '34');
INSERT INTO `areas` VALUES ('341321', '砀山县', '3413', '34');
INSERT INTO `areas` VALUES ('341322', '萧县', '3413', '34');
INSERT INTO `areas` VALUES ('341323', '灵璧县', '3413', '34');
INSERT INTO `areas` VALUES ('341324', '泗县', '3413', '34');
INSERT INTO `areas` VALUES ('341371', '宿州马鞍山现代产业园区', '3413', '34');
INSERT INTO `areas` VALUES ('341372', '宿州经济技术开发区', '3413', '34');
INSERT INTO `areas` VALUES ('341502', '金安区', '3415', '34');
INSERT INTO `areas` VALUES ('341503', '裕安区', '3415', '34');
INSERT INTO `areas` VALUES ('341504', '叶集区', '3415', '34');
INSERT INTO `areas` VALUES ('341522', '霍邱县', '3415', '34');
INSERT INTO `areas` VALUES ('341523', '舒城县', '3415', '34');
INSERT INTO `areas` VALUES ('341524', '金寨县', '3415', '34');
INSERT INTO `areas` VALUES ('341525', '霍山县', '3415', '34');
INSERT INTO `areas` VALUES ('341602', '谯城区', '3416', '34');
INSERT INTO `areas` VALUES ('341621', '涡阳县', '3416', '34');
INSERT INTO `areas` VALUES ('341622', '蒙城县', '3416', '34');
INSERT INTO `areas` VALUES ('341623', '利辛县', '3416', '34');
INSERT INTO `areas` VALUES ('341702', '贵池区', '3417', '34');
INSERT INTO `areas` VALUES ('341721', '东至县', '3417', '34');
INSERT INTO `areas` VALUES ('341722', '石台县', '3417', '34');
INSERT INTO `areas` VALUES ('341723', '青阳县', '3417', '34');
INSERT INTO `areas` VALUES ('341802', '宣州区', '3418', '34');
INSERT INTO `areas` VALUES ('341821', '郎溪县', '3418', '34');
INSERT INTO `areas` VALUES ('341823', '泾县', '3418', '34');
INSERT INTO `areas` VALUES ('341824', '绩溪县', '3418', '34');
INSERT INTO `areas` VALUES ('341825', '旌德县', '3418', '34');
INSERT INTO `areas` VALUES ('341871', '宣城市经济开发区', '3418', '34');
INSERT INTO `areas` VALUES ('341881', '宁国市', '3418', '34');
INSERT INTO `areas` VALUES ('341882', '广德市', '3418', '34');
INSERT INTO `areas` VALUES ('350102', '鼓楼区', '3501', '35');
INSERT INTO `areas` VALUES ('350103', '台江区', '3501', '35');
INSERT INTO `areas` VALUES ('350104', '仓山区', '3501', '35');
INSERT INTO `areas` VALUES ('350105', '马尾区', '3501', '35');
INSERT INTO `areas` VALUES ('350111', '晋安区', '3501', '35');
INSERT INTO `areas` VALUES ('350112', '长乐区', '3501', '35');
INSERT INTO `areas` VALUES ('350121', '闽侯县', '3501', '35');
INSERT INTO `areas` VALUES ('350122', '连江县', '3501', '35');
INSERT INTO `areas` VALUES ('350123', '罗源县', '3501', '35');
INSERT INTO `areas` VALUES ('350124', '闽清县', '3501', '35');
INSERT INTO `areas` VALUES ('350125', '永泰县', '3501', '35');
INSERT INTO `areas` VALUES ('350128', '平潭县', '3501', '35');
INSERT INTO `areas` VALUES ('350181', '福清市', '3501', '35');
INSERT INTO `areas` VALUES ('350203', '思明区', '3502', '35');
INSERT INTO `areas` VALUES ('350205', '海沧区', '3502', '35');
INSERT INTO `areas` VALUES ('350206', '湖里区', '3502', '35');
INSERT INTO `areas` VALUES ('350211', '集美区', '3502', '35');
INSERT INTO `areas` VALUES ('350212', '同安区', '3502', '35');
INSERT INTO `areas` VALUES ('350213', '翔安区', '3502', '35');
INSERT INTO `areas` VALUES ('350302', '城厢区', '3503', '35');
INSERT INTO `areas` VALUES ('350303', '涵江区', '3503', '35');
INSERT INTO `areas` VALUES ('350304', '荔城区', '3503', '35');
INSERT INTO `areas` VALUES ('350305', '秀屿区', '3503', '35');
INSERT INTO `areas` VALUES ('350322', '仙游县', '3503', '35');
INSERT INTO `areas` VALUES ('350404', '三元区', '3504', '35');
INSERT INTO `areas` VALUES ('350405', '沙县区', '3504', '35');
INSERT INTO `areas` VALUES ('350421', '明溪县', '3504', '35');
INSERT INTO `areas` VALUES ('350423', '清流县', '3504', '35');
INSERT INTO `areas` VALUES ('350424', '宁化县', '3504', '35');
INSERT INTO `areas` VALUES ('350425', '大田县', '3504', '35');
INSERT INTO `areas` VALUES ('350426', '尤溪县', '3504', '35');
INSERT INTO `areas` VALUES ('350428', '将乐县', '3504', '35');
INSERT INTO `areas` VALUES ('350429', '泰宁县', '3504', '35');
INSERT INTO `areas` VALUES ('350430', '建宁县', '3504', '35');
INSERT INTO `areas` VALUES ('350481', '永安市', '3504', '35');
INSERT INTO `areas` VALUES ('350502', '鲤城区', '3505', '35');
INSERT INTO `areas` VALUES ('350503', '丰泽区', '3505', '35');
INSERT INTO `areas` VALUES ('350504', '洛江区', '3505', '35');
INSERT INTO `areas` VALUES ('350505', '泉港区', '3505', '35');
INSERT INTO `areas` VALUES ('350521', '惠安县', '3505', '35');
INSERT INTO `areas` VALUES ('350524', '安溪县', '3505', '35');
INSERT INTO `areas` VALUES ('350525', '永春县', '3505', '35');
INSERT INTO `areas` VALUES ('350526', '德化县', '3505', '35');
INSERT INTO `areas` VALUES ('350527', '金门县', '3505', '35');
INSERT INTO `areas` VALUES ('350581', '石狮市', '3505', '35');
INSERT INTO `areas` VALUES ('350582', '晋江市', '3505', '35');
INSERT INTO `areas` VALUES ('350583', '南安市', '3505', '35');
INSERT INTO `areas` VALUES ('350602', '芗城区', '3506', '35');
INSERT INTO `areas` VALUES ('350603', '龙文区', '3506', '35');
INSERT INTO `areas` VALUES ('350604', '龙海区', '3506', '35');
INSERT INTO `areas` VALUES ('350605', '长泰区', '3506', '35');
INSERT INTO `areas` VALUES ('350622', '云霄县', '3506', '35');
INSERT INTO `areas` VALUES ('350623', '漳浦县', '3506', '35');
INSERT INTO `areas` VALUES ('350624', '诏安县', '3506', '35');
INSERT INTO `areas` VALUES ('350626', '东山县', '3506', '35');
INSERT INTO `areas` VALUES ('350627', '南靖县', '3506', '35');
INSERT INTO `areas` VALUES ('350628', '平和县', '3506', '35');
INSERT INTO `areas` VALUES ('350629', '华安县', '3506', '35');
INSERT INTO `areas` VALUES ('350702', '延平区', '3507', '35');
INSERT INTO `areas` VALUES ('350703', '建阳区', '3507', '35');
INSERT INTO `areas` VALUES ('350721', '顺昌县', '3507', '35');
INSERT INTO `areas` VALUES ('350722', '浦城县', '3507', '35');
INSERT INTO `areas` VALUES ('350723', '光泽县', '3507', '35');
INSERT INTO `areas` VALUES ('350724', '松溪县', '3507', '35');
INSERT INTO `areas` VALUES ('350725', '政和县', '3507', '35');
INSERT INTO `areas` VALUES ('350781', '邵武市', '3507', '35');
INSERT INTO `areas` VALUES ('350782', '武夷山市', '3507', '35');
INSERT INTO `areas` VALUES ('350783', '建瓯市', '3507', '35');
INSERT INTO `areas` VALUES ('350802', '新罗区', '3508', '35');
INSERT INTO `areas` VALUES ('350803', '永定区', '3508', '35');
INSERT INTO `areas` VALUES ('350821', '长汀县', '3508', '35');
INSERT INTO `areas` VALUES ('350823', '上杭县', '3508', '35');
INSERT INTO `areas` VALUES ('350824', '武平县', '3508', '35');
INSERT INTO `areas` VALUES ('350825', '连城县', '3508', '35');
INSERT INTO `areas` VALUES ('350881', '漳平市', '3508', '35');
INSERT INTO `areas` VALUES ('350902', '蕉城区', '3509', '35');
INSERT INTO `areas` VALUES ('350921', '霞浦县', '3509', '35');
INSERT INTO `areas` VALUES ('350922', '古田县', '3509', '35');
INSERT INTO `areas` VALUES ('350923', '屏南县', '3509', '35');
INSERT INTO `areas` VALUES ('350924', '寿宁县', '3509', '35');
INSERT INTO `areas` VALUES ('350925', '周宁县', '3509', '35');
INSERT INTO `areas` VALUES ('350926', '柘荣县', '3509', '35');
INSERT INTO `areas` VALUES ('350981', '福安市', '3509', '35');
INSERT INTO `areas` VALUES ('350982', '福鼎市', '3509', '35');
INSERT INTO `areas` VALUES ('360102', '东湖区', '3601', '36');
INSERT INTO `areas` VALUES ('360103', '西湖区', '3601', '36');
INSERT INTO `areas` VALUES ('360104', '青云谱区', '3601', '36');
INSERT INTO `areas` VALUES ('360111', '青山湖区', '3601', '36');
INSERT INTO `areas` VALUES ('360112', '新建区', '3601', '36');
INSERT INTO `areas` VALUES ('360113', '红谷滩区', '3601', '36');
INSERT INTO `areas` VALUES ('360121', '南昌县', '3601', '36');
INSERT INTO `areas` VALUES ('360123', '安义县', '3601', '36');
INSERT INTO `areas` VALUES ('360124', '进贤县', '3601', '36');
INSERT INTO `areas` VALUES ('360202', '昌江区', '3602', '36');
INSERT INTO `areas` VALUES ('360203', '珠山区', '3602', '36');
INSERT INTO `areas` VALUES ('360222', '浮梁县', '3602', '36');
INSERT INTO `areas` VALUES ('360281', '乐平市', '3602', '36');
INSERT INTO `areas` VALUES ('360302', '安源区', '3603', '36');
INSERT INTO `areas` VALUES ('360313', '湘东区', '3603', '36');
INSERT INTO `areas` VALUES ('360321', '莲花县', '3603', '36');
INSERT INTO `areas` VALUES ('360322', '上栗县', '3603', '36');
INSERT INTO `areas` VALUES ('360323', '芦溪县', '3603', '36');
INSERT INTO `areas` VALUES ('360402', '濂溪区', '3604', '36');
INSERT INTO `areas` VALUES ('360403', '浔阳区', '3604', '36');
INSERT INTO `areas` VALUES ('360404', '柴桑区', '3604', '36');
INSERT INTO `areas` VALUES ('360423', '武宁县', '3604', '36');
INSERT INTO `areas` VALUES ('360424', '修水县', '3604', '36');
INSERT INTO `areas` VALUES ('360425', '永修县', '3604', '36');
INSERT INTO `areas` VALUES ('360426', '德安县', '3604', '36');
INSERT INTO `areas` VALUES ('360428', '都昌县', '3604', '36');
INSERT INTO `areas` VALUES ('360429', '湖口县', '3604', '36');
INSERT INTO `areas` VALUES ('360430', '彭泽县', '3604', '36');
INSERT INTO `areas` VALUES ('360481', '瑞昌市', '3604', '36');
INSERT INTO `areas` VALUES ('360482', '共青城市', '3604', '36');
INSERT INTO `areas` VALUES ('360483', '庐山市', '3604', '36');
INSERT INTO `areas` VALUES ('360502', '渝水区', '3605', '36');
INSERT INTO `areas` VALUES ('360521', '分宜县', '3605', '36');
INSERT INTO `areas` VALUES ('360602', '月湖区', '3606', '36');
INSERT INTO `areas` VALUES ('360603', '余江区', '3606', '36');
INSERT INTO `areas` VALUES ('360681', '贵溪市', '3606', '36');
INSERT INTO `areas` VALUES ('360702', '章贡区', '3607', '36');
INSERT INTO `areas` VALUES ('360703', '南康区', '3607', '36');
INSERT INTO `areas` VALUES ('360704', '赣县区', '3607', '36');
INSERT INTO `areas` VALUES ('360722', '信丰县', '3607', '36');
INSERT INTO `areas` VALUES ('360723', '大余县', '3607', '36');
INSERT INTO `areas` VALUES ('360724', '上犹县', '3607', '36');
INSERT INTO `areas` VALUES ('360725', '崇义县', '3607', '36');
INSERT INTO `areas` VALUES ('360726', '安远县', '3607', '36');
INSERT INTO `areas` VALUES ('360728', '定南县', '3607', '36');
INSERT INTO `areas` VALUES ('360729', '全南县', '3607', '36');
INSERT INTO `areas` VALUES ('360730', '宁都县', '3607', '36');
INSERT INTO `areas` VALUES ('360731', '于都县', '3607', '36');
INSERT INTO `areas` VALUES ('360732', '兴国县', '3607', '36');
INSERT INTO `areas` VALUES ('360733', '会昌县', '3607', '36');
INSERT INTO `areas` VALUES ('360734', '寻乌县', '3607', '36');
INSERT INTO `areas` VALUES ('360735', '石城县', '3607', '36');
INSERT INTO `areas` VALUES ('360781', '瑞金市', '3607', '36');
INSERT INTO `areas` VALUES ('360783', '龙南市', '3607', '36');
INSERT INTO `areas` VALUES ('360802', '吉州区', '3608', '36');
INSERT INTO `areas` VALUES ('360803', '青原区', '3608', '36');
INSERT INTO `areas` VALUES ('360821', '吉安县', '3608', '36');
INSERT INTO `areas` VALUES ('360822', '吉水县', '3608', '36');
INSERT INTO `areas` VALUES ('360823', '峡江县', '3608', '36');
INSERT INTO `areas` VALUES ('360824', '新干县', '3608', '36');
INSERT INTO `areas` VALUES ('360825', '永丰县', '3608', '36');
INSERT INTO `areas` VALUES ('360826', '泰和县', '3608', '36');
INSERT INTO `areas` VALUES ('360827', '遂川县', '3608', '36');
INSERT INTO `areas` VALUES ('360828', '万安县', '3608', '36');
INSERT INTO `areas` VALUES ('360829', '安福县', '3608', '36');
INSERT INTO `areas` VALUES ('360830', '永新县', '3608', '36');
INSERT INTO `areas` VALUES ('360881', '井冈山市', '3608', '36');
INSERT INTO `areas` VALUES ('360902', '袁州区', '3609', '36');
INSERT INTO `areas` VALUES ('360921', '奉新县', '3609', '36');
INSERT INTO `areas` VALUES ('360922', '万载县', '3609', '36');
INSERT INTO `areas` VALUES ('360923', '上高县', '3609', '36');
INSERT INTO `areas` VALUES ('360924', '宜丰县', '3609', '36');
INSERT INTO `areas` VALUES ('360925', '靖安县', '3609', '36');
INSERT INTO `areas` VALUES ('360926', '铜鼓县', '3609', '36');
INSERT INTO `areas` VALUES ('360981', '丰城市', '3609', '36');
INSERT INTO `areas` VALUES ('360982', '樟树市', '3609', '36');
INSERT INTO `areas` VALUES ('360983', '高安市', '3609', '36');
INSERT INTO `areas` VALUES ('361002', '临川区', '3610', '36');
INSERT INTO `areas` VALUES ('361003', '东乡区', '3610', '36');
INSERT INTO `areas` VALUES ('361021', '南城县', '3610', '36');
INSERT INTO `areas` VALUES ('361022', '黎川县', '3610', '36');
INSERT INTO `areas` VALUES ('361023', '南丰县', '3610', '36');
INSERT INTO `areas` VALUES ('361024', '崇仁县', '3610', '36');
INSERT INTO `areas` VALUES ('361025', '乐安县', '3610', '36');
INSERT INTO `areas` VALUES ('361026', '宜黄县', '3610', '36');
INSERT INTO `areas` VALUES ('361027', '金溪县', '3610', '36');
INSERT INTO `areas` VALUES ('361028', '资溪县', '3610', '36');
INSERT INTO `areas` VALUES ('361030', '广昌县', '3610', '36');
INSERT INTO `areas` VALUES ('361102', '信州区', '3611', '36');
INSERT INTO `areas` VALUES ('361103', '广丰区', '3611', '36');
INSERT INTO `areas` VALUES ('361104', '广信区', '3611', '36');
INSERT INTO `areas` VALUES ('361123', '玉山县', '3611', '36');
INSERT INTO `areas` VALUES ('361124', '铅山县', '3611', '36');
INSERT INTO `areas` VALUES ('361125', '横峰县', '3611', '36');
INSERT INTO `areas` VALUES ('361126', '弋阳县', '3611', '36');
INSERT INTO `areas` VALUES ('361127', '余干县', '3611', '36');
INSERT INTO `areas` VALUES ('361128', '鄱阳县', '3611', '36');
INSERT INTO `areas` VALUES ('361129', '万年县', '3611', '36');
INSERT INTO `areas` VALUES ('361130', '婺源县', '3611', '36');
INSERT INTO `areas` VALUES ('361181', '德兴市', '3611', '36');
INSERT INTO `areas` VALUES ('370102', '历下区', '3701', '37');
INSERT INTO `areas` VALUES ('370103', '市中区', '3701', '37');
INSERT INTO `areas` VALUES ('370104', '槐荫区', '3701', '37');
INSERT INTO `areas` VALUES ('370105', '天桥区', '3701', '37');
INSERT INTO `areas` VALUES ('370112', '历城区', '3701', '37');
INSERT INTO `areas` VALUES ('370113', '长清区', '3701', '37');
INSERT INTO `areas` VALUES ('370114', '章丘区', '3701', '37');
INSERT INTO `areas` VALUES ('370115', '济阳区', '3701', '37');
INSERT INTO `areas` VALUES ('370116', '莱芜区', '3701', '37');
INSERT INTO `areas` VALUES ('370117', '钢城区', '3701', '37');
INSERT INTO `areas` VALUES ('370124', '平阴县', '3701', '37');
INSERT INTO `areas` VALUES ('370126', '商河县', '3701', '37');
INSERT INTO `areas` VALUES ('370171', '济南高新技术产业开发区', '3701', '37');
INSERT INTO `areas` VALUES ('370202', '市南区', '3702', '37');
INSERT INTO `areas` VALUES ('370203', '市北区', '3702', '37');
INSERT INTO `areas` VALUES ('370211', '黄岛区', '3702', '37');
INSERT INTO `areas` VALUES ('370212', '崂山区', '3702', '37');
INSERT INTO `areas` VALUES ('370213', '李沧区', '3702', '37');
INSERT INTO `areas` VALUES ('370214', '城阳区', '3702', '37');
INSERT INTO `areas` VALUES ('370215', '即墨区', '3702', '37');
INSERT INTO `areas` VALUES ('370271', '青岛高新技术产业开发区', '3702', '37');
INSERT INTO `areas` VALUES ('370281', '胶州市', '3702', '37');
INSERT INTO `areas` VALUES ('370283', '平度市', '3702', '37');
INSERT INTO `areas` VALUES ('370285', '莱西市', '3702', '37');
INSERT INTO `areas` VALUES ('370302', '淄川区', '3703', '37');
INSERT INTO `areas` VALUES ('370303', '张店区', '3703', '37');
INSERT INTO `areas` VALUES ('370304', '博山区', '3703', '37');
INSERT INTO `areas` VALUES ('370305', '临淄区', '3703', '37');
INSERT INTO `areas` VALUES ('370306', '周村区', '3703', '37');
INSERT INTO `areas` VALUES ('370321', '桓台县', '3703', '37');
INSERT INTO `areas` VALUES ('370322', '高青县', '3703', '37');
INSERT INTO `areas` VALUES ('370323', '沂源县', '3703', '37');
INSERT INTO `areas` VALUES ('370402', '市中区', '3704', '37');
INSERT INTO `areas` VALUES ('370403', '薛城区', '3704', '37');
INSERT INTO `areas` VALUES ('370404', '峄城区', '3704', '37');
INSERT INTO `areas` VALUES ('370405', '台儿庄区', '3704', '37');
INSERT INTO `areas` VALUES ('370406', '山亭区', '3704', '37');
INSERT INTO `areas` VALUES ('370481', '滕州市', '3704', '37');
INSERT INTO `areas` VALUES ('370502', '东营区', '3705', '37');
INSERT INTO `areas` VALUES ('370503', '河口区', '3705', '37');
INSERT INTO `areas` VALUES ('370505', '垦利区', '3705', '37');
INSERT INTO `areas` VALUES ('370522', '利津县', '3705', '37');
INSERT INTO `areas` VALUES ('370523', '广饶县', '3705', '37');
INSERT INTO `areas` VALUES ('370571', '东营经济技术开发区', '3705', '37');
INSERT INTO `areas` VALUES ('370572', '东营港经济开发区', '3705', '37');
INSERT INTO `areas` VALUES ('370602', '芝罘区', '3706', '37');
INSERT INTO `areas` VALUES ('370611', '福山区', '3706', '37');
INSERT INTO `areas` VALUES ('370612', '牟平区', '3706', '37');
INSERT INTO `areas` VALUES ('370613', '莱山区', '3706', '37');
INSERT INTO `areas` VALUES ('370614', '蓬莱区', '3706', '37');
INSERT INTO `areas` VALUES ('370671', '烟台高新技术产业开发区', '3706', '37');
INSERT INTO `areas` VALUES ('370672', '烟台经济技术开发区', '3706', '37');
INSERT INTO `areas` VALUES ('370681', '龙口市', '3706', '37');
INSERT INTO `areas` VALUES ('370682', '莱阳市', '3706', '37');
INSERT INTO `areas` VALUES ('370683', '莱州市', '3706', '37');
INSERT INTO `areas` VALUES ('370685', '招远市', '3706', '37');
INSERT INTO `areas` VALUES ('370686', '栖霞市', '3706', '37');
INSERT INTO `areas` VALUES ('370687', '海阳市', '3706', '37');
INSERT INTO `areas` VALUES ('370702', '潍城区', '3707', '37');
INSERT INTO `areas` VALUES ('370703', '寒亭区', '3707', '37');
INSERT INTO `areas` VALUES ('370704', '坊子区', '3707', '37');
INSERT INTO `areas` VALUES ('370705', '奎文区', '3707', '37');
INSERT INTO `areas` VALUES ('370724', '临朐县', '3707', '37');
INSERT INTO `areas` VALUES ('370725', '昌乐县', '3707', '37');
INSERT INTO `areas` VALUES ('370772', '潍坊滨海经济技术开发区', '3707', '37');
INSERT INTO `areas` VALUES ('370781', '青州市', '3707', '37');
INSERT INTO `areas` VALUES ('370782', '诸城市', '3707', '37');
INSERT INTO `areas` VALUES ('370783', '寿光市', '3707', '37');
INSERT INTO `areas` VALUES ('370784', '安丘市', '3707', '37');
INSERT INTO `areas` VALUES ('370785', '高密市', '3707', '37');
INSERT INTO `areas` VALUES ('370786', '昌邑市', '3707', '37');
INSERT INTO `areas` VALUES ('370811', '任城区', '3708', '37');
INSERT INTO `areas` VALUES ('370812', '兖州区', '3708', '37');
INSERT INTO `areas` VALUES ('370826', '微山县', '3708', '37');
INSERT INTO `areas` VALUES ('370827', '鱼台县', '3708', '37');
INSERT INTO `areas` VALUES ('370828', '金乡县', '3708', '37');
INSERT INTO `areas` VALUES ('370829', '嘉祥县', '3708', '37');
INSERT INTO `areas` VALUES ('370830', '汶上县', '3708', '37');
INSERT INTO `areas` VALUES ('370831', '泗水县', '3708', '37');
INSERT INTO `areas` VALUES ('370832', '梁山县', '3708', '37');
INSERT INTO `areas` VALUES ('370871', '济宁高新技术产业开发区', '3708', '37');
INSERT INTO `areas` VALUES ('370881', '曲阜市', '3708', '37');
INSERT INTO `areas` VALUES ('370883', '邹城市', '3708', '37');
INSERT INTO `areas` VALUES ('370902', '泰山区', '3709', '37');
INSERT INTO `areas` VALUES ('370911', '岱岳区', '3709', '37');
INSERT INTO `areas` VALUES ('370921', '宁阳县', '3709', '37');
INSERT INTO `areas` VALUES ('370923', '东平县', '3709', '37');
INSERT INTO `areas` VALUES ('370982', '新泰市', '3709', '37');
INSERT INTO `areas` VALUES ('370983', '肥城市', '3709', '37');
INSERT INTO `areas` VALUES ('371002', '环翠区', '3710', '37');
INSERT INTO `areas` VALUES ('371003', '文登区', '3710', '37');
INSERT INTO `areas` VALUES ('371071', '威海火炬高技术产业开发区', '3710', '37');
INSERT INTO `areas` VALUES ('371072', '威海经济技术开发区', '3710', '37');
INSERT INTO `areas` VALUES ('371073', '威海临港经济技术开发区', '3710', '37');
INSERT INTO `areas` VALUES ('371082', '荣成市', '3710', '37');
INSERT INTO `areas` VALUES ('371083', '乳山市', '3710', '37');
INSERT INTO `areas` VALUES ('371102', '东港区', '3711', '37');
INSERT INTO `areas` VALUES ('371103', '岚山区', '3711', '37');
INSERT INTO `areas` VALUES ('371121', '五莲县', '3711', '37');
INSERT INTO `areas` VALUES ('371122', '莒县', '3711', '37');
INSERT INTO `areas` VALUES ('371171', '日照经济技术开发区', '3711', '37');
INSERT INTO `areas` VALUES ('371302', '兰山区', '3713', '37');
INSERT INTO `areas` VALUES ('371311', '罗庄区', '3713', '37');
INSERT INTO `areas` VALUES ('371312', '河东区', '3713', '37');
INSERT INTO `areas` VALUES ('371321', '沂南县', '3713', '37');
INSERT INTO `areas` VALUES ('371322', '郯城县', '3713', '37');
INSERT INTO `areas` VALUES ('371323', '沂水县', '3713', '37');
INSERT INTO `areas` VALUES ('371324', '兰陵县', '3713', '37');
INSERT INTO `areas` VALUES ('371325', '费县', '3713', '37');
INSERT INTO `areas` VALUES ('371326', '平邑县', '3713', '37');
INSERT INTO `areas` VALUES ('371327', '莒南县', '3713', '37');
INSERT INTO `areas` VALUES ('371328', '蒙阴县', '3713', '37');
INSERT INTO `areas` VALUES ('371329', '临沭县', '3713', '37');
INSERT INTO `areas` VALUES ('371371', '临沂高新技术产业开发区', '3713', '37');
INSERT INTO `areas` VALUES ('371402', '德城区', '3714', '37');
INSERT INTO `areas` VALUES ('371403', '陵城区', '3714', '37');
INSERT INTO `areas` VALUES ('371422', '宁津县', '3714', '37');
INSERT INTO `areas` VALUES ('371423', '庆云县', '3714', '37');
INSERT INTO `areas` VALUES ('371424', '临邑县', '3714', '37');
INSERT INTO `areas` VALUES ('371425', '齐河县', '3714', '37');
INSERT INTO `areas` VALUES ('371426', '平原县', '3714', '37');
INSERT INTO `areas` VALUES ('371427', '夏津县', '3714', '37');
INSERT INTO `areas` VALUES ('371428', '武城县', '3714', '37');
INSERT INTO `areas` VALUES ('371471', '德州经济技术开发区', '3714', '37');
INSERT INTO `areas` VALUES ('371472', '德州运河经济开发区', '3714', '37');
INSERT INTO `areas` VALUES ('371481', '乐陵市', '3714', '37');
INSERT INTO `areas` VALUES ('371482', '禹城市', '3714', '37');
INSERT INTO `areas` VALUES ('371502', '东昌府区', '3715', '37');
INSERT INTO `areas` VALUES ('371503', '茌平区', '3715', '37');
INSERT INTO `areas` VALUES ('371521', '阳谷县', '3715', '37');
INSERT INTO `areas` VALUES ('371522', '莘县', '3715', '37');
INSERT INTO `areas` VALUES ('371524', '东阿县', '3715', '37');
INSERT INTO `areas` VALUES ('371525', '冠县', '3715', '37');
INSERT INTO `areas` VALUES ('371526', '高唐县', '3715', '37');
INSERT INTO `areas` VALUES ('371581', '临清市', '3715', '37');
INSERT INTO `areas` VALUES ('371602', '滨城区', '3716', '37');
INSERT INTO `areas` VALUES ('371603', '沾化区', '3716', '37');
INSERT INTO `areas` VALUES ('371621', '惠民县', '3716', '37');
INSERT INTO `areas` VALUES ('371622', '阳信县', '3716', '37');
INSERT INTO `areas` VALUES ('371623', '无棣县', '3716', '37');
INSERT INTO `areas` VALUES ('371625', '博兴县', '3716', '37');
INSERT INTO `areas` VALUES ('371681', '邹平市', '3716', '37');
INSERT INTO `areas` VALUES ('371702', '牡丹区', '3717', '37');
INSERT INTO `areas` VALUES ('371703', '定陶区', '3717', '37');
INSERT INTO `areas` VALUES ('371721', '曹县', '3717', '37');
INSERT INTO `areas` VALUES ('371722', '单县', '3717', '37');
INSERT INTO `areas` VALUES ('371723', '成武县', '3717', '37');
INSERT INTO `areas` VALUES ('371724', '巨野县', '3717', '37');
INSERT INTO `areas` VALUES ('371725', '郓城县', '3717', '37');
INSERT INTO `areas` VALUES ('371726', '鄄城县', '3717', '37');
INSERT INTO `areas` VALUES ('371728', '东明县', '3717', '37');
INSERT INTO `areas` VALUES ('371771', '菏泽经济技术开发区', '3717', '37');
INSERT INTO `areas` VALUES ('371772', '菏泽高新技术开发区', '3717', '37');
INSERT INTO `areas` VALUES ('410102', '中原区', '4101', '41');
INSERT INTO `areas` VALUES ('410103', '二七区', '4101', '41');
INSERT INTO `areas` VALUES ('410104', '管城回族区', '4101', '41');
INSERT INTO `areas` VALUES ('410105', '金水区', '4101', '41');
INSERT INTO `areas` VALUES ('410106', '上街区', '4101', '41');
INSERT INTO `areas` VALUES ('410108', '惠济区', '4101', '41');
INSERT INTO `areas` VALUES ('410122', '中牟县', '4101', '41');
INSERT INTO `areas` VALUES ('410171', '郑州经济技术开发区', '4101', '41');
INSERT INTO `areas` VALUES ('410172', '郑州高新技术产业开发区', '4101', '41');
INSERT INTO `areas` VALUES ('410173', '郑州航空港经济综合实验区', '4101', '41');
INSERT INTO `areas` VALUES ('410181', '巩义市', '4101', '41');
INSERT INTO `areas` VALUES ('410182', '荥阳市', '4101', '41');
INSERT INTO `areas` VALUES ('410183', '新密市', '4101', '41');
INSERT INTO `areas` VALUES ('410184', '新郑市', '4101', '41');
INSERT INTO `areas` VALUES ('410185', '登封市', '4101', '41');
INSERT INTO `areas` VALUES ('410202', '龙亭区', '4102', '41');
INSERT INTO `areas` VALUES ('410203', '顺河回族区', '4102', '41');
INSERT INTO `areas` VALUES ('410204', '鼓楼区', '4102', '41');
INSERT INTO `areas` VALUES ('410205', '禹王台区', '4102', '41');
INSERT INTO `areas` VALUES ('410212', '祥符区', '4102', '41');
INSERT INTO `areas` VALUES ('410221', '杞县', '4102', '41');
INSERT INTO `areas` VALUES ('410222', '通许县', '4102', '41');
INSERT INTO `areas` VALUES ('410223', '尉氏县', '4102', '41');
INSERT INTO `areas` VALUES ('410225', '兰考县', '4102', '41');
INSERT INTO `areas` VALUES ('410302', '老城区', '4103', '41');
INSERT INTO `areas` VALUES ('410303', '西工区', '4103', '41');
INSERT INTO `areas` VALUES ('410304', '瀍河回族区', '4103', '41');
INSERT INTO `areas` VALUES ('410305', '涧西区', '4103', '41');
INSERT INTO `areas` VALUES ('410307', '偃师区', '4103', '41');
INSERT INTO `areas` VALUES ('410308', '孟津区', '4103', '41');
INSERT INTO `areas` VALUES ('410311', '洛龙区', '4103', '41');
INSERT INTO `areas` VALUES ('410323', '新安县', '4103', '41');
INSERT INTO `areas` VALUES ('410324', '栾川县', '4103', '41');
INSERT INTO `areas` VALUES ('410325', '嵩县', '4103', '41');
INSERT INTO `areas` VALUES ('410326', '汝阳县', '4103', '41');
INSERT INTO `areas` VALUES ('410327', '宜阳县', '4103', '41');
INSERT INTO `areas` VALUES ('410328', '洛宁县', '4103', '41');
INSERT INTO `areas` VALUES ('410329', '伊川县', '4103', '41');
INSERT INTO `areas` VALUES ('410371', '洛阳高新技术产业开发区', '4103', '41');
INSERT INTO `areas` VALUES ('410402', '新华区', '4104', '41');
INSERT INTO `areas` VALUES ('410403', '卫东区', '4104', '41');
INSERT INTO `areas` VALUES ('410404', '石龙区', '4104', '41');
INSERT INTO `areas` VALUES ('410411', '湛河区', '4104', '41');
INSERT INTO `areas` VALUES ('410421', '宝丰县', '4104', '41');
INSERT INTO `areas` VALUES ('410422', '叶县', '4104', '41');
INSERT INTO `areas` VALUES ('410423', '鲁山县', '4104', '41');
INSERT INTO `areas` VALUES ('410425', '郏县', '4104', '41');
INSERT INTO `areas` VALUES ('410471', '平顶山高新技术产业开发区', '4104', '41');
INSERT INTO `areas` VALUES ('410472', '平顶山市城乡一体化示范区', '4104', '41');
INSERT INTO `areas` VALUES ('410481', '舞钢市', '4104', '41');
INSERT INTO `areas` VALUES ('410482', '汝州市', '4104', '41');
INSERT INTO `areas` VALUES ('410502', '文峰区', '4105', '41');
INSERT INTO `areas` VALUES ('410503', '北关区', '4105', '41');
INSERT INTO `areas` VALUES ('410505', '殷都区', '4105', '41');
INSERT INTO `areas` VALUES ('410506', '龙安区', '4105', '41');
INSERT INTO `areas` VALUES ('410522', '安阳县', '4105', '41');
INSERT INTO `areas` VALUES ('410523', '汤阴县', '4105', '41');
INSERT INTO `areas` VALUES ('410526', '滑县', '4105', '41');
INSERT INTO `areas` VALUES ('410527', '内黄县', '4105', '41');
INSERT INTO `areas` VALUES ('410571', '安阳高新技术产业开发区', '4105', '41');
INSERT INTO `areas` VALUES ('410581', '林州市', '4105', '41');
INSERT INTO `areas` VALUES ('410602', '鹤山区', '4106', '41');
INSERT INTO `areas` VALUES ('410603', '山城区', '4106', '41');
INSERT INTO `areas` VALUES ('410611', '淇滨区', '4106', '41');
INSERT INTO `areas` VALUES ('410621', '浚县', '4106', '41');
INSERT INTO `areas` VALUES ('410622', '淇县', '4106', '41');
INSERT INTO `areas` VALUES ('410671', '鹤壁经济技术开发区', '4106', '41');
INSERT INTO `areas` VALUES ('410702', '红旗区', '4107', '41');
INSERT INTO `areas` VALUES ('410703', '卫滨区', '4107', '41');
INSERT INTO `areas` VALUES ('410704', '凤泉区', '4107', '41');
INSERT INTO `areas` VALUES ('410711', '牧野区', '4107', '41');
INSERT INTO `areas` VALUES ('410721', '新乡县', '4107', '41');
INSERT INTO `areas` VALUES ('410724', '获嘉县', '4107', '41');
INSERT INTO `areas` VALUES ('410725', '原阳县', '4107', '41');
INSERT INTO `areas` VALUES ('410726', '延津县', '4107', '41');
INSERT INTO `areas` VALUES ('410727', '封丘县', '4107', '41');
INSERT INTO `areas` VALUES ('410771', '新乡高新技术产业开发区', '4107', '41');
INSERT INTO `areas` VALUES ('410772', '新乡经济技术开发区', '4107', '41');
INSERT INTO `areas` VALUES ('410773', '新乡市平原城乡一体化示范区', '4107', '41');
INSERT INTO `areas` VALUES ('410781', '卫辉市', '4107', '41');
INSERT INTO `areas` VALUES ('410782', '辉县市', '4107', '41');
INSERT INTO `areas` VALUES ('410783', '长垣市', '4107', '41');
INSERT INTO `areas` VALUES ('410802', '解放区', '4108', '41');
INSERT INTO `areas` VALUES ('410803', '中站区', '4108', '41');
INSERT INTO `areas` VALUES ('410804', '马村区', '4108', '41');
INSERT INTO `areas` VALUES ('410811', '山阳区', '4108', '41');
INSERT INTO `areas` VALUES ('410821', '修武县', '4108', '41');
INSERT INTO `areas` VALUES ('410822', '博爱县', '4108', '41');
INSERT INTO `areas` VALUES ('410823', '武陟县', '4108', '41');
INSERT INTO `areas` VALUES ('410825', '温县', '4108', '41');
INSERT INTO `areas` VALUES ('410871', '焦作城乡一体化示范区', '4108', '41');
INSERT INTO `areas` VALUES ('410882', '沁阳市', '4108', '41');
INSERT INTO `areas` VALUES ('410883', '孟州市', '4108', '41');
INSERT INTO `areas` VALUES ('410902', '华龙区', '4109', '41');
INSERT INTO `areas` VALUES ('410922', '清丰县', '4109', '41');
INSERT INTO `areas` VALUES ('410923', '南乐县', '4109', '41');
INSERT INTO `areas` VALUES ('410926', '范县', '4109', '41');
INSERT INTO `areas` VALUES ('410927', '台前县', '4109', '41');
INSERT INTO `areas` VALUES ('410928', '濮阳县', '4109', '41');
INSERT INTO `areas` VALUES ('410971', '河南濮阳工业园区', '4109', '41');
INSERT INTO `areas` VALUES ('410972', '濮阳经济技术开发区', '4109', '41');
INSERT INTO `areas` VALUES ('411002', '魏都区', '4110', '41');
INSERT INTO `areas` VALUES ('411003', '建安区', '4110', '41');
INSERT INTO `areas` VALUES ('411024', '鄢陵县', '4110', '41');
INSERT INTO `areas` VALUES ('411025', '襄城县', '4110', '41');
INSERT INTO `areas` VALUES ('411071', '许昌经济技术开发区', '4110', '41');
INSERT INTO `areas` VALUES ('411081', '禹州市', '4110', '41');
INSERT INTO `areas` VALUES ('411082', '长葛市', '4110', '41');
INSERT INTO `areas` VALUES ('411102', '源汇区', '4111', '41');
INSERT INTO `areas` VALUES ('411103', '郾城区', '4111', '41');
INSERT INTO `areas` VALUES ('411104', '召陵区', '4111', '41');
INSERT INTO `areas` VALUES ('411121', '舞阳县', '4111', '41');
INSERT INTO `areas` VALUES ('411122', '临颍县', '4111', '41');
INSERT INTO `areas` VALUES ('411171', '漯河经济技术开发区', '4111', '41');
INSERT INTO `areas` VALUES ('411202', '湖滨区', '4112', '41');
INSERT INTO `areas` VALUES ('411203', '陕州区', '4112', '41');
INSERT INTO `areas` VALUES ('411221', '渑池县', '4112', '41');
INSERT INTO `areas` VALUES ('411224', '卢氏县', '4112', '41');
INSERT INTO `areas` VALUES ('411271', '河南三门峡经济开发区', '4112', '41');
INSERT INTO `areas` VALUES ('411281', '义马市', '4112', '41');
INSERT INTO `areas` VALUES ('411282', '灵宝市', '4112', '41');
INSERT INTO `areas` VALUES ('411302', '宛城区', '4113', '41');
INSERT INTO `areas` VALUES ('411303', '卧龙区', '4113', '41');
INSERT INTO `areas` VALUES ('411321', '南召县', '4113', '41');
INSERT INTO `areas` VALUES ('411322', '方城县', '4113', '41');
INSERT INTO `areas` VALUES ('411323', '西峡县', '4113', '41');
INSERT INTO `areas` VALUES ('411324', '镇平县', '4113', '41');
INSERT INTO `areas` VALUES ('411325', '内乡县', '4113', '41');
INSERT INTO `areas` VALUES ('411326', '淅川县', '4113', '41');
INSERT INTO `areas` VALUES ('411327', '社旗县', '4113', '41');
INSERT INTO `areas` VALUES ('411328', '唐河县', '4113', '41');
INSERT INTO `areas` VALUES ('411329', '新野县', '4113', '41');
INSERT INTO `areas` VALUES ('411330', '桐柏县', '4113', '41');
INSERT INTO `areas` VALUES ('411371', '南阳高新技术产业开发区', '4113', '41');
INSERT INTO `areas` VALUES ('411372', '南阳市城乡一体化示范区', '4113', '41');
INSERT INTO `areas` VALUES ('411381', '邓州市', '4113', '41');
INSERT INTO `areas` VALUES ('411402', '梁园区', '4114', '41');
INSERT INTO `areas` VALUES ('411403', '睢阳区', '4114', '41');
INSERT INTO `areas` VALUES ('411421', '民权县', '4114', '41');
INSERT INTO `areas` VALUES ('411422', '睢县', '4114', '41');
INSERT INTO `areas` VALUES ('411423', '宁陵县', '4114', '41');
INSERT INTO `areas` VALUES ('411424', '柘城县', '4114', '41');
INSERT INTO `areas` VALUES ('411425', '虞城县', '4114', '41');
INSERT INTO `areas` VALUES ('411426', '夏邑县', '4114', '41');
INSERT INTO `areas` VALUES ('411471', '豫东综合物流产业聚集区', '4114', '41');
INSERT INTO `areas` VALUES ('411472', '河南商丘经济开发区', '4114', '41');
INSERT INTO `areas` VALUES ('411481', '永城市', '4114', '41');
INSERT INTO `areas` VALUES ('411502', '浉河区', '4115', '41');
INSERT INTO `areas` VALUES ('411503', '平桥区', '4115', '41');
INSERT INTO `areas` VALUES ('411521', '罗山县', '4115', '41');
INSERT INTO `areas` VALUES ('411522', '光山县', '4115', '41');
INSERT INTO `areas` VALUES ('411523', '新县', '4115', '41');
INSERT INTO `areas` VALUES ('411524', '商城县', '4115', '41');
INSERT INTO `areas` VALUES ('411525', '固始县', '4115', '41');
INSERT INTO `areas` VALUES ('411526', '潢川县', '4115', '41');
INSERT INTO `areas` VALUES ('411527', '淮滨县', '4115', '41');
INSERT INTO `areas` VALUES ('411528', '息县', '4115', '41');
INSERT INTO `areas` VALUES ('411571', '信阳高新技术产业开发区', '4115', '41');
INSERT INTO `areas` VALUES ('411602', '川汇区', '4116', '41');
INSERT INTO `areas` VALUES ('411603', '淮阳区', '4116', '41');
INSERT INTO `areas` VALUES ('411621', '扶沟县', '4116', '41');
INSERT INTO `areas` VALUES ('411622', '西华县', '4116', '41');
INSERT INTO `areas` VALUES ('411623', '商水县', '4116', '41');
INSERT INTO `areas` VALUES ('411624', '沈丘县', '4116', '41');
INSERT INTO `areas` VALUES ('411625', '郸城县', '4116', '41');
INSERT INTO `areas` VALUES ('411627', '太康县', '4116', '41');
INSERT INTO `areas` VALUES ('411628', '鹿邑县', '4116', '41');
INSERT INTO `areas` VALUES ('411671', '河南周口经济开发区', '4116', '41');
INSERT INTO `areas` VALUES ('411681', '项城市', '4116', '41');
INSERT INTO `areas` VALUES ('411702', '驿城区', '4117', '41');
INSERT INTO `areas` VALUES ('411721', '西平县', '4117', '41');
INSERT INTO `areas` VALUES ('411722', '上蔡县', '4117', '41');
INSERT INTO `areas` VALUES ('411723', '平舆县', '4117', '41');
INSERT INTO `areas` VALUES ('411724', '正阳县', '4117', '41');
INSERT INTO `areas` VALUES ('411725', '确山县', '4117', '41');
INSERT INTO `areas` VALUES ('411726', '泌阳县', '4117', '41');
INSERT INTO `areas` VALUES ('411727', '汝南县', '4117', '41');
INSERT INTO `areas` VALUES ('411728', '遂平县', '4117', '41');
INSERT INTO `areas` VALUES ('411729', '新蔡县', '4117', '41');
INSERT INTO `areas` VALUES ('411771', '河南驻马店经济开发区', '4117', '41');
INSERT INTO `areas` VALUES ('419001', '济源市', '4190', '41');
INSERT INTO `areas` VALUES ('420102', '江岸区', '4201', '42');
INSERT INTO `areas` VALUES ('420103', '江汉区', '4201', '42');
INSERT INTO `areas` VALUES ('420104', '硚口区', '4201', '42');
INSERT INTO `areas` VALUES ('420105', '汉阳区', '4201', '42');
INSERT INTO `areas` VALUES ('420106', '武昌区', '4201', '42');
INSERT INTO `areas` VALUES ('420107', '青山区', '4201', '42');
INSERT INTO `areas` VALUES ('420111', '洪山区', '4201', '42');
INSERT INTO `areas` VALUES ('420112', '东西湖区', '4201', '42');
INSERT INTO `areas` VALUES ('420113', '汉南区', '4201', '42');
INSERT INTO `areas` VALUES ('420114', '蔡甸区', '4201', '42');
INSERT INTO `areas` VALUES ('420115', '江夏区', '4201', '42');
INSERT INTO `areas` VALUES ('420116', '黄陂区', '4201', '42');
INSERT INTO `areas` VALUES ('420117', '新洲区', '4201', '42');
INSERT INTO `areas` VALUES ('420202', '黄石港区', '4202', '42');
INSERT INTO `areas` VALUES ('420203', '西塞山区', '4202', '42');
INSERT INTO `areas` VALUES ('420204', '下陆区', '4202', '42');
INSERT INTO `areas` VALUES ('420205', '铁山区', '4202', '42');
INSERT INTO `areas` VALUES ('420222', '阳新县', '4202', '42');
INSERT INTO `areas` VALUES ('420281', '大冶市', '4202', '42');
INSERT INTO `areas` VALUES ('420302', '茅箭区', '4203', '42');
INSERT INTO `areas` VALUES ('420303', '张湾区', '4203', '42');
INSERT INTO `areas` VALUES ('420304', '郧阳区', '4203', '42');
INSERT INTO `areas` VALUES ('420322', '郧西县', '4203', '42');
INSERT INTO `areas` VALUES ('420323', '竹山县', '4203', '42');
INSERT INTO `areas` VALUES ('420324', '竹溪县', '4203', '42');
INSERT INTO `areas` VALUES ('420325', '房县', '4203', '42');
INSERT INTO `areas` VALUES ('420381', '丹江口市', '4203', '42');
INSERT INTO `areas` VALUES ('420502', '西陵区', '4205', '42');
INSERT INTO `areas` VALUES ('420503', '伍家岗区', '4205', '42');
INSERT INTO `areas` VALUES ('420504', '点军区', '4205', '42');
INSERT INTO `areas` VALUES ('420505', '猇亭区', '4205', '42');
INSERT INTO `areas` VALUES ('420506', '夷陵区', '4205', '42');
INSERT INTO `areas` VALUES ('420525', '远安县', '4205', '42');
INSERT INTO `areas` VALUES ('420526', '兴山县', '4205', '42');
INSERT INTO `areas` VALUES ('420527', '秭归县', '4205', '42');
INSERT INTO `areas` VALUES ('420528', '长阳土家族自治县', '4205', '42');
INSERT INTO `areas` VALUES ('420529', '五峰土家族自治县', '4205', '42');
INSERT INTO `areas` VALUES ('420581', '宜都市', '4205', '42');
INSERT INTO `areas` VALUES ('420582', '当阳市', '4205', '42');
INSERT INTO `areas` VALUES ('420583', '枝江市', '4205', '42');
INSERT INTO `areas` VALUES ('420602', '襄城区', '4206', '42');
INSERT INTO `areas` VALUES ('420606', '樊城区', '4206', '42');
INSERT INTO `areas` VALUES ('420607', '襄州区', '4206', '42');
INSERT INTO `areas` VALUES ('420624', '南漳县', '4206', '42');
INSERT INTO `areas` VALUES ('420625', '谷城县', '4206', '42');
INSERT INTO `areas` VALUES ('420626', '保康县', '4206', '42');
INSERT INTO `areas` VALUES ('420682', '老河口市', '4206', '42');
INSERT INTO `areas` VALUES ('420683', '枣阳市', '4206', '42');
INSERT INTO `areas` VALUES ('420684', '宜城市', '4206', '42');
INSERT INTO `areas` VALUES ('420702', '梁子湖区', '4207', '42');
INSERT INTO `areas` VALUES ('420703', '华容区', '4207', '42');
INSERT INTO `areas` VALUES ('420704', '鄂城区', '4207', '42');
INSERT INTO `areas` VALUES ('420802', '东宝区', '4208', '42');
INSERT INTO `areas` VALUES ('420804', '掇刀区', '4208', '42');
INSERT INTO `areas` VALUES ('420822', '沙洋县', '4208', '42');
INSERT INTO `areas` VALUES ('420881', '钟祥市', '4208', '42');
INSERT INTO `areas` VALUES ('420882', '京山市', '4208', '42');
INSERT INTO `areas` VALUES ('420902', '孝南区', '4209', '42');
INSERT INTO `areas` VALUES ('420921', '孝昌县', '4209', '42');
INSERT INTO `areas` VALUES ('420922', '大悟县', '4209', '42');
INSERT INTO `areas` VALUES ('420923', '云梦县', '4209', '42');
INSERT INTO `areas` VALUES ('420981', '应城市', '4209', '42');
INSERT INTO `areas` VALUES ('420982', '安陆市', '4209', '42');
INSERT INTO `areas` VALUES ('420984', '汉川市', '4209', '42');
INSERT INTO `areas` VALUES ('421002', '沙市区', '4210', '42');
INSERT INTO `areas` VALUES ('421003', '荆州区', '4210', '42');
INSERT INTO `areas` VALUES ('421022', '公安县', '4210', '42');
INSERT INTO `areas` VALUES ('421024', '江陵县', '4210', '42');
INSERT INTO `areas` VALUES ('421071', '荆州经济技术开发区', '4210', '42');
INSERT INTO `areas` VALUES ('421081', '石首市', '4210', '42');
INSERT INTO `areas` VALUES ('421083', '洪湖市', '4210', '42');
INSERT INTO `areas` VALUES ('421087', '松滋市', '4210', '42');
INSERT INTO `areas` VALUES ('421088', '监利市', '4210', '42');
INSERT INTO `areas` VALUES ('421102', '黄州区', '4211', '42');
INSERT INTO `areas` VALUES ('421121', '团风县', '4211', '42');
INSERT INTO `areas` VALUES ('421122', '红安县', '4211', '42');
INSERT INTO `areas` VALUES ('421123', '罗田县', '4211', '42');
INSERT INTO `areas` VALUES ('421124', '英山县', '4211', '42');
INSERT INTO `areas` VALUES ('421125', '浠水县', '4211', '42');
INSERT INTO `areas` VALUES ('421126', '蕲春县', '4211', '42');
INSERT INTO `areas` VALUES ('421127', '黄梅县', '4211', '42');
INSERT INTO `areas` VALUES ('421171', '龙感湖管理区', '4211', '42');
INSERT INTO `areas` VALUES ('421181', '麻城市', '4211', '42');
INSERT INTO `areas` VALUES ('421182', '武穴市', '4211', '42');
INSERT INTO `areas` VALUES ('421202', '咸安区', '4212', '42');
INSERT INTO `areas` VALUES ('421221', '嘉鱼县', '4212', '42');
INSERT INTO `areas` VALUES ('421222', '通城县', '4212', '42');
INSERT INTO `areas` VALUES ('421223', '崇阳县', '4212', '42');
INSERT INTO `areas` VALUES ('421224', '通山县', '4212', '42');
INSERT INTO `areas` VALUES ('421281', '赤壁市', '4212', '42');
INSERT INTO `areas` VALUES ('421303', '曾都区', '4213', '42');
INSERT INTO `areas` VALUES ('421321', '随县', '4213', '42');
INSERT INTO `areas` VALUES ('421381', '广水市', '4213', '42');
INSERT INTO `areas` VALUES ('422801', '恩施市', '4228', '42');
INSERT INTO `areas` VALUES ('422802', '利川市', '4228', '42');
INSERT INTO `areas` VALUES ('422822', '建始县', '4228', '42');
INSERT INTO `areas` VALUES ('422823', '巴东县', '4228', '42');
INSERT INTO `areas` VALUES ('422825', '宣恩县', '4228', '42');
INSERT INTO `areas` VALUES ('422826', '咸丰县', '4228', '42');
INSERT INTO `areas` VALUES ('422827', '来凤县', '4228', '42');
INSERT INTO `areas` VALUES ('422828', '鹤峰县', '4228', '42');
INSERT INTO `areas` VALUES ('429004', '仙桃市', '4290', '42');
INSERT INTO `areas` VALUES ('429005', '潜江市', '4290', '42');
INSERT INTO `areas` VALUES ('429006', '天门市', '4290', '42');
INSERT INTO `areas` VALUES ('429021', '神农架林区', '4290', '42');
INSERT INTO `areas` VALUES ('430102', '芙蓉区', '4301', '43');
INSERT INTO `areas` VALUES ('430103', '天心区', '4301', '43');
INSERT INTO `areas` VALUES ('430104', '岳麓区', '4301', '43');
INSERT INTO `areas` VALUES ('430105', '开福区', '4301', '43');
INSERT INTO `areas` VALUES ('430111', '雨花区', '4301', '43');
INSERT INTO `areas` VALUES ('430112', '望城区', '4301', '43');
INSERT INTO `areas` VALUES ('430121', '长沙县', '4301', '43');
INSERT INTO `areas` VALUES ('430181', '浏阳市', '4301', '43');
INSERT INTO `areas` VALUES ('430182', '宁乡市', '4301', '43');
INSERT INTO `areas` VALUES ('430202', '荷塘区', '4302', '43');
INSERT INTO `areas` VALUES ('430203', '芦淞区', '4302', '43');
INSERT INTO `areas` VALUES ('430204', '石峰区', '4302', '43');
INSERT INTO `areas` VALUES ('430211', '天元区', '4302', '43');
INSERT INTO `areas` VALUES ('430212', '渌口区', '4302', '43');
INSERT INTO `areas` VALUES ('430223', '攸县', '4302', '43');
INSERT INTO `areas` VALUES ('430224', '茶陵县', '4302', '43');
INSERT INTO `areas` VALUES ('430225', '炎陵县', '4302', '43');
INSERT INTO `areas` VALUES ('430271', '云龙示范区', '4302', '43');
INSERT INTO `areas` VALUES ('430281', '醴陵市', '4302', '43');
INSERT INTO `areas` VALUES ('430302', '雨湖区', '4303', '43');
INSERT INTO `areas` VALUES ('430304', '岳塘区', '4303', '43');
INSERT INTO `areas` VALUES ('430321', '湘潭县', '4303', '43');
INSERT INTO `areas` VALUES ('430371', '湖南湘潭高新技术产业园区', '4303', '43');
INSERT INTO `areas` VALUES ('430372', '湘潭昭山示范区', '4303', '43');
INSERT INTO `areas` VALUES ('430373', '湘潭九华示范区', '4303', '43');
INSERT INTO `areas` VALUES ('430381', '湘乡市', '4303', '43');
INSERT INTO `areas` VALUES ('430382', '韶山市', '4303', '43');
INSERT INTO `areas` VALUES ('430405', '珠晖区', '4304', '43');
INSERT INTO `areas` VALUES ('430406', '雁峰区', '4304', '43');
INSERT INTO `areas` VALUES ('430407', '石鼓区', '4304', '43');
INSERT INTO `areas` VALUES ('430408', '蒸湘区', '4304', '43');
INSERT INTO `areas` VALUES ('430412', '南岳区', '4304', '43');
INSERT INTO `areas` VALUES ('430421', '衡阳县', '4304', '43');
INSERT INTO `areas` VALUES ('430422', '衡南县', '4304', '43');
INSERT INTO `areas` VALUES ('430423', '衡山县', '4304', '43');
INSERT INTO `areas` VALUES ('430424', '衡东县', '4304', '43');
INSERT INTO `areas` VALUES ('430426', '祁东县', '4304', '43');
INSERT INTO `areas` VALUES ('430471', '衡阳综合保税区', '4304', '43');
INSERT INTO `areas` VALUES ('430472', '湖南衡阳高新技术产业园区', '4304', '43');
INSERT INTO `areas` VALUES ('430473', '湖南衡阳松木经济开发区', '4304', '43');
INSERT INTO `areas` VALUES ('430481', '耒阳市', '4304', '43');
INSERT INTO `areas` VALUES ('430482', '常宁市', '4304', '43');
INSERT INTO `areas` VALUES ('430502', '双清区', '4305', '43');
INSERT INTO `areas` VALUES ('430503', '大祥区', '4305', '43');
INSERT INTO `areas` VALUES ('430511', '北塔区', '4305', '43');
INSERT INTO `areas` VALUES ('430522', '新邵县', '4305', '43');
INSERT INTO `areas` VALUES ('430523', '邵阳县', '4305', '43');
INSERT INTO `areas` VALUES ('430524', '隆回县', '4305', '43');
INSERT INTO `areas` VALUES ('430525', '洞口县', '4305', '43');
INSERT INTO `areas` VALUES ('430527', '绥宁县', '4305', '43');
INSERT INTO `areas` VALUES ('430528', '新宁县', '4305', '43');
INSERT INTO `areas` VALUES ('430529', '城步苗族自治县', '4305', '43');
INSERT INTO `areas` VALUES ('430581', '武冈市', '4305', '43');
INSERT INTO `areas` VALUES ('430582', '邵东市', '4305', '43');
INSERT INTO `areas` VALUES ('430602', '岳阳楼区', '4306', '43');
INSERT INTO `areas` VALUES ('430603', '云溪区', '4306', '43');
INSERT INTO `areas` VALUES ('430611', '君山区', '4306', '43');
INSERT INTO `areas` VALUES ('430621', '岳阳县', '4306', '43');
INSERT INTO `areas` VALUES ('430623', '华容县', '4306', '43');
INSERT INTO `areas` VALUES ('430624', '湘阴县', '4306', '43');
INSERT INTO `areas` VALUES ('430626', '平江县', '4306', '43');
INSERT INTO `areas` VALUES ('430671', '岳阳市屈原管理区', '4306', '43');
INSERT INTO `areas` VALUES ('430681', '汨罗市', '4306', '43');
INSERT INTO `areas` VALUES ('430682', '临湘市', '4306', '43');
INSERT INTO `areas` VALUES ('430702', '武陵区', '4307', '43');
INSERT INTO `areas` VALUES ('430703', '鼎城区', '4307', '43');
INSERT INTO `areas` VALUES ('430721', '安乡县', '4307', '43');
INSERT INTO `areas` VALUES ('430722', '汉寿县', '4307', '43');
INSERT INTO `areas` VALUES ('430723', '澧县', '4307', '43');
INSERT INTO `areas` VALUES ('430724', '临澧县', '4307', '43');
INSERT INTO `areas` VALUES ('430725', '桃源县', '4307', '43');
INSERT INTO `areas` VALUES ('430726', '石门县', '4307', '43');
INSERT INTO `areas` VALUES ('430771', '常德市西洞庭管理区', '4307', '43');
INSERT INTO `areas` VALUES ('430781', '津市市', '4307', '43');
INSERT INTO `areas` VALUES ('430802', '永定区', '4308', '43');
INSERT INTO `areas` VALUES ('430811', '武陵源区', '4308', '43');
INSERT INTO `areas` VALUES ('430821', '慈利县', '4308', '43');
INSERT INTO `areas` VALUES ('430822', '桑植县', '4308', '43');
INSERT INTO `areas` VALUES ('430902', '资阳区', '4309', '43');
INSERT INTO `areas` VALUES ('430903', '赫山区', '4309', '43');
INSERT INTO `areas` VALUES ('430921', '南县', '4309', '43');
INSERT INTO `areas` VALUES ('430922', '桃江县', '4309', '43');
INSERT INTO `areas` VALUES ('430923', '安化县', '4309', '43');
INSERT INTO `areas` VALUES ('430971', '益阳市大通湖管理区', '4309', '43');
INSERT INTO `areas` VALUES ('430972', '湖南益阳高新技术产业园区', '4309', '43');
INSERT INTO `areas` VALUES ('430981', '沅江市', '4309', '43');
INSERT INTO `areas` VALUES ('431002', '北湖区', '4310', '43');
INSERT INTO `areas` VALUES ('431003', '苏仙区', '4310', '43');
INSERT INTO `areas` VALUES ('431021', '桂阳县', '4310', '43');
INSERT INTO `areas` VALUES ('431022', '宜章县', '4310', '43');
INSERT INTO `areas` VALUES ('431023', '永兴县', '4310', '43');
INSERT INTO `areas` VALUES ('431024', '嘉禾县', '4310', '43');
INSERT INTO `areas` VALUES ('431025', '临武县', '4310', '43');
INSERT INTO `areas` VALUES ('431026', '汝城县', '4310', '43');
INSERT INTO `areas` VALUES ('431027', '桂东县', '4310', '43');
INSERT INTO `areas` VALUES ('431028', '安仁县', '4310', '43');
INSERT INTO `areas` VALUES ('431081', '资兴市', '4310', '43');
INSERT INTO `areas` VALUES ('431102', '零陵区', '4311', '43');
INSERT INTO `areas` VALUES ('431103', '冷水滩区', '4311', '43');
INSERT INTO `areas` VALUES ('431122', '东安县', '4311', '43');
INSERT INTO `areas` VALUES ('431123', '双牌县', '4311', '43');
INSERT INTO `areas` VALUES ('431124', '道县', '4311', '43');
INSERT INTO `areas` VALUES ('431125', '江永县', '4311', '43');
INSERT INTO `areas` VALUES ('431126', '宁远县', '4311', '43');
INSERT INTO `areas` VALUES ('431127', '蓝山县', '4311', '43');
INSERT INTO `areas` VALUES ('431128', '新田县', '4311', '43');
INSERT INTO `areas` VALUES ('431129', '江华瑶族自治县', '4311', '43');
INSERT INTO `areas` VALUES ('431171', '永州经济技术开发区', '4311', '43');
INSERT INTO `areas` VALUES ('431173', '永州市回龙圩管理区', '4311', '43');
INSERT INTO `areas` VALUES ('431181', '祁阳市', '4311', '43');
INSERT INTO `areas` VALUES ('431202', '鹤城区', '4312', '43');
INSERT INTO `areas` VALUES ('431221', '中方县', '4312', '43');
INSERT INTO `areas` VALUES ('431222', '沅陵县', '4312', '43');
INSERT INTO `areas` VALUES ('431223', '辰溪县', '4312', '43');
INSERT INTO `areas` VALUES ('431224', '溆浦县', '4312', '43');
INSERT INTO `areas` VALUES ('431225', '会同县', '4312', '43');
INSERT INTO `areas` VALUES ('431226', '麻阳苗族自治县', '4312', '43');
INSERT INTO `areas` VALUES ('431227', '新晃侗族自治县', '4312', '43');
INSERT INTO `areas` VALUES ('431228', '芷江侗族自治县', '4312', '43');
INSERT INTO `areas` VALUES ('431229', '靖州苗族侗族自治县', '4312', '43');
INSERT INTO `areas` VALUES ('431230', '通道侗族自治县', '4312', '43');
INSERT INTO `areas` VALUES ('431271', '怀化市洪江管理区', '4312', '43');
INSERT INTO `areas` VALUES ('431281', '洪江市', '4312', '43');
INSERT INTO `areas` VALUES ('431302', '娄星区', '4313', '43');
INSERT INTO `areas` VALUES ('431321', '双峰县', '4313', '43');
INSERT INTO `areas` VALUES ('431322', '新化县', '4313', '43');
INSERT INTO `areas` VALUES ('431381', '冷水江市', '4313', '43');
INSERT INTO `areas` VALUES ('431382', '涟源市', '4313', '43');
INSERT INTO `areas` VALUES ('433101', '吉首市', '4331', '43');
INSERT INTO `areas` VALUES ('433122', '泸溪县', '4331', '43');
INSERT INTO `areas` VALUES ('433123', '凤凰县', '4331', '43');
INSERT INTO `areas` VALUES ('433124', '花垣县', '4331', '43');
INSERT INTO `areas` VALUES ('433125', '保靖县', '4331', '43');
INSERT INTO `areas` VALUES ('433126', '古丈县', '4331', '43');
INSERT INTO `areas` VALUES ('433127', '永顺县', '4331', '43');
INSERT INTO `areas` VALUES ('433130', '龙山县', '4331', '43');
INSERT INTO `areas` VALUES ('440103', '荔湾区', '4401', '44');
INSERT INTO `areas` VALUES ('440104', '越秀区', '4401', '44');
INSERT INTO `areas` VALUES ('440105', '海珠区', '4401', '44');
INSERT INTO `areas` VALUES ('440106', '天河区', '4401', '44');
INSERT INTO `areas` VALUES ('440111', '白云区', '4401', '44');
INSERT INTO `areas` VALUES ('440112', '黄埔区', '4401', '44');
INSERT INTO `areas` VALUES ('440113', '番禺区', '4401', '44');
INSERT INTO `areas` VALUES ('440114', '花都区', '4401', '44');
INSERT INTO `areas` VALUES ('440115', '南沙区', '4401', '44');
INSERT INTO `areas` VALUES ('440117', '从化区', '4401', '44');
INSERT INTO `areas` VALUES ('440118', '增城区', '4401', '44');
INSERT INTO `areas` VALUES ('440203', '武江区', '4402', '44');
INSERT INTO `areas` VALUES ('440204', '浈江区', '4402', '44');
INSERT INTO `areas` VALUES ('440205', '曲江区', '4402', '44');
INSERT INTO `areas` VALUES ('440222', '始兴县', '4402', '44');
INSERT INTO `areas` VALUES ('440224', '仁化县', '4402', '44');
INSERT INTO `areas` VALUES ('440229', '翁源县', '4402', '44');
INSERT INTO `areas` VALUES ('440232', '乳源瑶族自治县', '4402', '44');
INSERT INTO `areas` VALUES ('440233', '新丰县', '4402', '44');
INSERT INTO `areas` VALUES ('440281', '乐昌市', '4402', '44');
INSERT INTO `areas` VALUES ('440282', '南雄市', '4402', '44');
INSERT INTO `areas` VALUES ('440303', '罗湖区', '4403', '44');
INSERT INTO `areas` VALUES ('440304', '福田区', '4403', '44');
INSERT INTO `areas` VALUES ('440305', '南山区', '4403', '44');
INSERT INTO `areas` VALUES ('440306', '宝安区', '4403', '44');
INSERT INTO `areas` VALUES ('440307', '龙岗区', '4403', '44');
INSERT INTO `areas` VALUES ('440308', '盐田区', '4403', '44');
INSERT INTO `areas` VALUES ('440309', '龙华区', '4403', '44');
INSERT INTO `areas` VALUES ('440310', '坪山区', '4403', '44');
INSERT INTO `areas` VALUES ('440311', '光明区', '4403', '44');
INSERT INTO `areas` VALUES ('440402', '香洲区', '4404', '44');
INSERT INTO `areas` VALUES ('440403', '斗门区', '4404', '44');
INSERT INTO `areas` VALUES ('440404', '金湾区', '4404', '44');
INSERT INTO `areas` VALUES ('440507', '龙湖区', '4405', '44');
INSERT INTO `areas` VALUES ('440511', '金平区', '4405', '44');
INSERT INTO `areas` VALUES ('440512', '濠江区', '4405', '44');
INSERT INTO `areas` VALUES ('440513', '潮阳区', '4405', '44');
INSERT INTO `areas` VALUES ('440514', '潮南区', '4405', '44');
INSERT INTO `areas` VALUES ('440515', '澄海区', '4405', '44');
INSERT INTO `areas` VALUES ('440523', '南澳县', '4405', '44');
INSERT INTO `areas` VALUES ('440604', '禅城区', '4406', '44');
INSERT INTO `areas` VALUES ('440605', '南海区', '4406', '44');
INSERT INTO `areas` VALUES ('440606', '顺德区', '4406', '44');
INSERT INTO `areas` VALUES ('440607', '三水区', '4406', '44');
INSERT INTO `areas` VALUES ('440608', '高明区', '4406', '44');
INSERT INTO `areas` VALUES ('440703', '蓬江区', '4407', '44');
INSERT INTO `areas` VALUES ('440704', '江海区', '4407', '44');
INSERT INTO `areas` VALUES ('440705', '新会区', '4407', '44');
INSERT INTO `areas` VALUES ('440781', '台山市', '4407', '44');
INSERT INTO `areas` VALUES ('440783', '开平市', '4407', '44');
INSERT INTO `areas` VALUES ('440784', '鹤山市', '4407', '44');
INSERT INTO `areas` VALUES ('440785', '恩平市', '4407', '44');
INSERT INTO `areas` VALUES ('440802', '赤坎区', '4408', '44');
INSERT INTO `areas` VALUES ('440803', '霞山区', '4408', '44');
INSERT INTO `areas` VALUES ('440804', '坡头区', '4408', '44');
INSERT INTO `areas` VALUES ('440811', '麻章区', '4408', '44');
INSERT INTO `areas` VALUES ('440823', '遂溪县', '4408', '44');
INSERT INTO `areas` VALUES ('440825', '徐闻县', '4408', '44');
INSERT INTO `areas` VALUES ('440881', '廉江市', '4408', '44');
INSERT INTO `areas` VALUES ('440882', '雷州市', '4408', '44');
INSERT INTO `areas` VALUES ('440883', '吴川市', '4408', '44');
INSERT INTO `areas` VALUES ('440902', '茂南区', '4409', '44');
INSERT INTO `areas` VALUES ('440904', '电白区', '4409', '44');
INSERT INTO `areas` VALUES ('440981', '高州市', '4409', '44');
INSERT INTO `areas` VALUES ('440982', '化州市', '4409', '44');
INSERT INTO `areas` VALUES ('440983', '信宜市', '4409', '44');
INSERT INTO `areas` VALUES ('441202', '端州区', '4412', '44');
INSERT INTO `areas` VALUES ('441203', '鼎湖区', '4412', '44');
INSERT INTO `areas` VALUES ('441204', '高要区', '4412', '44');
INSERT INTO `areas` VALUES ('441223', '广宁县', '4412', '44');
INSERT INTO `areas` VALUES ('441224', '怀集县', '4412', '44');
INSERT INTO `areas` VALUES ('441225', '封开县', '4412', '44');
INSERT INTO `areas` VALUES ('441226', '德庆县', '4412', '44');
INSERT INTO `areas` VALUES ('441284', '四会市', '4412', '44');
INSERT INTO `areas` VALUES ('441302', '惠城区', '4413', '44');
INSERT INTO `areas` VALUES ('441303', '惠阳区', '4413', '44');
INSERT INTO `areas` VALUES ('441322', '博罗县', '4413', '44');
INSERT INTO `areas` VALUES ('441323', '惠东县', '4413', '44');
INSERT INTO `areas` VALUES ('441324', '龙门县', '4413', '44');
INSERT INTO `areas` VALUES ('441402', '梅江区', '4414', '44');
INSERT INTO `areas` VALUES ('441403', '梅县区', '4414', '44');
INSERT INTO `areas` VALUES ('441422', '大埔县', '4414', '44');
INSERT INTO `areas` VALUES ('441423', '丰顺县', '4414', '44');
INSERT INTO `areas` VALUES ('441424', '五华县', '4414', '44');
INSERT INTO `areas` VALUES ('441426', '平远县', '4414', '44');
INSERT INTO `areas` VALUES ('441427', '蕉岭县', '4414', '44');
INSERT INTO `areas` VALUES ('441481', '兴宁市', '4414', '44');
INSERT INTO `areas` VALUES ('441502', '城区', '4415', '44');
INSERT INTO `areas` VALUES ('441521', '海丰县', '4415', '44');
INSERT INTO `areas` VALUES ('441523', '陆河县', '4415', '44');
INSERT INTO `areas` VALUES ('441581', '陆丰市', '4415', '44');
INSERT INTO `areas` VALUES ('441602', '源城区', '4416', '44');
INSERT INTO `areas` VALUES ('441621', '紫金县', '4416', '44');
INSERT INTO `areas` VALUES ('441622', '龙川县', '4416', '44');
INSERT INTO `areas` VALUES ('441623', '连平县', '4416', '44');
INSERT INTO `areas` VALUES ('441624', '和平县', '4416', '44');
INSERT INTO `areas` VALUES ('441625', '东源县', '4416', '44');
INSERT INTO `areas` VALUES ('441702', '江城区', '4417', '44');
INSERT INTO `areas` VALUES ('441704', '阳东区', '4417', '44');
INSERT INTO `areas` VALUES ('441721', '阳西县', '4417', '44');
INSERT INTO `areas` VALUES ('441781', '阳春市', '4417', '44');
INSERT INTO `areas` VALUES ('441802', '清城区', '4418', '44');
INSERT INTO `areas` VALUES ('441803', '清新区', '4418', '44');
INSERT INTO `areas` VALUES ('441821', '佛冈县', '4418', '44');
INSERT INTO `areas` VALUES ('441823', '阳山县', '4418', '44');
INSERT INTO `areas` VALUES ('441825', '连山壮族瑶族自治县', '4418', '44');
INSERT INTO `areas` VALUES ('441826', '连南瑶族自治县', '4418', '44');
INSERT INTO `areas` VALUES ('441881', '英德市', '4418', '44');
INSERT INTO `areas` VALUES ('441882', '连州市', '4418', '44');
INSERT INTO `areas` VALUES ('441900', '东莞市', '4419', '44');
INSERT INTO `areas` VALUES ('442000', '中山市', '4420', '44');
INSERT INTO `areas` VALUES ('445102', '湘桥区', '4451', '44');
INSERT INTO `areas` VALUES ('445103', '潮安区', '4451', '44');
INSERT INTO `areas` VALUES ('445122', '饶平县', '4451', '44');
INSERT INTO `areas` VALUES ('445202', '榕城区', '4452', '44');
INSERT INTO `areas` VALUES ('445203', '揭东区', '4452', '44');
INSERT INTO `areas` VALUES ('445222', '揭西县', '4452', '44');
INSERT INTO `areas` VALUES ('445224', '惠来县', '4452', '44');
INSERT INTO `areas` VALUES ('445281', '普宁市', '4452', '44');
INSERT INTO `areas` VALUES ('445302', '云城区', '4453', '44');
INSERT INTO `areas` VALUES ('445303', '云安区', '4453', '44');
INSERT INTO `areas` VALUES ('445321', '新兴县', '4453', '44');
INSERT INTO `areas` VALUES ('445322', '郁南县', '4453', '44');
INSERT INTO `areas` VALUES ('445381', '罗定市', '4453', '44');
INSERT INTO `areas` VALUES ('450102', '兴宁区', '4501', '45');
INSERT INTO `areas` VALUES ('450103', '青秀区', '4501', '45');
INSERT INTO `areas` VALUES ('450105', '江南区', '4501', '45');
INSERT INTO `areas` VALUES ('450107', '西乡塘区', '4501', '45');
INSERT INTO `areas` VALUES ('450108', '良庆区', '4501', '45');
INSERT INTO `areas` VALUES ('450109', '邕宁区', '4501', '45');
INSERT INTO `areas` VALUES ('450110', '武鸣区', '4501', '45');
INSERT INTO `areas` VALUES ('450123', '隆安县', '4501', '45');
INSERT INTO `areas` VALUES ('450124', '马山县', '4501', '45');
INSERT INTO `areas` VALUES ('450125', '上林县', '4501', '45');
INSERT INTO `areas` VALUES ('450126', '宾阳县', '4501', '45');
INSERT INTO `areas` VALUES ('450181', '横州市', '4501', '45');
INSERT INTO `areas` VALUES ('450202', '城中区', '4502', '45');
INSERT INTO `areas` VALUES ('450203', '鱼峰区', '4502', '45');
INSERT INTO `areas` VALUES ('450204', '柳南区', '4502', '45');
INSERT INTO `areas` VALUES ('450205', '柳北区', '4502', '45');
INSERT INTO `areas` VALUES ('450206', '柳江区', '4502', '45');
INSERT INTO `areas` VALUES ('450222', '柳城县', '4502', '45');
INSERT INTO `areas` VALUES ('450223', '鹿寨县', '4502', '45');
INSERT INTO `areas` VALUES ('450224', '融安县', '4502', '45');
INSERT INTO `areas` VALUES ('450225', '融水苗族自治县', '4502', '45');
INSERT INTO `areas` VALUES ('450226', '三江侗族自治县', '4502', '45');
INSERT INTO `areas` VALUES ('450302', '秀峰区', '4503', '45');
INSERT INTO `areas` VALUES ('450303', '叠彩区', '4503', '45');
INSERT INTO `areas` VALUES ('450304', '象山区', '4503', '45');
INSERT INTO `areas` VALUES ('450305', '七星区', '4503', '45');
INSERT INTO `areas` VALUES ('450311', '雁山区', '4503', '45');
INSERT INTO `areas` VALUES ('450312', '临桂区', '4503', '45');
INSERT INTO `areas` VALUES ('450321', '阳朔县', '4503', '45');
INSERT INTO `areas` VALUES ('450323', '灵川县', '4503', '45');
INSERT INTO `areas` VALUES ('450324', '全州县', '4503', '45');
INSERT INTO `areas` VALUES ('450325', '兴安县', '4503', '45');
INSERT INTO `areas` VALUES ('450326', '永福县', '4503', '45');
INSERT INTO `areas` VALUES ('450327', '灌阳县', '4503', '45');
INSERT INTO `areas` VALUES ('450328', '龙胜各族自治县', '4503', '45');
INSERT INTO `areas` VALUES ('450329', '资源县', '4503', '45');
INSERT INTO `areas` VALUES ('450330', '平乐县', '4503', '45');
INSERT INTO `areas` VALUES ('450332', '恭城瑶族自治县', '4503', '45');
INSERT INTO `areas` VALUES ('450381', '荔浦市', '4503', '45');
INSERT INTO `areas` VALUES ('450403', '万秀区', '4504', '45');
INSERT INTO `areas` VALUES ('450405', '长洲区', '4504', '45');
INSERT INTO `areas` VALUES ('450406', '龙圩区', '4504', '45');
INSERT INTO `areas` VALUES ('450421', '苍梧县', '4504', '45');
INSERT INTO `areas` VALUES ('450422', '藤县', '4504', '45');
INSERT INTO `areas` VALUES ('450423', '蒙山县', '4504', '45');
INSERT INTO `areas` VALUES ('450481', '岑溪市', '4504', '45');
INSERT INTO `areas` VALUES ('450502', '海城区', '4505', '45');
INSERT INTO `areas` VALUES ('450503', '银海区', '4505', '45');
INSERT INTO `areas` VALUES ('450512', '铁山港区', '4505', '45');
INSERT INTO `areas` VALUES ('450521', '合浦县', '4505', '45');
INSERT INTO `areas` VALUES ('450602', '港口区', '4506', '45');
INSERT INTO `areas` VALUES ('450603', '防城区', '4506', '45');
INSERT INTO `areas` VALUES ('450621', '上思县', '4506', '45');
INSERT INTO `areas` VALUES ('450681', '东兴市', '4506', '45');
INSERT INTO `areas` VALUES ('450702', '钦南区', '4507', '45');
INSERT INTO `areas` VALUES ('450703', '钦北区', '4507', '45');
INSERT INTO `areas` VALUES ('450721', '灵山县', '4507', '45');
INSERT INTO `areas` VALUES ('450722', '浦北县', '4507', '45');
INSERT INTO `areas` VALUES ('450802', '港北区', '4508', '45');
INSERT INTO `areas` VALUES ('450803', '港南区', '4508', '45');
INSERT INTO `areas` VALUES ('450804', '覃塘区', '4508', '45');
INSERT INTO `areas` VALUES ('450821', '平南县', '4508', '45');
INSERT INTO `areas` VALUES ('450881', '桂平市', '4508', '45');
INSERT INTO `areas` VALUES ('450902', '玉州区', '4509', '45');
INSERT INTO `areas` VALUES ('450903', '福绵区', '4509', '45');
INSERT INTO `areas` VALUES ('450921', '容县', '4509', '45');
INSERT INTO `areas` VALUES ('450922', '陆川县', '4509', '45');
INSERT INTO `areas` VALUES ('450923', '博白县', '4509', '45');
INSERT INTO `areas` VALUES ('450924', '兴业县', '4509', '45');
INSERT INTO `areas` VALUES ('450981', '北流市', '4509', '45');
INSERT INTO `areas` VALUES ('451002', '右江区', '4510', '45');
INSERT INTO `areas` VALUES ('451003', '田阳区', '4510', '45');
INSERT INTO `areas` VALUES ('451022', '田东县', '4510', '45');
INSERT INTO `areas` VALUES ('451024', '德保县', '4510', '45');
INSERT INTO `areas` VALUES ('451026', '那坡县', '4510', '45');
INSERT INTO `areas` VALUES ('451027', '凌云县', '4510', '45');
INSERT INTO `areas` VALUES ('451028', '乐业县', '4510', '45');
INSERT INTO `areas` VALUES ('451029', '田林县', '4510', '45');
INSERT INTO `areas` VALUES ('451030', '西林县', '4510', '45');
INSERT INTO `areas` VALUES ('451031', '隆林各族自治县', '4510', '45');
INSERT INTO `areas` VALUES ('451081', '靖西市', '4510', '45');
INSERT INTO `areas` VALUES ('451082', '平果市', '4510', '45');
INSERT INTO `areas` VALUES ('451102', '八步区', '4511', '45');
INSERT INTO `areas` VALUES ('451103', '平桂区', '4511', '45');
INSERT INTO `areas` VALUES ('451121', '昭平县', '4511', '45');
INSERT INTO `areas` VALUES ('451122', '钟山县', '4511', '45');
INSERT INTO `areas` VALUES ('451123', '富川瑶族自治县', '4511', '45');
INSERT INTO `areas` VALUES ('451202', '金城江区', '4512', '45');
INSERT INTO `areas` VALUES ('451203', '宜州区', '4512', '45');
INSERT INTO `areas` VALUES ('451221', '南丹县', '4512', '45');
INSERT INTO `areas` VALUES ('451222', '天峨县', '4512', '45');
INSERT INTO `areas` VALUES ('451223', '凤山县', '4512', '45');
INSERT INTO `areas` VALUES ('451224', '东兰县', '4512', '45');
INSERT INTO `areas` VALUES ('451225', '罗城仫佬族自治县', '4512', '45');
INSERT INTO `areas` VALUES ('451226', '环江毛南族自治县', '4512', '45');
INSERT INTO `areas` VALUES ('451227', '巴马瑶族自治县', '4512', '45');
INSERT INTO `areas` VALUES ('451228', '都安瑶族自治县', '4512', '45');
INSERT INTO `areas` VALUES ('451229', '大化瑶族自治县', '4512', '45');
INSERT INTO `areas` VALUES ('451302', '兴宾区', '4513', '45');
INSERT INTO `areas` VALUES ('451321', '忻城县', '4513', '45');
INSERT INTO `areas` VALUES ('451322', '象州县', '4513', '45');
INSERT INTO `areas` VALUES ('451323', '武宣县', '4513', '45');
INSERT INTO `areas` VALUES ('451324', '金秀瑶族自治县', '4513', '45');
INSERT INTO `areas` VALUES ('451381', '合山市', '4513', '45');
INSERT INTO `areas` VALUES ('451402', '江州区', '4514', '45');
INSERT INTO `areas` VALUES ('451421', '扶绥县', '4514', '45');
INSERT INTO `areas` VALUES ('451422', '宁明县', '4514', '45');
INSERT INTO `areas` VALUES ('451423', '龙州县', '4514', '45');
INSERT INTO `areas` VALUES ('451424', '大新县', '4514', '45');
INSERT INTO `areas` VALUES ('451425', '天等县', '4514', '45');
INSERT INTO `areas` VALUES ('451481', '凭祥市', '4514', '45');
INSERT INTO `areas` VALUES ('460105', '秀英区', '4601', '46');
INSERT INTO `areas` VALUES ('460106', '龙华区', '4601', '46');
INSERT INTO `areas` VALUES ('460107', '琼山区', '4601', '46');
INSERT INTO `areas` VALUES ('460108', '美兰区', '4601', '46');
INSERT INTO `areas` VALUES ('460202', '海棠区', '4602', '46');
INSERT INTO `areas` VALUES ('460203', '吉阳区', '4602', '46');
INSERT INTO `areas` VALUES ('460204', '天涯区', '4602', '46');
INSERT INTO `areas` VALUES ('460205', '崖州区', '4602', '46');
INSERT INTO `areas` VALUES ('460321', '西沙群岛', '4603', '46');
INSERT INTO `areas` VALUES ('460322', '南沙群岛', '4603', '46');
INSERT INTO `areas` VALUES ('460323', '中沙群岛的岛礁及其海域', '4603', '46');
INSERT INTO `areas` VALUES ('460400', '儋州市', '4604', '46');
INSERT INTO `areas` VALUES ('469001', '五指山市', '4690', '46');
INSERT INTO `areas` VALUES ('469002', '琼海市', '4690', '46');
INSERT INTO `areas` VALUES ('469005', '文昌市', '4690', '46');
INSERT INTO `areas` VALUES ('469006', '万宁市', '4690', '46');
INSERT INTO `areas` VALUES ('469007', '东方市', '4690', '46');
INSERT INTO `areas` VALUES ('469021', '定安县', '4690', '46');
INSERT INTO `areas` VALUES ('469022', '屯昌县', '4690', '46');
INSERT INTO `areas` VALUES ('469023', '澄迈县', '4690', '46');
INSERT INTO `areas` VALUES ('469024', '临高县', '4690', '46');
INSERT INTO `areas` VALUES ('469025', '白沙黎族自治县', '4690', '46');
INSERT INTO `areas` VALUES ('469026', '昌江黎族自治县', '4690', '46');
INSERT INTO `areas` VALUES ('469027', '乐东黎族自治县', '4690', '46');
INSERT INTO `areas` VALUES ('469028', '陵水黎族自治县', '4690', '46');
INSERT INTO `areas` VALUES ('469029', '保亭黎族苗族自治县', '4690', '46');
INSERT INTO `areas` VALUES ('469030', '琼中黎族苗族自治县', '4690', '46');
INSERT INTO `areas` VALUES ('500101', '万州区', '5001', '50');
INSERT INTO `areas` VALUES ('500102', '涪陵区', '5001', '50');
INSERT INTO `areas` VALUES ('500103', '渝中区', '5001', '50');
INSERT INTO `areas` VALUES ('500104', '大渡口区', '5001', '50');
INSERT INTO `areas` VALUES ('500105', '江北区', '5001', '50');
INSERT INTO `areas` VALUES ('500106', '沙坪坝区', '5001', '50');
INSERT INTO `areas` VALUES ('500107', '九龙坡区', '5001', '50');
INSERT INTO `areas` VALUES ('500108', '南岸区', '5001', '50');
INSERT INTO `areas` VALUES ('500109', '北碚区', '5001', '50');
INSERT INTO `areas` VALUES ('500110', '綦江区', '5001', '50');
INSERT INTO `areas` VALUES ('500111', '大足区', '5001', '50');
INSERT INTO `areas` VALUES ('500112', '渝北区', '5001', '50');
INSERT INTO `areas` VALUES ('500113', '巴南区', '5001', '50');
INSERT INTO `areas` VALUES ('500114', '黔江区', '5001', '50');
INSERT INTO `areas` VALUES ('500115', '长寿区', '5001', '50');
INSERT INTO `areas` VALUES ('500116', '江津区', '5001', '50');
INSERT INTO `areas` VALUES ('500117', '合川区', '5001', '50');
INSERT INTO `areas` VALUES ('500118', '永川区', '5001', '50');
INSERT INTO `areas` VALUES ('500119', '南川区', '5001', '50');
INSERT INTO `areas` VALUES ('500120', '璧山区', '5001', '50');
INSERT INTO `areas` VALUES ('500151', '铜梁区', '5001', '50');
INSERT INTO `areas` VALUES ('500152', '潼南区', '5001', '50');
INSERT INTO `areas` VALUES ('500153', '荣昌区', '5001', '50');
INSERT INTO `areas` VALUES ('500154', '开州区', '5001', '50');
INSERT INTO `areas` VALUES ('500155', '梁平区', '5001', '50');
INSERT INTO `areas` VALUES ('500156', '武隆区', '5001', '50');
INSERT INTO `areas` VALUES ('500229', '城口县', '5002', '50');
INSERT INTO `areas` VALUES ('500230', '丰都县', '5002', '50');
INSERT INTO `areas` VALUES ('500231', '垫江县', '5002', '50');
INSERT INTO `areas` VALUES ('500233', '忠县', '5002', '50');
INSERT INTO `areas` VALUES ('500235', '云阳县', '5002', '50');
INSERT INTO `areas` VALUES ('500236', '奉节县', '5002', '50');
INSERT INTO `areas` VALUES ('500237', '巫山县', '5002', '50');
INSERT INTO `areas` VALUES ('500238', '巫溪县', '5002', '50');
INSERT INTO `areas` VALUES ('500240', '石柱土家族自治县', '5002', '50');
INSERT INTO `areas` VALUES ('500241', '秀山土家族苗族自治县', '5002', '50');
INSERT INTO `areas` VALUES ('500242', '酉阳土家族苗族自治县', '5002', '50');
INSERT INTO `areas` VALUES ('500243', '彭水苗族土家族自治县', '5002', '50');
INSERT INTO `areas` VALUES ('510104', '锦江区', '5101', '51');
INSERT INTO `areas` VALUES ('510105', '青羊区', '5101', '51');
INSERT INTO `areas` VALUES ('510106', '金牛区', '5101', '51');
INSERT INTO `areas` VALUES ('510107', '武侯区', '5101', '51');
INSERT INTO `areas` VALUES ('510108', '成华区', '5101', '51');
INSERT INTO `areas` VALUES ('510112', '龙泉驿区', '5101', '51');
INSERT INTO `areas` VALUES ('510113', '青白江区', '5101', '51');
INSERT INTO `areas` VALUES ('510114', '新都区', '5101', '51');
INSERT INTO `areas` VALUES ('510115', '温江区', '5101', '51');
INSERT INTO `areas` VALUES ('510116', '双流区', '5101', '51');
INSERT INTO `areas` VALUES ('510117', '郫都区', '5101', '51');
INSERT INTO `areas` VALUES ('510118', '新津区', '5101', '51');
INSERT INTO `areas` VALUES ('510121', '金堂县', '5101', '51');
INSERT INTO `areas` VALUES ('510129', '大邑县', '5101', '51');
INSERT INTO `areas` VALUES ('510131', '蒲江县', '5101', '51');
INSERT INTO `areas` VALUES ('510181', '都江堰市', '5101', '51');
INSERT INTO `areas` VALUES ('510182', '彭州市', '5101', '51');
INSERT INTO `areas` VALUES ('510183', '邛崃市', '5101', '51');
INSERT INTO `areas` VALUES ('510184', '崇州市', '5101', '51');
INSERT INTO `areas` VALUES ('510185', '简阳市', '5101', '51');
INSERT INTO `areas` VALUES ('510302', '自流井区', '5103', '51');
INSERT INTO `areas` VALUES ('510303', '贡井区', '5103', '51');
INSERT INTO `areas` VALUES ('510304', '大安区', '5103', '51');
INSERT INTO `areas` VALUES ('510311', '沿滩区', '5103', '51');
INSERT INTO `areas` VALUES ('510321', '荣县', '5103', '51');
INSERT INTO `areas` VALUES ('510322', '富顺县', '5103', '51');
INSERT INTO `areas` VALUES ('510402', '东区', '5104', '51');
INSERT INTO `areas` VALUES ('510403', '西区', '5104', '51');
INSERT INTO `areas` VALUES ('510411', '仁和区', '5104', '51');
INSERT INTO `areas` VALUES ('510421', '米易县', '5104', '51');
INSERT INTO `areas` VALUES ('510422', '盐边县', '5104', '51');
INSERT INTO `areas` VALUES ('510502', '江阳区', '5105', '51');
INSERT INTO `areas` VALUES ('510503', '纳溪区', '5105', '51');
INSERT INTO `areas` VALUES ('510504', '龙马潭区', '5105', '51');
INSERT INTO `areas` VALUES ('510521', '泸县', '5105', '51');
INSERT INTO `areas` VALUES ('510522', '合江县', '5105', '51');
INSERT INTO `areas` VALUES ('510524', '叙永县', '5105', '51');
INSERT INTO `areas` VALUES ('510525', '古蔺县', '5105', '51');
INSERT INTO `areas` VALUES ('510603', '旌阳区', '5106', '51');
INSERT INTO `areas` VALUES ('510604', '罗江区', '5106', '51');
INSERT INTO `areas` VALUES ('510623', '中江县', '5106', '51');
INSERT INTO `areas` VALUES ('510681', '广汉市', '5106', '51');
INSERT INTO `areas` VALUES ('510682', '什邡市', '5106', '51');
INSERT INTO `areas` VALUES ('510683', '绵竹市', '5106', '51');
INSERT INTO `areas` VALUES ('510703', '涪城区', '5107', '51');
INSERT INTO `areas` VALUES ('510704', '游仙区', '5107', '51');
INSERT INTO `areas` VALUES ('510705', '安州区', '5107', '51');
INSERT INTO `areas` VALUES ('510722', '三台县', '5107', '51');
INSERT INTO `areas` VALUES ('510723', '盐亭县', '5107', '51');
INSERT INTO `areas` VALUES ('510725', '梓潼县', '5107', '51');
INSERT INTO `areas` VALUES ('510726', '北川羌族自治县', '5107', '51');
INSERT INTO `areas` VALUES ('510727', '平武县', '5107', '51');
INSERT INTO `areas` VALUES ('510781', '江油市', '5107', '51');
INSERT INTO `areas` VALUES ('510802', '利州区', '5108', '51');
INSERT INTO `areas` VALUES ('510811', '昭化区', '5108', '51');
INSERT INTO `areas` VALUES ('510812', '朝天区', '5108', '51');
INSERT INTO `areas` VALUES ('510821', '旺苍县', '5108', '51');
INSERT INTO `areas` VALUES ('510822', '青川县', '5108', '51');
INSERT INTO `areas` VALUES ('510823', '剑阁县', '5108', '51');
INSERT INTO `areas` VALUES ('510824', '苍溪县', '5108', '51');
INSERT INTO `areas` VALUES ('510903', '船山区', '5109', '51');
INSERT INTO `areas` VALUES ('510904', '安居区', '5109', '51');
INSERT INTO `areas` VALUES ('510921', '蓬溪县', '5109', '51');
INSERT INTO `areas` VALUES ('510923', '大英县', '5109', '51');
INSERT INTO `areas` VALUES ('510981', '射洪市', '5109', '51');
INSERT INTO `areas` VALUES ('511002', '市中区', '5110', '51');
INSERT INTO `areas` VALUES ('511011', '东兴区', '5110', '51');
INSERT INTO `areas` VALUES ('511024', '威远县', '5110', '51');
INSERT INTO `areas` VALUES ('511025', '资中县', '5110', '51');
INSERT INTO `areas` VALUES ('511071', '内江经济开发区', '5110', '51');
INSERT INTO `areas` VALUES ('511083', '隆昌市', '5110', '51');
INSERT INTO `areas` VALUES ('511102', '市中区', '5111', '51');
INSERT INTO `areas` VALUES ('511111', '沙湾区', '5111', '51');
INSERT INTO `areas` VALUES ('511112', '五通桥区', '5111', '51');
INSERT INTO `areas` VALUES ('511113', '金口河区', '5111', '51');
INSERT INTO `areas` VALUES ('511123', '犍为县', '5111', '51');
INSERT INTO `areas` VALUES ('511124', '井研县', '5111', '51');
INSERT INTO `areas` VALUES ('511126', '夹江县', '5111', '51');
INSERT INTO `areas` VALUES ('511129', '沐川县', '5111', '51');
INSERT INTO `areas` VALUES ('511132', '峨边彝族自治县', '5111', '51');
INSERT INTO `areas` VALUES ('511133', '马边彝族自治县', '5111', '51');
INSERT INTO `areas` VALUES ('511181', '峨眉山市', '5111', '51');
INSERT INTO `areas` VALUES ('511302', '顺庆区', '5113', '51');
INSERT INTO `areas` VALUES ('511303', '高坪区', '5113', '51');
INSERT INTO `areas` VALUES ('511304', '嘉陵区', '5113', '51');
INSERT INTO `areas` VALUES ('511321', '南部县', '5113', '51');
INSERT INTO `areas` VALUES ('511322', '营山县', '5113', '51');
INSERT INTO `areas` VALUES ('511323', '蓬安县', '5113', '51');
INSERT INTO `areas` VALUES ('511324', '仪陇县', '5113', '51');
INSERT INTO `areas` VALUES ('511325', '西充县', '5113', '51');
INSERT INTO `areas` VALUES ('511381', '阆中市', '5113', '51');
INSERT INTO `areas` VALUES ('511402', '东坡区', '5114', '51');
INSERT INTO `areas` VALUES ('511403', '彭山区', '5114', '51');
INSERT INTO `areas` VALUES ('511421', '仁寿县', '5114', '51');
INSERT INTO `areas` VALUES ('511423', '洪雅县', '5114', '51');
INSERT INTO `areas` VALUES ('511424', '丹棱县', '5114', '51');
INSERT INTO `areas` VALUES ('511425', '青神县', '5114', '51');
INSERT INTO `areas` VALUES ('511502', '翠屏区', '5115', '51');
INSERT INTO `areas` VALUES ('511503', '南溪区', '5115', '51');
INSERT INTO `areas` VALUES ('511504', '叙州区', '5115', '51');
INSERT INTO `areas` VALUES ('511523', '江安县', '5115', '51');
INSERT INTO `areas` VALUES ('511524', '长宁县', '5115', '51');
INSERT INTO `areas` VALUES ('511525', '高县', '5115', '51');
INSERT INTO `areas` VALUES ('511526', '珙县', '5115', '51');
INSERT INTO `areas` VALUES ('511527', '筠连县', '5115', '51');
INSERT INTO `areas` VALUES ('511528', '兴文县', '5115', '51');
INSERT INTO `areas` VALUES ('511529', '屏山县', '5115', '51');
INSERT INTO `areas` VALUES ('511602', '广安区', '5116', '51');
INSERT INTO `areas` VALUES ('511603', '前锋区', '5116', '51');
INSERT INTO `areas` VALUES ('511621', '岳池县', '5116', '51');
INSERT INTO `areas` VALUES ('511622', '武胜县', '5116', '51');
INSERT INTO `areas` VALUES ('511623', '邻水县', '5116', '51');
INSERT INTO `areas` VALUES ('511681', '华蓥市', '5116', '51');
INSERT INTO `areas` VALUES ('511702', '通川区', '5117', '51');
INSERT INTO `areas` VALUES ('511703', '达川区', '5117', '51');
INSERT INTO `areas` VALUES ('511722', '宣汉县', '5117', '51');
INSERT INTO `areas` VALUES ('511723', '开江县', '5117', '51');
INSERT INTO `areas` VALUES ('511724', '大竹县', '5117', '51');
INSERT INTO `areas` VALUES ('511725', '渠县', '5117', '51');
INSERT INTO `areas` VALUES ('511771', '达州经济开发区', '5117', '51');
INSERT INTO `areas` VALUES ('511781', '万源市', '5117', '51');
INSERT INTO `areas` VALUES ('511802', '雨城区', '5118', '51');
INSERT INTO `areas` VALUES ('511803', '名山区', '5118', '51');
INSERT INTO `areas` VALUES ('511822', '荥经县', '5118', '51');
INSERT INTO `areas` VALUES ('511823', '汉源县', '5118', '51');
INSERT INTO `areas` VALUES ('511824', '石棉县', '5118', '51');
INSERT INTO `areas` VALUES ('511825', '天全县', '5118', '51');
INSERT INTO `areas` VALUES ('511826', '芦山县', '5118', '51');
INSERT INTO `areas` VALUES ('511827', '宝兴县', '5118', '51');
INSERT INTO `areas` VALUES ('511902', '巴州区', '5119', '51');
INSERT INTO `areas` VALUES ('511903', '恩阳区', '5119', '51');
INSERT INTO `areas` VALUES ('511921', '通江县', '5119', '51');
INSERT INTO `areas` VALUES ('511922', '南江县', '5119', '51');
INSERT INTO `areas` VALUES ('511923', '平昌县', '5119', '51');
INSERT INTO `areas` VALUES ('511971', '巴中经济开发区', '5119', '51');
INSERT INTO `areas` VALUES ('512002', '雁江区', '5120', '51');
INSERT INTO `areas` VALUES ('512021', '安岳县', '5120', '51');
INSERT INTO `areas` VALUES ('512022', '乐至县', '5120', '51');
INSERT INTO `areas` VALUES ('513201', '马尔康市', '5132', '51');
INSERT INTO `areas` VALUES ('513221', '汶川县', '5132', '51');
INSERT INTO `areas` VALUES ('513222', '理县', '5132', '51');
INSERT INTO `areas` VALUES ('513223', '茂县', '5132', '51');
INSERT INTO `areas` VALUES ('513224', '松潘县', '5132', '51');
INSERT INTO `areas` VALUES ('513225', '九寨沟县', '5132', '51');
INSERT INTO `areas` VALUES ('513226', '金川县', '5132', '51');
INSERT INTO `areas` VALUES ('513227', '小金县', '5132', '51');
INSERT INTO `areas` VALUES ('513228', '黑水县', '5132', '51');
INSERT INTO `areas` VALUES ('513230', '壤塘县', '5132', '51');
INSERT INTO `areas` VALUES ('513231', '阿坝县', '5132', '51');
INSERT INTO `areas` VALUES ('513232', '若尔盖县', '5132', '51');
INSERT INTO `areas` VALUES ('513233', '红原县', '5132', '51');
INSERT INTO `areas` VALUES ('513301', '康定市', '5133', '51');
INSERT INTO `areas` VALUES ('513322', '泸定县', '5133', '51');
INSERT INTO `areas` VALUES ('513323', '丹巴县', '5133', '51');
INSERT INTO `areas` VALUES ('513324', '九龙县', '5133', '51');
INSERT INTO `areas` VALUES ('513325', '雅江县', '5133', '51');
INSERT INTO `areas` VALUES ('513326', '道孚县', '5133', '51');
INSERT INTO `areas` VALUES ('513327', '炉霍县', '5133', '51');
INSERT INTO `areas` VALUES ('513328', '甘孜县', '5133', '51');
INSERT INTO `areas` VALUES ('513329', '新龙县', '5133', '51');
INSERT INTO `areas` VALUES ('513330', '德格县', '5133', '51');
INSERT INTO `areas` VALUES ('513331', '白玉县', '5133', '51');
INSERT INTO `areas` VALUES ('513332', '石渠县', '5133', '51');
INSERT INTO `areas` VALUES ('513333', '色达县', '5133', '51');
INSERT INTO `areas` VALUES ('513334', '理塘县', '5133', '51');
INSERT INTO `areas` VALUES ('513335', '巴塘县', '5133', '51');
INSERT INTO `areas` VALUES ('513336', '乡城县', '5133', '51');
INSERT INTO `areas` VALUES ('513337', '稻城县', '5133', '51');
INSERT INTO `areas` VALUES ('513338', '得荣县', '5133', '51');
INSERT INTO `areas` VALUES ('513401', '西昌市', '5134', '51');
INSERT INTO `areas` VALUES ('513402', '会理市', '5134', '51');
INSERT INTO `areas` VALUES ('513422', '木里藏族自治县', '5134', '51');
INSERT INTO `areas` VALUES ('513423', '盐源县', '5134', '51');
INSERT INTO `areas` VALUES ('513424', '德昌县', '5134', '51');
INSERT INTO `areas` VALUES ('513426', '会东县', '5134', '51');
INSERT INTO `areas` VALUES ('513427', '宁南县', '5134', '51');
INSERT INTO `areas` VALUES ('513428', '普格县', '5134', '51');
INSERT INTO `areas` VALUES ('513429', '布拖县', '5134', '51');
INSERT INTO `areas` VALUES ('513430', '金阳县', '5134', '51');
INSERT INTO `areas` VALUES ('513431', '昭觉县', '5134', '51');
INSERT INTO `areas` VALUES ('513432', '喜德县', '5134', '51');
INSERT INTO `areas` VALUES ('513433', '冕宁县', '5134', '51');
INSERT INTO `areas` VALUES ('513434', '越西县', '5134', '51');
INSERT INTO `areas` VALUES ('513435', '甘洛县', '5134', '51');
INSERT INTO `areas` VALUES ('513436', '美姑县', '5134', '51');
INSERT INTO `areas` VALUES ('513437', '雷波县', '5134', '51');
INSERT INTO `areas` VALUES ('520102', '南明区', '5201', '52');
INSERT INTO `areas` VALUES ('520103', '云岩区', '5201', '52');
INSERT INTO `areas` VALUES ('520111', '花溪区', '5201', '52');
INSERT INTO `areas` VALUES ('520112', '乌当区', '5201', '52');
INSERT INTO `areas` VALUES ('520113', '白云区', '5201', '52');
INSERT INTO `areas` VALUES ('520115', '观山湖区', '5201', '52');
INSERT INTO `areas` VALUES ('520121', '开阳县', '5201', '52');
INSERT INTO `areas` VALUES ('520122', '息烽县', '5201', '52');
INSERT INTO `areas` VALUES ('520123', '修文县', '5201', '52');
INSERT INTO `areas` VALUES ('520181', '清镇市', '5201', '52');
INSERT INTO `areas` VALUES ('520201', '钟山区', '5202', '52');
INSERT INTO `areas` VALUES ('520203', '六枝特区', '5202', '52');
INSERT INTO `areas` VALUES ('520204', '水城区', '5202', '52');
INSERT INTO `areas` VALUES ('520281', '盘州市', '5202', '52');
INSERT INTO `areas` VALUES ('520302', '红花岗区', '5203', '52');
INSERT INTO `areas` VALUES ('520303', '汇川区', '5203', '52');
INSERT INTO `areas` VALUES ('520304', '播州区', '5203', '52');
INSERT INTO `areas` VALUES ('520322', '桐梓县', '5203', '52');
INSERT INTO `areas` VALUES ('520323', '绥阳县', '5203', '52');
INSERT INTO `areas` VALUES ('520324', '正安县', '5203', '52');
INSERT INTO `areas` VALUES ('520325', '道真仡佬族苗族自治县', '5203', '52');
INSERT INTO `areas` VALUES ('520326', '务川仡佬族苗族自治县', '5203', '52');
INSERT INTO `areas` VALUES ('520327', '凤冈县', '5203', '52');
INSERT INTO `areas` VALUES ('520328', '湄潭县', '5203', '52');
INSERT INTO `areas` VALUES ('520329', '余庆县', '5203', '52');
INSERT INTO `areas` VALUES ('520330', '习水县', '5203', '52');
INSERT INTO `areas` VALUES ('520381', '赤水市', '5203', '52');
INSERT INTO `areas` VALUES ('520382', '仁怀市', '5203', '52');
INSERT INTO `areas` VALUES ('520402', '西秀区', '5204', '52');
INSERT INTO `areas` VALUES ('520403', '平坝区', '5204', '52');
INSERT INTO `areas` VALUES ('520422', '普定县', '5204', '52');
INSERT INTO `areas` VALUES ('520423', '镇宁布依族苗族自治县', '5204', '52');
INSERT INTO `areas` VALUES ('520424', '关岭布依族苗族自治县', '5204', '52');
INSERT INTO `areas` VALUES ('520425', '紫云苗族布依族自治县', '5204', '52');
INSERT INTO `areas` VALUES ('520502', '七星关区', '5205', '52');
INSERT INTO `areas` VALUES ('520521', '大方县', '5205', '52');
INSERT INTO `areas` VALUES ('520523', '金沙县', '5205', '52');
INSERT INTO `areas` VALUES ('520524', '织金县', '5205', '52');
INSERT INTO `areas` VALUES ('520525', '纳雍县', '5205', '52');
INSERT INTO `areas` VALUES ('520526', '威宁彝族回族苗族自治县', '5205', '52');
INSERT INTO `areas` VALUES ('520527', '赫章县', '5205', '52');
INSERT INTO `areas` VALUES ('520581', '黔西市', '5205', '52');
INSERT INTO `areas` VALUES ('520602', '碧江区', '5206', '52');
INSERT INTO `areas` VALUES ('520603', '万山区', '5206', '52');
INSERT INTO `areas` VALUES ('520621', '江口县', '5206', '52');
INSERT INTO `areas` VALUES ('520622', '玉屏侗族自治县', '5206', '52');
INSERT INTO `areas` VALUES ('520623', '石阡县', '5206', '52');
INSERT INTO `areas` VALUES ('520624', '思南县', '5206', '52');
INSERT INTO `areas` VALUES ('520625', '印江土家族苗族自治县', '5206', '52');
INSERT INTO `areas` VALUES ('520626', '德江县', '5206', '52');
INSERT INTO `areas` VALUES ('520627', '沿河土家族自治县', '5206', '52');
INSERT INTO `areas` VALUES ('520628', '松桃苗族自治县', '5206', '52');
INSERT INTO `areas` VALUES ('522301', '兴义市', '5223', '52');
INSERT INTO `areas` VALUES ('522302', '兴仁市', '5223', '52');
INSERT INTO `areas` VALUES ('522323', '普安县', '5223', '52');
INSERT INTO `areas` VALUES ('522324', '晴隆县', '5223', '52');
INSERT INTO `areas` VALUES ('522325', '贞丰县', '5223', '52');
INSERT INTO `areas` VALUES ('522326', '望谟县', '5223', '52');
INSERT INTO `areas` VALUES ('522327', '册亨县', '5223', '52');
INSERT INTO `areas` VALUES ('522328', '安龙县', '5223', '52');
INSERT INTO `areas` VALUES ('522601', '凯里市', '5226', '52');
INSERT INTO `areas` VALUES ('522622', '黄平县', '5226', '52');
INSERT INTO `areas` VALUES ('522623', '施秉县', '5226', '52');
INSERT INTO `areas` VALUES ('522624', '三穗县', '5226', '52');
INSERT INTO `areas` VALUES ('522625', '镇远县', '5226', '52');
INSERT INTO `areas` VALUES ('522626', '岑巩县', '5226', '52');
INSERT INTO `areas` VALUES ('522627', '天柱县', '5226', '52');
INSERT INTO `areas` VALUES ('522628', '锦屏县', '5226', '52');
INSERT INTO `areas` VALUES ('522629', '剑河县', '5226', '52');
INSERT INTO `areas` VALUES ('522630', '台江县', '5226', '52');
INSERT INTO `areas` VALUES ('522631', '黎平县', '5226', '52');
INSERT INTO `areas` VALUES ('522632', '榕江县', '5226', '52');
INSERT INTO `areas` VALUES ('522633', '从江县', '5226', '52');
INSERT INTO `areas` VALUES ('522634', '雷山县', '5226', '52');
INSERT INTO `areas` VALUES ('522635', '麻江县', '5226', '52');
INSERT INTO `areas` VALUES ('522636', '丹寨县', '5226', '52');
INSERT INTO `areas` VALUES ('522701', '都匀市', '5227', '52');
INSERT INTO `areas` VALUES ('522702', '福泉市', '5227', '52');
INSERT INTO `areas` VALUES ('522722', '荔波县', '5227', '52');
INSERT INTO `areas` VALUES ('522723', '贵定县', '5227', '52');
INSERT INTO `areas` VALUES ('522725', '瓮安县', '5227', '52');
INSERT INTO `areas` VALUES ('522726', '独山县', '5227', '52');
INSERT INTO `areas` VALUES ('522727', '平塘县', '5227', '52');
INSERT INTO `areas` VALUES ('522728', '罗甸县', '5227', '52');
INSERT INTO `areas` VALUES ('522729', '长顺县', '5227', '52');
INSERT INTO `areas` VALUES ('522730', '龙里县', '5227', '52');
INSERT INTO `areas` VALUES ('522731', '惠水县', '5227', '52');
INSERT INTO `areas` VALUES ('522732', '三都水族自治县', '5227', '52');
INSERT INTO `areas` VALUES ('530102', '五华区', '5301', '53');
INSERT INTO `areas` VALUES ('530103', '盘龙区', '5301', '53');
INSERT INTO `areas` VALUES ('530111', '官渡区', '5301', '53');
INSERT INTO `areas` VALUES ('530112', '西山区', '5301', '53');
INSERT INTO `areas` VALUES ('530113', '东川区', '5301', '53');
INSERT INTO `areas` VALUES ('530114', '呈贡区', '5301', '53');
INSERT INTO `areas` VALUES ('530115', '晋宁区', '5301', '53');
INSERT INTO `areas` VALUES ('530124', '富民县', '5301', '53');
INSERT INTO `areas` VALUES ('530125', '宜良县', '5301', '53');
INSERT INTO `areas` VALUES ('530126', '石林彝族自治县', '5301', '53');
INSERT INTO `areas` VALUES ('530127', '嵩明县', '5301', '53');
INSERT INTO `areas` VALUES ('530128', '禄劝彝族苗族自治县', '5301', '53');
INSERT INTO `areas` VALUES ('530129', '寻甸回族彝族自治县', '5301', '53');
INSERT INTO `areas` VALUES ('530181', '安宁市', '5301', '53');
INSERT INTO `areas` VALUES ('530302', '麒麟区', '5303', '53');
INSERT INTO `areas` VALUES ('530303', '沾益区', '5303', '53');
INSERT INTO `areas` VALUES ('530304', '马龙区', '5303', '53');
INSERT INTO `areas` VALUES ('530322', '陆良县', '5303', '53');
INSERT INTO `areas` VALUES ('530323', '师宗县', '5303', '53');
INSERT INTO `areas` VALUES ('530324', '罗平县', '5303', '53');
INSERT INTO `areas` VALUES ('530325', '富源县', '5303', '53');
INSERT INTO `areas` VALUES ('530326', '会泽县', '5303', '53');
INSERT INTO `areas` VALUES ('530381', '宣威市', '5303', '53');
INSERT INTO `areas` VALUES ('530402', '红塔区', '5304', '53');
INSERT INTO `areas` VALUES ('530403', '江川区', '5304', '53');
INSERT INTO `areas` VALUES ('530423', '通海县', '5304', '53');
INSERT INTO `areas` VALUES ('530424', '华宁县', '5304', '53');
INSERT INTO `areas` VALUES ('530425', '易门县', '5304', '53');
INSERT INTO `areas` VALUES ('530426', '峨山彝族自治县', '5304', '53');
INSERT INTO `areas` VALUES ('530427', '新平彝族傣族自治县', '5304', '53');
INSERT INTO `areas` VALUES ('530428', '元江哈尼族彝族傣族自治县', '5304', '53');
INSERT INTO `areas` VALUES ('530481', '澄江市', '5304', '53');
INSERT INTO `areas` VALUES ('530502', '隆阳区', '5305', '53');
INSERT INTO `areas` VALUES ('530521', '施甸县', '5305', '53');
INSERT INTO `areas` VALUES ('530523', '龙陵县', '5305', '53');
INSERT INTO `areas` VALUES ('530524', '昌宁县', '5305', '53');
INSERT INTO `areas` VALUES ('530581', '腾冲市', '5305', '53');
INSERT INTO `areas` VALUES ('530602', '昭阳区', '5306', '53');
INSERT INTO `areas` VALUES ('530621', '鲁甸县', '5306', '53');
INSERT INTO `areas` VALUES ('530622', '巧家县', '5306', '53');
INSERT INTO `areas` VALUES ('530623', '盐津县', '5306', '53');
INSERT INTO `areas` VALUES ('530624', '大关县', '5306', '53');
INSERT INTO `areas` VALUES ('530625', '永善县', '5306', '53');
INSERT INTO `areas` VALUES ('530626', '绥江县', '5306', '53');
INSERT INTO `areas` VALUES ('530627', '镇雄县', '5306', '53');
INSERT INTO `areas` VALUES ('530628', '彝良县', '5306', '53');
INSERT INTO `areas` VALUES ('530629', '威信县', '5306', '53');
INSERT INTO `areas` VALUES ('530681', '水富市', '5306', '53');
INSERT INTO `areas` VALUES ('530702', '古城区', '5307', '53');
INSERT INTO `areas` VALUES ('530721', '玉龙纳西族自治县', '5307', '53');
INSERT INTO `areas` VALUES ('530722', '永胜县', '5307', '53');
INSERT INTO `areas` VALUES ('530723', '华坪县', '5307', '53');
INSERT INTO `areas` VALUES ('530724', '宁蒗彝族自治县', '5307', '53');
INSERT INTO `areas` VALUES ('530802', '思茅区', '5308', '53');
INSERT INTO `areas` VALUES ('530821', '宁洱哈尼族彝族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530822', '墨江哈尼族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530823', '景东彝族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530824', '景谷傣族彝族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530825', '镇沅彝族哈尼族拉祜族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530826', '江城哈尼族彝族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530827', '孟连傣族拉祜族佤族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530828', '澜沧拉祜族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530829', '西盟佤族自治县', '5308', '53');
INSERT INTO `areas` VALUES ('530902', '临翔区', '5309', '53');
INSERT INTO `areas` VALUES ('530921', '凤庆县', '5309', '53');
INSERT INTO `areas` VALUES ('530922', '云县', '5309', '53');
INSERT INTO `areas` VALUES ('530923', '永德县', '5309', '53');
INSERT INTO `areas` VALUES ('530924', '镇康县', '5309', '53');
INSERT INTO `areas` VALUES ('530925', '双江拉祜族佤族布朗族傣族自治县', '5309', '53');
INSERT INTO `areas` VALUES ('530926', '耿马傣族佤族自治县', '5309', '53');
INSERT INTO `areas` VALUES ('530927', '沧源佤族自治县', '5309', '53');
INSERT INTO `areas` VALUES ('532301', '楚雄市', '5323', '53');
INSERT INTO `areas` VALUES ('532302', '禄丰市', '5323', '53');
INSERT INTO `areas` VALUES ('532322', '双柏县', '5323', '53');
INSERT INTO `areas` VALUES ('532323', '牟定县', '5323', '53');
INSERT INTO `areas` VALUES ('532324', '南华县', '5323', '53');
INSERT INTO `areas` VALUES ('532325', '姚安县', '5323', '53');
INSERT INTO `areas` VALUES ('532326', '大姚县', '5323', '53');
INSERT INTO `areas` VALUES ('532327', '永仁县', '5323', '53');
INSERT INTO `areas` VALUES ('532328', '元谋县', '5323', '53');
INSERT INTO `areas` VALUES ('532329', '武定县', '5323', '53');
INSERT INTO `areas` VALUES ('532501', '个旧市', '5325', '53');
INSERT INTO `areas` VALUES ('532502', '开远市', '5325', '53');
INSERT INTO `areas` VALUES ('532503', '蒙自市', '5325', '53');
INSERT INTO `areas` VALUES ('532504', '弥勒市', '5325', '53');
INSERT INTO `areas` VALUES ('532523', '屏边苗族自治县', '5325', '53');
INSERT INTO `areas` VALUES ('532524', '建水县', '5325', '53');
INSERT INTO `areas` VALUES ('532525', '石屏县', '5325', '53');
INSERT INTO `areas` VALUES ('532527', '泸西县', '5325', '53');
INSERT INTO `areas` VALUES ('532528', '元阳县', '5325', '53');
INSERT INTO `areas` VALUES ('532529', '红河县', '5325', '53');
INSERT INTO `areas` VALUES ('532530', '金平苗族瑶族傣族自治县', '5325', '53');
INSERT INTO `areas` VALUES ('532531', '绿春县', '5325', '53');
INSERT INTO `areas` VALUES ('532532', '河口瑶族自治县', '5325', '53');
INSERT INTO `areas` VALUES ('532601', '文山市', '5326', '53');
INSERT INTO `areas` VALUES ('532622', '砚山县', '5326', '53');
INSERT INTO `areas` VALUES ('532623', '西畴县', '5326', '53');
INSERT INTO `areas` VALUES ('532624', '麻栗坡县', '5326', '53');
INSERT INTO `areas` VALUES ('532625', '马关县', '5326', '53');
INSERT INTO `areas` VALUES ('532626', '丘北县', '5326', '53');
INSERT INTO `areas` VALUES ('532627', '广南县', '5326', '53');
INSERT INTO `areas` VALUES ('532628', '富宁县', '5326', '53');
INSERT INTO `areas` VALUES ('532801', '景洪市', '5328', '53');
INSERT INTO `areas` VALUES ('532822', '勐海县', '5328', '53');
INSERT INTO `areas` VALUES ('532823', '勐腊县', '5328', '53');
INSERT INTO `areas` VALUES ('532901', '大理市', '5329', '53');
INSERT INTO `areas` VALUES ('532922', '漾濞彝族自治县', '5329', '53');
INSERT INTO `areas` VALUES ('532923', '祥云县', '5329', '53');
INSERT INTO `areas` VALUES ('532924', '宾川县', '5329', '53');
INSERT INTO `areas` VALUES ('532925', '弥渡县', '5329', '53');
INSERT INTO `areas` VALUES ('532926', '南涧彝族自治县', '5329', '53');
INSERT INTO `areas` VALUES ('532927', '巍山彝族回族自治县', '5329', '53');
INSERT INTO `areas` VALUES ('532928', '永平县', '5329', '53');
INSERT INTO `areas` VALUES ('532929', '云龙县', '5329', '53');
INSERT INTO `areas` VALUES ('532930', '洱源县', '5329', '53');
INSERT INTO `areas` VALUES ('532931', '剑川县', '5329', '53');
INSERT INTO `areas` VALUES ('532932', '鹤庆县', '5329', '53');
INSERT INTO `areas` VALUES ('533102', '瑞丽市', '5331', '53');
INSERT INTO `areas` VALUES ('533103', '芒市', '5331', '53');
INSERT INTO `areas` VALUES ('533122', '梁河县', '5331', '53');
INSERT INTO `areas` VALUES ('533123', '盈江县', '5331', '53');
INSERT INTO `areas` VALUES ('533124', '陇川县', '5331', '53');
INSERT INTO `areas` VALUES ('533301', '泸水市', '5333', '53');
INSERT INTO `areas` VALUES ('533323', '福贡县', '5333', '53');
INSERT INTO `areas` VALUES ('533324', '贡山独龙族怒族自治县', '5333', '53');
INSERT INTO `areas` VALUES ('533325', '兰坪白族普米族自治县', '5333', '53');
INSERT INTO `areas` VALUES ('533401', '香格里拉市', '5334', '53');
INSERT INTO `areas` VALUES ('533422', '德钦县', '5334', '53');
INSERT INTO `areas` VALUES ('533423', '维西傈僳族自治县', '5334', '53');
INSERT INTO `areas` VALUES ('540102', '城关区', '5401', '54');
INSERT INTO `areas` VALUES ('540103', '堆龙德庆区', '5401', '54');
INSERT INTO `areas` VALUES ('540104', '达孜区', '5401', '54');
INSERT INTO `areas` VALUES ('540121', '林周县', '5401', '54');
INSERT INTO `areas` VALUES ('540122', '当雄县', '5401', '54');
INSERT INTO `areas` VALUES ('540123', '尼木县', '5401', '54');
INSERT INTO `areas` VALUES ('540124', '曲水县', '5401', '54');
INSERT INTO `areas` VALUES ('540127', '墨竹工卡县', '5401', '54');
INSERT INTO `areas` VALUES ('540171', '格尔木藏青工业园区', '5401', '54');
INSERT INTO `areas` VALUES ('540172', '拉萨经济技术开发区', '5401', '54');
INSERT INTO `areas` VALUES ('540173', '西藏文化旅游创意园区', '5401', '54');
INSERT INTO `areas` VALUES ('540174', '达孜工业园区', '5401', '54');
INSERT INTO `areas` VALUES ('540202', '桑珠孜区', '5402', '54');
INSERT INTO `areas` VALUES ('540221', '南木林县', '5402', '54');
INSERT INTO `areas` VALUES ('540222', '江孜县', '5402', '54');
INSERT INTO `areas` VALUES ('540223', '定日县', '5402', '54');
INSERT INTO `areas` VALUES ('540224', '萨迦县', '5402', '54');
INSERT INTO `areas` VALUES ('540225', '拉孜县', '5402', '54');
INSERT INTO `areas` VALUES ('540226', '昂仁县', '5402', '54');
INSERT INTO `areas` VALUES ('540227', '谢通门县', '5402', '54');
INSERT INTO `areas` VALUES ('540228', '白朗县', '5402', '54');
INSERT INTO `areas` VALUES ('540229', '仁布县', '5402', '54');
INSERT INTO `areas` VALUES ('540230', '康马县', '5402', '54');
INSERT INTO `areas` VALUES ('540231', '定结县', '5402', '54');
INSERT INTO `areas` VALUES ('540232', '仲巴县', '5402', '54');
INSERT INTO `areas` VALUES ('540233', '亚东县', '5402', '54');
INSERT INTO `areas` VALUES ('540234', '吉隆县', '5402', '54');
INSERT INTO `areas` VALUES ('540235', '聂拉木县', '5402', '54');
INSERT INTO `areas` VALUES ('540236', '萨嘎县', '5402', '54');
INSERT INTO `areas` VALUES ('540237', '岗巴县', '5402', '54');
INSERT INTO `areas` VALUES ('540302', '卡若区', '5403', '54');
INSERT INTO `areas` VALUES ('540321', '江达县', '5403', '54');
INSERT INTO `areas` VALUES ('540322', '贡觉县', '5403', '54');
INSERT INTO `areas` VALUES ('540323', '类乌齐县', '5403', '54');
INSERT INTO `areas` VALUES ('540324', '丁青县', '5403', '54');
INSERT INTO `areas` VALUES ('540325', '察雅县', '5403', '54');
INSERT INTO `areas` VALUES ('540326', '八宿县', '5403', '54');
INSERT INTO `areas` VALUES ('540327', '左贡县', '5403', '54');
INSERT INTO `areas` VALUES ('540328', '芒康县', '5403', '54');
INSERT INTO `areas` VALUES ('540329', '洛隆县', '5403', '54');
INSERT INTO `areas` VALUES ('540330', '边坝县', '5403', '54');
INSERT INTO `areas` VALUES ('540402', '巴宜区', '5404', '54');
INSERT INTO `areas` VALUES ('540421', '工布江达县', '5404', '54');
INSERT INTO `areas` VALUES ('540422', '米林县', '5404', '54');
INSERT INTO `areas` VALUES ('540423', '墨脱县', '5404', '54');
INSERT INTO `areas` VALUES ('540424', '波密县', '5404', '54');
INSERT INTO `areas` VALUES ('540425', '察隅县', '5404', '54');
INSERT INTO `areas` VALUES ('540426', '朗县', '5404', '54');
INSERT INTO `areas` VALUES ('540502', '乃东区', '5405', '54');
INSERT INTO `areas` VALUES ('540521', '扎囊县', '5405', '54');
INSERT INTO `areas` VALUES ('540522', '贡嘎县', '5405', '54');
INSERT INTO `areas` VALUES ('540523', '桑日县', '5405', '54');
INSERT INTO `areas` VALUES ('540524', '琼结县', '5405', '54');
INSERT INTO `areas` VALUES ('540525', '曲松县', '5405', '54');
INSERT INTO `areas` VALUES ('540526', '措美县', '5405', '54');
INSERT INTO `areas` VALUES ('540527', '洛扎县', '5405', '54');
INSERT INTO `areas` VALUES ('540528', '加查县', '5405', '54');
INSERT INTO `areas` VALUES ('540529', '隆子县', '5405', '54');
INSERT INTO `areas` VALUES ('540530', '错那县', '5405', '54');
INSERT INTO `areas` VALUES ('540531', '浪卡子县', '5405', '54');
INSERT INTO `areas` VALUES ('540602', '色尼区', '5406', '54');
INSERT INTO `areas` VALUES ('540621', '嘉黎县', '5406', '54');
INSERT INTO `areas` VALUES ('540622', '比如县', '5406', '54');
INSERT INTO `areas` VALUES ('540623', '聂荣县', '5406', '54');
INSERT INTO `areas` VALUES ('540624', '安多县', '5406', '54');
INSERT INTO `areas` VALUES ('540625', '申扎县', '5406', '54');
INSERT INTO `areas` VALUES ('540626', '索县', '5406', '54');
INSERT INTO `areas` VALUES ('540627', '班戈县', '5406', '54');
INSERT INTO `areas` VALUES ('540628', '巴青县', '5406', '54');
INSERT INTO `areas` VALUES ('540629', '尼玛县', '5406', '54');
INSERT INTO `areas` VALUES ('540630', '双湖县', '5406', '54');
INSERT INTO `areas` VALUES ('542521', '普兰县', '5425', '54');
INSERT INTO `areas` VALUES ('542522', '札达县', '5425', '54');
INSERT INTO `areas` VALUES ('542523', '噶尔县', '5425', '54');
INSERT INTO `areas` VALUES ('542524', '日土县', '5425', '54');
INSERT INTO `areas` VALUES ('542525', '革吉县', '5425', '54');
INSERT INTO `areas` VALUES ('542526', '改则县', '5425', '54');
INSERT INTO `areas` VALUES ('542527', '措勤县', '5425', '54');
INSERT INTO `areas` VALUES ('610102', '新城区', '6101', '61');
INSERT INTO `areas` VALUES ('610103', '碑林区', '6101', '61');
INSERT INTO `areas` VALUES ('610104', '莲湖区', '6101', '61');
INSERT INTO `areas` VALUES ('610111', '灞桥区', '6101', '61');
INSERT INTO `areas` VALUES ('610112', '未央区', '6101', '61');
INSERT INTO `areas` VALUES ('610113', '雁塔区', '6101', '61');
INSERT INTO `areas` VALUES ('610114', '阎良区', '6101', '61');
INSERT INTO `areas` VALUES ('610115', '临潼区', '6101', '61');
INSERT INTO `areas` VALUES ('610116', '长安区', '6101', '61');
INSERT INTO `areas` VALUES ('610117', '高陵区', '6101', '61');
INSERT INTO `areas` VALUES ('610118', '鄠邑区', '6101', '61');
INSERT INTO `areas` VALUES ('610122', '蓝田县', '6101', '61');
INSERT INTO `areas` VALUES ('610124', '周至县', '6101', '61');
INSERT INTO `areas` VALUES ('610202', '王益区', '6102', '61');
INSERT INTO `areas` VALUES ('610203', '印台区', '6102', '61');
INSERT INTO `areas` VALUES ('610204', '耀州区', '6102', '61');
INSERT INTO `areas` VALUES ('610222', '宜君县', '6102', '61');
INSERT INTO `areas` VALUES ('610302', '渭滨区', '6103', '61');
INSERT INTO `areas` VALUES ('610303', '金台区', '6103', '61');
INSERT INTO `areas` VALUES ('610304', '陈仓区', '6103', '61');
INSERT INTO `areas` VALUES ('610305', '凤翔区', '6103', '61');
INSERT INTO `areas` VALUES ('610323', '岐山县', '6103', '61');
INSERT INTO `areas` VALUES ('610324', '扶风县', '6103', '61');
INSERT INTO `areas` VALUES ('610326', '眉县', '6103', '61');
INSERT INTO `areas` VALUES ('610327', '陇县', '6103', '61');
INSERT INTO `areas` VALUES ('610328', '千阳县', '6103', '61');
INSERT INTO `areas` VALUES ('610329', '麟游县', '6103', '61');
INSERT INTO `areas` VALUES ('610330', '凤县', '6103', '61');
INSERT INTO `areas` VALUES ('610331', '太白县', '6103', '61');
INSERT INTO `areas` VALUES ('610402', '秦都区', '6104', '61');
INSERT INTO `areas` VALUES ('610403', '杨陵区', '6104', '61');
INSERT INTO `areas` VALUES ('610404', '渭城区', '6104', '61');
INSERT INTO `areas` VALUES ('610422', '三原县', '6104', '61');
INSERT INTO `areas` VALUES ('610423', '泾阳县', '6104', '61');
INSERT INTO `areas` VALUES ('610424', '乾县', '6104', '61');
INSERT INTO `areas` VALUES ('610425', '礼泉县', '6104', '61');
INSERT INTO `areas` VALUES ('610426', '永寿县', '6104', '61');
INSERT INTO `areas` VALUES ('610428', '长武县', '6104', '61');
INSERT INTO `areas` VALUES ('610429', '旬邑县', '6104', '61');
INSERT INTO `areas` VALUES ('610430', '淳化县', '6104', '61');
INSERT INTO `areas` VALUES ('610431', '武功县', '6104', '61');
INSERT INTO `areas` VALUES ('610481', '兴平市', '6104', '61');
INSERT INTO `areas` VALUES ('610482', '彬州市', '6104', '61');
INSERT INTO `areas` VALUES ('610502', '临渭区', '6105', '61');
INSERT INTO `areas` VALUES ('610503', '华州区', '6105', '61');
INSERT INTO `areas` VALUES ('610522', '潼关县', '6105', '61');
INSERT INTO `areas` VALUES ('610523', '大荔县', '6105', '61');
INSERT INTO `areas` VALUES ('610524', '合阳县', '6105', '61');
INSERT INTO `areas` VALUES ('610525', '澄城县', '6105', '61');
INSERT INTO `areas` VALUES ('610526', '蒲城县', '6105', '61');
INSERT INTO `areas` VALUES ('610527', '白水县', '6105', '61');
INSERT INTO `areas` VALUES ('610528', '富平县', '6105', '61');
INSERT INTO `areas` VALUES ('610581', '韩城市', '6105', '61');
INSERT INTO `areas` VALUES ('610582', '华阴市', '6105', '61');
INSERT INTO `areas` VALUES ('610602', '宝塔区', '6106', '61');
INSERT INTO `areas` VALUES ('610603', '安塞区', '6106', '61');
INSERT INTO `areas` VALUES ('610621', '延长县', '6106', '61');
INSERT INTO `areas` VALUES ('610622', '延川县', '6106', '61');
INSERT INTO `areas` VALUES ('610625', '志丹县', '6106', '61');
INSERT INTO `areas` VALUES ('610626', '吴起县', '6106', '61');
INSERT INTO `areas` VALUES ('610627', '甘泉县', '6106', '61');
INSERT INTO `areas` VALUES ('610628', '富县', '6106', '61');
INSERT INTO `areas` VALUES ('610629', '洛川县', '6106', '61');
INSERT INTO `areas` VALUES ('610630', '宜川县', '6106', '61');
INSERT INTO `areas` VALUES ('610631', '黄龙县', '6106', '61');
INSERT INTO `areas` VALUES ('610632', '黄陵县', '6106', '61');
INSERT INTO `areas` VALUES ('610681', '子长市', '6106', '61');
INSERT INTO `areas` VALUES ('610702', '汉台区', '6107', '61');
INSERT INTO `areas` VALUES ('610703', '南郑区', '6107', '61');
INSERT INTO `areas` VALUES ('610722', '城固县', '6107', '61');
INSERT INTO `areas` VALUES ('610723', '洋县', '6107', '61');
INSERT INTO `areas` VALUES ('610724', '西乡县', '6107', '61');
INSERT INTO `areas` VALUES ('610725', '勉县', '6107', '61');
INSERT INTO `areas` VALUES ('610726', '宁强县', '6107', '61');
INSERT INTO `areas` VALUES ('610727', '略阳县', '6107', '61');
INSERT INTO `areas` VALUES ('610728', '镇巴县', '6107', '61');
INSERT INTO `areas` VALUES ('610729', '留坝县', '6107', '61');
INSERT INTO `areas` VALUES ('610730', '佛坪县', '6107', '61');
INSERT INTO `areas` VALUES ('610802', '榆阳区', '6108', '61');
INSERT INTO `areas` VALUES ('610803', '横山区', '6108', '61');
INSERT INTO `areas` VALUES ('610822', '府谷县', '6108', '61');
INSERT INTO `areas` VALUES ('610824', '靖边县', '6108', '61');
INSERT INTO `areas` VALUES ('610825', '定边县', '6108', '61');
INSERT INTO `areas` VALUES ('610826', '绥德县', '6108', '61');
INSERT INTO `areas` VALUES ('610827', '米脂县', '6108', '61');
INSERT INTO `areas` VALUES ('610828', '佳县', '6108', '61');
INSERT INTO `areas` VALUES ('610829', '吴堡县', '6108', '61');
INSERT INTO `areas` VALUES ('610830', '清涧县', '6108', '61');
INSERT INTO `areas` VALUES ('610831', '子洲县', '6108', '61');
INSERT INTO `areas` VALUES ('610881', '神木市', '6108', '61');
INSERT INTO `areas` VALUES ('610902', '汉滨区', '6109', '61');
INSERT INTO `areas` VALUES ('610921', '汉阴县', '6109', '61');
INSERT INTO `areas` VALUES ('610922', '石泉县', '6109', '61');
INSERT INTO `areas` VALUES ('610923', '宁陕县', '6109', '61');
INSERT INTO `areas` VALUES ('610924', '紫阳县', '6109', '61');
INSERT INTO `areas` VALUES ('610925', '岚皋县', '6109', '61');
INSERT INTO `areas` VALUES ('610926', '平利县', '6109', '61');
INSERT INTO `areas` VALUES ('610927', '镇坪县', '6109', '61');
INSERT INTO `areas` VALUES ('610929', '白河县', '6109', '61');
INSERT INTO `areas` VALUES ('610981', '旬阳市', '6109', '61');
INSERT INTO `areas` VALUES ('611002', '商州区', '6110', '61');
INSERT INTO `areas` VALUES ('611021', '洛南县', '6110', '61');
INSERT INTO `areas` VALUES ('611022', '丹凤县', '6110', '61');
INSERT INTO `areas` VALUES ('611023', '商南县', '6110', '61');
INSERT INTO `areas` VALUES ('611024', '山阳县', '6110', '61');
INSERT INTO `areas` VALUES ('611025', '镇安县', '6110', '61');
INSERT INTO `areas` VALUES ('611026', '柞水县', '6110', '61');
INSERT INTO `areas` VALUES ('620102', '城关区', '6201', '62');
INSERT INTO `areas` VALUES ('620103', '七里河区', '6201', '62');
INSERT INTO `areas` VALUES ('620104', '西固区', '6201', '62');
INSERT INTO `areas` VALUES ('620105', '安宁区', '6201', '62');
INSERT INTO `areas` VALUES ('620111', '红古区', '6201', '62');
INSERT INTO `areas` VALUES ('620121', '永登县', '6201', '62');
INSERT INTO `areas` VALUES ('620122', '皋兰县', '6201', '62');
INSERT INTO `areas` VALUES ('620123', '榆中县', '6201', '62');
INSERT INTO `areas` VALUES ('620171', '兰州新区', '6201', '62');
INSERT INTO `areas` VALUES ('620201', '嘉峪关市', '6202', '62');
INSERT INTO `areas` VALUES ('620302', '金川区', '6203', '62');
INSERT INTO `areas` VALUES ('620321', '永昌县', '6203', '62');
INSERT INTO `areas` VALUES ('620402', '白银区', '6204', '62');
INSERT INTO `areas` VALUES ('620403', '平川区', '6204', '62');
INSERT INTO `areas` VALUES ('620421', '靖远县', '6204', '62');
INSERT INTO `areas` VALUES ('620422', '会宁县', '6204', '62');
INSERT INTO `areas` VALUES ('620423', '景泰县', '6204', '62');
INSERT INTO `areas` VALUES ('620502', '秦州区', '6205', '62');
INSERT INTO `areas` VALUES ('620503', '麦积区', '6205', '62');
INSERT INTO `areas` VALUES ('620521', '清水县', '6205', '62');
INSERT INTO `areas` VALUES ('620522', '秦安县', '6205', '62');
INSERT INTO `areas` VALUES ('620523', '甘谷县', '6205', '62');
INSERT INTO `areas` VALUES ('620524', '武山县', '6205', '62');
INSERT INTO `areas` VALUES ('620525', '张家川回族自治县', '6205', '62');
INSERT INTO `areas` VALUES ('620602', '凉州区', '6206', '62');
INSERT INTO `areas` VALUES ('620621', '民勤县', '6206', '62');
INSERT INTO `areas` VALUES ('620622', '古浪县', '6206', '62');
INSERT INTO `areas` VALUES ('620623', '天祝藏族自治县', '6206', '62');
INSERT INTO `areas` VALUES ('620702', '甘州区', '6207', '62');
INSERT INTO `areas` VALUES ('620721', '肃南裕固族自治县', '6207', '62');
INSERT INTO `areas` VALUES ('620722', '民乐县', '6207', '62');
INSERT INTO `areas` VALUES ('620723', '临泽县', '6207', '62');
INSERT INTO `areas` VALUES ('620724', '高台县', '6207', '62');
INSERT INTO `areas` VALUES ('620725', '山丹县', '6207', '62');
INSERT INTO `areas` VALUES ('620802', '崆峒区', '6208', '62');
INSERT INTO `areas` VALUES ('620821', '泾川县', '6208', '62');
INSERT INTO `areas` VALUES ('620822', '灵台县', '6208', '62');
INSERT INTO `areas` VALUES ('620823', '崇信县', '6208', '62');
INSERT INTO `areas` VALUES ('620825', '庄浪县', '6208', '62');
INSERT INTO `areas` VALUES ('620826', '静宁县', '6208', '62');
INSERT INTO `areas` VALUES ('620881', '华亭市', '6208', '62');
INSERT INTO `areas` VALUES ('620902', '肃州区', '6209', '62');
INSERT INTO `areas` VALUES ('620921', '金塔县', '6209', '62');
INSERT INTO `areas` VALUES ('620922', '瓜州县', '6209', '62');
INSERT INTO `areas` VALUES ('620923', '肃北蒙古族自治县', '6209', '62');
INSERT INTO `areas` VALUES ('620924', '阿克塞哈萨克族自治县', '6209', '62');
INSERT INTO `areas` VALUES ('620981', '玉门市', '6209', '62');
INSERT INTO `areas` VALUES ('620982', '敦煌市', '6209', '62');
INSERT INTO `areas` VALUES ('621002', '西峰区', '6210', '62');
INSERT INTO `areas` VALUES ('621021', '庆城县', '6210', '62');
INSERT INTO `areas` VALUES ('621022', '环县', '6210', '62');
INSERT INTO `areas` VALUES ('621023', '华池县', '6210', '62');
INSERT INTO `areas` VALUES ('621024', '合水县', '6210', '62');
INSERT INTO `areas` VALUES ('621025', '正宁县', '6210', '62');
INSERT INTO `areas` VALUES ('621026', '宁县', '6210', '62');
INSERT INTO `areas` VALUES ('621027', '镇原县', '6210', '62');
INSERT INTO `areas` VALUES ('621102', '安定区', '6211', '62');
INSERT INTO `areas` VALUES ('621121', '通渭县', '6211', '62');
INSERT INTO `areas` VALUES ('621122', '陇西县', '6211', '62');
INSERT INTO `areas` VALUES ('621123', '渭源县', '6211', '62');
INSERT INTO `areas` VALUES ('621124', '临洮县', '6211', '62');
INSERT INTO `areas` VALUES ('621125', '漳县', '6211', '62');
INSERT INTO `areas` VALUES ('621126', '岷县', '6211', '62');
INSERT INTO `areas` VALUES ('621202', '武都区', '6212', '62');
INSERT INTO `areas` VALUES ('621221', '成县', '6212', '62');
INSERT INTO `areas` VALUES ('621222', '文县', '6212', '62');
INSERT INTO `areas` VALUES ('621223', '宕昌县', '6212', '62');
INSERT INTO `areas` VALUES ('621224', '康县', '6212', '62');
INSERT INTO `areas` VALUES ('621225', '西和县', '6212', '62');
INSERT INTO `areas` VALUES ('621226', '礼县', '6212', '62');
INSERT INTO `areas` VALUES ('621227', '徽县', '6212', '62');
INSERT INTO `areas` VALUES ('621228', '两当县', '6212', '62');
INSERT INTO `areas` VALUES ('622901', '临夏市', '6229', '62');
INSERT INTO `areas` VALUES ('622921', '临夏县', '6229', '62');
INSERT INTO `areas` VALUES ('622922', '康乐县', '6229', '62');
INSERT INTO `areas` VALUES ('622923', '永靖县', '6229', '62');
INSERT INTO `areas` VALUES ('622924', '广河县', '6229', '62');
INSERT INTO `areas` VALUES ('622925', '和政县', '6229', '62');
INSERT INTO `areas` VALUES ('622926', '东乡族自治县', '6229', '62');
INSERT INTO `areas` VALUES ('622927', '积石山保安族东乡族撒拉族自治县', '6229', '62');
INSERT INTO `areas` VALUES ('623001', '合作市', '6230', '62');
INSERT INTO `areas` VALUES ('623021', '临潭县', '6230', '62');
INSERT INTO `areas` VALUES ('623022', '卓尼县', '6230', '62');
INSERT INTO `areas` VALUES ('623023', '舟曲县', '6230', '62');
INSERT INTO `areas` VALUES ('623024', '迭部县', '6230', '62');
INSERT INTO `areas` VALUES ('623025', '玛曲县', '6230', '62');
INSERT INTO `areas` VALUES ('623026', '碌曲县', '6230', '62');
INSERT INTO `areas` VALUES ('623027', '夏河县', '6230', '62');
INSERT INTO `areas` VALUES ('630102', '城东区', '6301', '63');
INSERT INTO `areas` VALUES ('630103', '城中区', '6301', '63');
INSERT INTO `areas` VALUES ('630104', '城西区', '6301', '63');
INSERT INTO `areas` VALUES ('630105', '城北区', '6301', '63');
INSERT INTO `areas` VALUES ('630106', '湟中区', '6301', '63');
INSERT INTO `areas` VALUES ('630121', '大通回族土族自治县', '6301', '63');
INSERT INTO `areas` VALUES ('630123', '湟源县', '6301', '63');
INSERT INTO `areas` VALUES ('630202', '乐都区', '6302', '63');
INSERT INTO `areas` VALUES ('630203', '平安区', '6302', '63');
INSERT INTO `areas` VALUES ('630222', '民和回族土族自治县', '6302', '63');
INSERT INTO `areas` VALUES ('630223', '互助土族自治县', '6302', '63');
INSERT INTO `areas` VALUES ('630224', '化隆回族自治县', '6302', '63');
INSERT INTO `areas` VALUES ('630225', '循化撒拉族自治县', '6302', '63');
INSERT INTO `areas` VALUES ('632221', '门源回族自治县', '6322', '63');
INSERT INTO `areas` VALUES ('632222', '祁连县', '6322', '63');
INSERT INTO `areas` VALUES ('632223', '海晏县', '6322', '63');
INSERT INTO `areas` VALUES ('632224', '刚察县', '6322', '63');
INSERT INTO `areas` VALUES ('632301', '同仁市', '6323', '63');
INSERT INTO `areas` VALUES ('632322', '尖扎县', '6323', '63');
INSERT INTO `areas` VALUES ('632323', '泽库县', '6323', '63');
INSERT INTO `areas` VALUES ('632324', '河南蒙古族自治县', '6323', '63');
INSERT INTO `areas` VALUES ('632521', '共和县', '6325', '63');
INSERT INTO `areas` VALUES ('632522', '同德县', '6325', '63');
INSERT INTO `areas` VALUES ('632523', '贵德县', '6325', '63');
INSERT INTO `areas` VALUES ('632524', '兴海县', '6325', '63');
INSERT INTO `areas` VALUES ('632525', '贵南县', '6325', '63');
INSERT INTO `areas` VALUES ('632621', '玛沁县', '6326', '63');
INSERT INTO `areas` VALUES ('632622', '班玛县', '6326', '63');
INSERT INTO `areas` VALUES ('632623', '甘德县', '6326', '63');
INSERT INTO `areas` VALUES ('632624', '达日县', '6326', '63');
INSERT INTO `areas` VALUES ('632625', '久治县', '6326', '63');
INSERT INTO `areas` VALUES ('632626', '玛多县', '6326', '63');
INSERT INTO `areas` VALUES ('632701', '玉树市', '6327', '63');
INSERT INTO `areas` VALUES ('632722', '杂多县', '6327', '63');
INSERT INTO `areas` VALUES ('632723', '称多县', '6327', '63');
INSERT INTO `areas` VALUES ('632724', '治多县', '6327', '63');
INSERT INTO `areas` VALUES ('632725', '囊谦县', '6327', '63');
INSERT INTO `areas` VALUES ('632726', '曲麻莱县', '6327', '63');
INSERT INTO `areas` VALUES ('632801', '格尔木市', '6328', '63');
INSERT INTO `areas` VALUES ('632802', '德令哈市', '6328', '63');
INSERT INTO `areas` VALUES ('632803', '茫崖市', '6328', '63');
INSERT INTO `areas` VALUES ('632821', '乌兰县', '6328', '63');
INSERT INTO `areas` VALUES ('632822', '都兰县', '6328', '63');
INSERT INTO `areas` VALUES ('632823', '天峻县', '6328', '63');
INSERT INTO `areas` VALUES ('632857', '大柴旦行政委员会', '6328', '63');
INSERT INTO `areas` VALUES ('640104', '兴庆区', '6401', '64');
INSERT INTO `areas` VALUES ('640105', '西夏区', '6401', '64');
INSERT INTO `areas` VALUES ('640106', '金凤区', '6401', '64');
INSERT INTO `areas` VALUES ('640121', '永宁县', '6401', '64');
INSERT INTO `areas` VALUES ('640122', '贺兰县', '6401', '64');
INSERT INTO `areas` VALUES ('640181', '灵武市', '6401', '64');
INSERT INTO `areas` VALUES ('640202', '大武口区', '6402', '64');
INSERT INTO `areas` VALUES ('640205', '惠农区', '6402', '64');
INSERT INTO `areas` VALUES ('640221', '平罗县', '6402', '64');
INSERT INTO `areas` VALUES ('640302', '利通区', '6403', '64');
INSERT INTO `areas` VALUES ('640303', '红寺堡区', '6403', '64');
INSERT INTO `areas` VALUES ('640323', '盐池县', '6403', '64');
INSERT INTO `areas` VALUES ('640324', '同心县', '6403', '64');
INSERT INTO `areas` VALUES ('640381', '青铜峡市', '6403', '64');
INSERT INTO `areas` VALUES ('640402', '原州区', '6404', '64');
INSERT INTO `areas` VALUES ('640422', '西吉县', '6404', '64');
INSERT INTO `areas` VALUES ('640423', '隆德县', '6404', '64');
INSERT INTO `areas` VALUES ('640424', '泾源县', '6404', '64');
INSERT INTO `areas` VALUES ('640425', '彭阳县', '6404', '64');
INSERT INTO `areas` VALUES ('640502', '沙坡头区', '6405', '64');
INSERT INTO `areas` VALUES ('640521', '中宁县', '6405', '64');
INSERT INTO `areas` VALUES ('640522', '海原县', '6405', '64');
INSERT INTO `areas` VALUES ('650102', '天山区', '6501', '65');
INSERT INTO `areas` VALUES ('650103', '沙依巴克区', '6501', '65');
INSERT INTO `areas` VALUES ('650104', '新市区', '6501', '65');
INSERT INTO `areas` VALUES ('650105', '水磨沟区', '6501', '65');
INSERT INTO `areas` VALUES ('650106', '头屯河区', '6501', '65');
INSERT INTO `areas` VALUES ('650107', '达坂城区', '6501', '65');
INSERT INTO `areas` VALUES ('650109', '米东区', '6501', '65');
INSERT INTO `areas` VALUES ('650121', '乌鲁木齐县', '6501', '65');
INSERT INTO `areas` VALUES ('650202', '独山子区', '6502', '65');
INSERT INTO `areas` VALUES ('650203', '克拉玛依区', '6502', '65');
INSERT INTO `areas` VALUES ('650204', '白碱滩区', '6502', '65');
INSERT INTO `areas` VALUES ('650205', '乌尔禾区', '6502', '65');
INSERT INTO `areas` VALUES ('650402', '高昌区', '6504', '65');
INSERT INTO `areas` VALUES ('650421', '鄯善县', '6504', '65');
INSERT INTO `areas` VALUES ('650422', '托克逊县', '6504', '65');
INSERT INTO `areas` VALUES ('650502', '伊州区', '6505', '65');
INSERT INTO `areas` VALUES ('650521', '巴里坤哈萨克自治县', '6505', '65');
INSERT INTO `areas` VALUES ('650522', '伊吾县', '6505', '65');
INSERT INTO `areas` VALUES ('652301', '昌吉市', '6523', '65');
INSERT INTO `areas` VALUES ('652302', '阜康市', '6523', '65');
INSERT INTO `areas` VALUES ('652323', '呼图壁县', '6523', '65');
INSERT INTO `areas` VALUES ('652324', '玛纳斯县', '6523', '65');
INSERT INTO `areas` VALUES ('652325', '奇台县', '6523', '65');
INSERT INTO `areas` VALUES ('652327', '吉木萨尔县', '6523', '65');
INSERT INTO `areas` VALUES ('652328', '木垒哈萨克自治县', '6523', '65');
INSERT INTO `areas` VALUES ('652701', '博乐市', '6527', '65');
INSERT INTO `areas` VALUES ('652702', '阿拉山口市', '6527', '65');
INSERT INTO `areas` VALUES ('652722', '精河县', '6527', '65');
INSERT INTO `areas` VALUES ('652723', '温泉县', '6527', '65');
INSERT INTO `areas` VALUES ('652801', '库尔勒市', '6528', '65');
INSERT INTO `areas` VALUES ('652822', '轮台县', '6528', '65');
INSERT INTO `areas` VALUES ('652823', '尉犁县', '6528', '65');
INSERT INTO `areas` VALUES ('652824', '若羌县', '6528', '65');
INSERT INTO `areas` VALUES ('652825', '且末县', '6528', '65');
INSERT INTO `areas` VALUES ('652826', '焉耆回族自治县', '6528', '65');
INSERT INTO `areas` VALUES ('652827', '和静县', '6528', '65');
INSERT INTO `areas` VALUES ('652828', '和硕县', '6528', '65');
INSERT INTO `areas` VALUES ('652829', '博湖县', '6528', '65');
INSERT INTO `areas` VALUES ('652871', '库尔勒经济技术开发区', '6528', '65');
INSERT INTO `areas` VALUES ('652901', '阿克苏市', '6529', '65');
INSERT INTO `areas` VALUES ('652902', '库车市', '6529', '65');
INSERT INTO `areas` VALUES ('652922', '温宿县', '6529', '65');
INSERT INTO `areas` VALUES ('652924', '沙雅县', '6529', '65');
INSERT INTO `areas` VALUES ('652925', '新和县', '6529', '65');
INSERT INTO `areas` VALUES ('652926', '拜城县', '6529', '65');
INSERT INTO `areas` VALUES ('652927', '乌什县', '6529', '65');
INSERT INTO `areas` VALUES ('652928', '阿瓦提县', '6529', '65');
INSERT INTO `areas` VALUES ('652929', '柯坪县', '6529', '65');
INSERT INTO `areas` VALUES ('653001', '阿图什市', '6530', '65');
INSERT INTO `areas` VALUES ('653022', '阿克陶县', '6530', '65');
INSERT INTO `areas` VALUES ('653023', '阿合奇县', '6530', '65');
INSERT INTO `areas` VALUES ('653024', '乌恰县', '6530', '65');
INSERT INTO `areas` VALUES ('653101', '喀什市', '6531', '65');
INSERT INTO `areas` VALUES ('653121', '疏附县', '6531', '65');
INSERT INTO `areas` VALUES ('653122', '疏勒县', '6531', '65');
INSERT INTO `areas` VALUES ('653123', '英吉沙县', '6531', '65');
INSERT INTO `areas` VALUES ('653124', '泽普县', '6531', '65');
INSERT INTO `areas` VALUES ('653125', '莎车县', '6531', '65');
INSERT INTO `areas` VALUES ('653126', '叶城县', '6531', '65');
INSERT INTO `areas` VALUES ('653127', '麦盖提县', '6531', '65');
INSERT INTO `areas` VALUES ('653128', '岳普湖县', '6531', '65');
INSERT INTO `areas` VALUES ('653129', '伽师县', '6531', '65');
INSERT INTO `areas` VALUES ('653130', '巴楚县', '6531', '65');
INSERT INTO `areas` VALUES ('653131', '塔什库尔干塔吉克自治县', '6531', '65');
INSERT INTO `areas` VALUES ('653201', '和田市', '6532', '65');
INSERT INTO `areas` VALUES ('653221', '和田县', '6532', '65');
INSERT INTO `areas` VALUES ('653222', '墨玉县', '6532', '65');
INSERT INTO `areas` VALUES ('653223', '皮山县', '6532', '65');
INSERT INTO `areas` VALUES ('653224', '洛浦县', '6532', '65');
INSERT INTO `areas` VALUES ('653225', '策勒县', '6532', '65');
INSERT INTO `areas` VALUES ('653226', '于田县', '6532', '65');
INSERT INTO `areas` VALUES ('653227', '民丰县', '6532', '65');
INSERT INTO `areas` VALUES ('654002', '伊宁市', '6540', '65');
INSERT INTO `areas` VALUES ('654003', '奎屯市', '6540', '65');
INSERT INTO `areas` VALUES ('654004', '霍尔果斯市', '6540', '65');
INSERT INTO `areas` VALUES ('654021', '伊宁县', '6540', '65');
INSERT INTO `areas` VALUES ('654022', '察布查尔锡伯自治县', '6540', '65');
INSERT INTO `areas` VALUES ('654023', '霍城县', '6540', '65');
INSERT INTO `areas` VALUES ('654024', '巩留县', '6540', '65');
INSERT INTO `areas` VALUES ('654025', '新源县', '6540', '65');
INSERT INTO `areas` VALUES ('654026', '昭苏县', '6540', '65');
INSERT INTO `areas` VALUES ('654027', '特克斯县', '6540', '65');
INSERT INTO `areas` VALUES ('654028', '尼勒克县', '6540', '65');
INSERT INTO `areas` VALUES ('654201', '塔城市', '6542', '65');
INSERT INTO `areas` VALUES ('654202', '乌苏市', '6542', '65');
INSERT INTO `areas` VALUES ('654203', '沙湾市', '6542', '65');
INSERT INTO `areas` VALUES ('654221', '额敏县', '6542', '65');
INSERT INTO `areas` VALUES ('654224', '托里县', '6542', '65');
INSERT INTO `areas` VALUES ('654225', '裕民县', '6542', '65');
INSERT INTO `areas` VALUES ('654226', '和布克赛尔蒙古自治县', '6542', '65');
INSERT INTO `areas` VALUES ('654301', '阿勒泰市', '6543', '65');
INSERT INTO `areas` VALUES ('654321', '布尔津县', '6543', '65');
INSERT INTO `areas` VALUES ('654322', '富蕴县', '6543', '65');
INSERT INTO `areas` VALUES ('654323', '福海县', '6543', '65');
INSERT INTO `areas` VALUES ('654324', '哈巴河县', '6543', '65');
INSERT INTO `areas` VALUES ('654325', '青河县', '6543', '65');
INSERT INTO `areas` VALUES ('654326', '吉木乃县', '6543', '65');
INSERT INTO `areas` VALUES ('659001', '石河子市', '6590', '65');
INSERT INTO `areas` VALUES ('659002', '阿拉尔市', '6590', '65');
INSERT INTO `areas` VALUES ('659003', '图木舒克市', '6590', '65');
INSERT INTO `areas` VALUES ('659004', '五家渠市', '6590', '65');
INSERT INTO `areas` VALUES ('659005', '北屯市', '6590', '65');
INSERT INTO `areas` VALUES ('659006', '铁门关市', '6590', '65');
INSERT INTO `areas` VALUES ('659007', '双河市', '6590', '65');
INSERT INTO `areas` VALUES ('659008', '可克达拉市', '6590', '65');
INSERT INTO `areas` VALUES ('659009', '昆玉市', '6590', '65');
INSERT INTO `areas` VALUES ('659010', '胡杨河市', '6590', '65');
INSERT INTO `areas` VALUES ('659011', '新星市', '6590', '65');

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '城市code',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '城市名',
  `province_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份code',
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '城市表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1101', '市辖区', '11');
INSERT INTO `city` VALUES ('1201', '市辖区', '12');
INSERT INTO `city` VALUES ('1301', '石家庄市', '13');
INSERT INTO `city` VALUES ('1302', '唐山市', '13');
INSERT INTO `city` VALUES ('1303', '秦皇岛市', '13');
INSERT INTO `city` VALUES ('1304', '邯郸市', '13');
INSERT INTO `city` VALUES ('1305', '邢台市', '13');
INSERT INTO `city` VALUES ('1306', '保定市', '13');
INSERT INTO `city` VALUES ('1307', '张家口市', '13');
INSERT INTO `city` VALUES ('1308', '承德市', '13');
INSERT INTO `city` VALUES ('1309', '沧州市', '13');
INSERT INTO `city` VALUES ('1310', '廊坊市', '13');
INSERT INTO `city` VALUES ('1311', '衡水市', '13');
INSERT INTO `city` VALUES ('1401', '太原市', '14');
INSERT INTO `city` VALUES ('1402', '大同市', '14');
INSERT INTO `city` VALUES ('1403', '阳泉市', '14');
INSERT INTO `city` VALUES ('1404', '长治市', '14');
INSERT INTO `city` VALUES ('1405', '晋城市', '14');
INSERT INTO `city` VALUES ('1406', '朔州市', '14');
INSERT INTO `city` VALUES ('1407', '晋中市', '14');
INSERT INTO `city` VALUES ('1408', '运城市', '14');
INSERT INTO `city` VALUES ('1409', '忻州市', '14');
INSERT INTO `city` VALUES ('1410', '临汾市', '14');
INSERT INTO `city` VALUES ('1411', '吕梁市', '14');
INSERT INTO `city` VALUES ('1501', '呼和浩特市', '15');
INSERT INTO `city` VALUES ('1502', '包头市', '15');
INSERT INTO `city` VALUES ('1503', '乌海市', '15');
INSERT INTO `city` VALUES ('1504', '赤峰市', '15');
INSERT INTO `city` VALUES ('1505', '通辽市', '15');
INSERT INTO `city` VALUES ('1506', '鄂尔多斯市', '15');
INSERT INTO `city` VALUES ('1507', '呼伦贝尔市', '15');
INSERT INTO `city` VALUES ('1508', '巴彦淖尔市', '15');
INSERT INTO `city` VALUES ('1509', '乌兰察布市', '15');
INSERT INTO `city` VALUES ('1522', '兴安盟', '15');
INSERT INTO `city` VALUES ('1525', '锡林郭勒盟', '15');
INSERT INTO `city` VALUES ('1529', '阿拉善盟', '15');
INSERT INTO `city` VALUES ('2101', '沈阳市', '21');
INSERT INTO `city` VALUES ('2102', '大连市', '21');
INSERT INTO `city` VALUES ('2103', '鞍山市', '21');
INSERT INTO `city` VALUES ('2104', '抚顺市', '21');
INSERT INTO `city` VALUES ('2105', '本溪市', '21');
INSERT INTO `city` VALUES ('2106', '丹东市', '21');
INSERT INTO `city` VALUES ('2107', '锦州市', '21');
INSERT INTO `city` VALUES ('2108', '营口市', '21');
INSERT INTO `city` VALUES ('2109', '阜新市', '21');
INSERT INTO `city` VALUES ('2110', '辽阳市', '21');
INSERT INTO `city` VALUES ('2111', '盘锦市', '21');
INSERT INTO `city` VALUES ('2112', '铁岭市', '21');
INSERT INTO `city` VALUES ('2113', '朝阳市', '21');
INSERT INTO `city` VALUES ('2114', '葫芦岛市', '21');
INSERT INTO `city` VALUES ('2201', '长春市', '22');
INSERT INTO `city` VALUES ('2202', '吉林市', '22');
INSERT INTO `city` VALUES ('2203', '四平市', '22');
INSERT INTO `city` VALUES ('2204', '辽源市', '22');
INSERT INTO `city` VALUES ('2205', '通化市', '22');
INSERT INTO `city` VALUES ('2206', '白山市', '22');
INSERT INTO `city` VALUES ('2207', '松原市', '22');
INSERT INTO `city` VALUES ('2208', '白城市', '22');
INSERT INTO `city` VALUES ('2224', '延边朝鲜族自治州', '22');
INSERT INTO `city` VALUES ('2301', '哈尔滨市', '23');
INSERT INTO `city` VALUES ('2302', '齐齐哈尔市', '23');
INSERT INTO `city` VALUES ('2303', '鸡西市', '23');
INSERT INTO `city` VALUES ('2304', '鹤岗市', '23');
INSERT INTO `city` VALUES ('2305', '双鸭山市', '23');
INSERT INTO `city` VALUES ('2306', '大庆市', '23');
INSERT INTO `city` VALUES ('2307', '伊春市', '23');
INSERT INTO `city` VALUES ('2308', '佳木斯市', '23');
INSERT INTO `city` VALUES ('2309', '七台河市', '23');
INSERT INTO `city` VALUES ('2310', '牡丹江市', '23');
INSERT INTO `city` VALUES ('2311', '黑河市', '23');
INSERT INTO `city` VALUES ('2312', '绥化市', '23');
INSERT INTO `city` VALUES ('2327', '大兴安岭地区', '23');
INSERT INTO `city` VALUES ('3101', '市辖区', '31');
INSERT INTO `city` VALUES ('3201', '南京市', '32');
INSERT INTO `city` VALUES ('3202', '无锡市', '32');
INSERT INTO `city` VALUES ('3203', '徐州市', '32');
INSERT INTO `city` VALUES ('3204', '常州市', '32');
INSERT INTO `city` VALUES ('3205', '苏州市', '32');
INSERT INTO `city` VALUES ('3206', '南通市', '32');
INSERT INTO `city` VALUES ('3207', '连云港市', '32');
INSERT INTO `city` VALUES ('3208', '淮安市', '32');
INSERT INTO `city` VALUES ('3209', '盐城市', '32');
INSERT INTO `city` VALUES ('3210', '扬州市', '32');
INSERT INTO `city` VALUES ('3211', '镇江市', '32');
INSERT INTO `city` VALUES ('3212', '泰州市', '32');
INSERT INTO `city` VALUES ('3213', '宿迁市', '32');
INSERT INTO `city` VALUES ('3301', '杭州市', '33');
INSERT INTO `city` VALUES ('3302', '宁波市', '33');
INSERT INTO `city` VALUES ('3303', '温州市', '33');
INSERT INTO `city` VALUES ('3304', '嘉兴市', '33');
INSERT INTO `city` VALUES ('3305', '湖州市', '33');
INSERT INTO `city` VALUES ('3306', '绍兴市', '33');
INSERT INTO `city` VALUES ('3307', '金华市', '33');
INSERT INTO `city` VALUES ('3308', '衢州市', '33');
INSERT INTO `city` VALUES ('3309', '舟山市', '33');
INSERT INTO `city` VALUES ('3310', '台州市', '33');
INSERT INTO `city` VALUES ('3311', '丽水市', '33');
INSERT INTO `city` VALUES ('3401', '合肥市', '34');
INSERT INTO `city` VALUES ('3402', '芜湖市', '34');
INSERT INTO `city` VALUES ('3403', '蚌埠市', '34');
INSERT INTO `city` VALUES ('3404', '淮南市', '34');
INSERT INTO `city` VALUES ('3405', '马鞍山市', '34');
INSERT INTO `city` VALUES ('3406', '淮北市', '34');
INSERT INTO `city` VALUES ('3407', '铜陵市', '34');
INSERT INTO `city` VALUES ('3408', '安庆市', '34');
INSERT INTO `city` VALUES ('3410', '黄山市', '34');
INSERT INTO `city` VALUES ('3411', '滁州市', '34');
INSERT INTO `city` VALUES ('3412', '阜阳市', '34');
INSERT INTO `city` VALUES ('3413', '宿州市', '34');
INSERT INTO `city` VALUES ('3415', '六安市', '34');
INSERT INTO `city` VALUES ('3416', '亳州市', '34');
INSERT INTO `city` VALUES ('3417', '池州市', '34');
INSERT INTO `city` VALUES ('3418', '宣城市', '34');
INSERT INTO `city` VALUES ('3501', '福州市', '35');
INSERT INTO `city` VALUES ('3502', '厦门市', '35');
INSERT INTO `city` VALUES ('3503', '莆田市', '35');
INSERT INTO `city` VALUES ('3504', '三明市', '35');
INSERT INTO `city` VALUES ('3505', '泉州市', '35');
INSERT INTO `city` VALUES ('3506', '漳州市', '35');
INSERT INTO `city` VALUES ('3507', '南平市', '35');
INSERT INTO `city` VALUES ('3508', '龙岩市', '35');
INSERT INTO `city` VALUES ('3509', '宁德市', '35');
INSERT INTO `city` VALUES ('3601', '南昌市', '36');
INSERT INTO `city` VALUES ('3602', '景德镇市', '36');
INSERT INTO `city` VALUES ('3603', '萍乡市', '36');
INSERT INTO `city` VALUES ('3604', '九江市', '36');
INSERT INTO `city` VALUES ('3605', '新余市', '36');
INSERT INTO `city` VALUES ('3606', '鹰潭市', '36');
INSERT INTO `city` VALUES ('3607', '赣州市', '36');
INSERT INTO `city` VALUES ('3608', '吉安市', '36');
INSERT INTO `city` VALUES ('3609', '宜春市', '36');
INSERT INTO `city` VALUES ('3610', '抚州市', '36');
INSERT INTO `city` VALUES ('3611', '上饶市', '36');
INSERT INTO `city` VALUES ('3701', '济南市', '37');
INSERT INTO `city` VALUES ('3702', '青岛市', '37');
INSERT INTO `city` VALUES ('3703', '淄博市', '37');
INSERT INTO `city` VALUES ('3704', '枣庄市', '37');
INSERT INTO `city` VALUES ('3705', '东营市', '37');
INSERT INTO `city` VALUES ('3706', '烟台市', '37');
INSERT INTO `city` VALUES ('3707', '潍坊市', '37');
INSERT INTO `city` VALUES ('3708', '济宁市', '37');
INSERT INTO `city` VALUES ('3709', '泰安市', '37');
INSERT INTO `city` VALUES ('3710', '威海市', '37');
INSERT INTO `city` VALUES ('3711', '日照市', '37');
INSERT INTO `city` VALUES ('3713', '临沂市', '37');
INSERT INTO `city` VALUES ('3714', '德州市', '37');
INSERT INTO `city` VALUES ('3715', '聊城市', '37');
INSERT INTO `city` VALUES ('3716', '滨州市', '37');
INSERT INTO `city` VALUES ('3717', '菏泽市', '37');
INSERT INTO `city` VALUES ('4101', '郑州市', '41');
INSERT INTO `city` VALUES ('4102', '开封市', '41');
INSERT INTO `city` VALUES ('4103', '洛阳市', '41');
INSERT INTO `city` VALUES ('4104', '平顶山市', '41');
INSERT INTO `city` VALUES ('4105', '安阳市', '41');
INSERT INTO `city` VALUES ('4106', '鹤壁市', '41');
INSERT INTO `city` VALUES ('4107', '新乡市', '41');
INSERT INTO `city` VALUES ('4108', '焦作市', '41');
INSERT INTO `city` VALUES ('4109', '濮阳市', '41');
INSERT INTO `city` VALUES ('4110', '许昌市', '41');
INSERT INTO `city` VALUES ('4111', '漯河市', '41');
INSERT INTO `city` VALUES ('4112', '三门峡市', '41');
INSERT INTO `city` VALUES ('4113', '南阳市', '41');
INSERT INTO `city` VALUES ('4114', '商丘市', '41');
INSERT INTO `city` VALUES ('4115', '信阳市', '41');
INSERT INTO `city` VALUES ('4116', '周口市', '41');
INSERT INTO `city` VALUES ('4117', '驻马店市', '41');
INSERT INTO `city` VALUES ('4190', '省直辖县级行政区划', '41');
INSERT INTO `city` VALUES ('4201', '武汉市', '42');
INSERT INTO `city` VALUES ('4202', '黄石市', '42');
INSERT INTO `city` VALUES ('4203', '十堰市', '42');
INSERT INTO `city` VALUES ('4205', '宜昌市', '42');
INSERT INTO `city` VALUES ('4206', '襄阳市', '42');
INSERT INTO `city` VALUES ('4207', '鄂州市', '42');
INSERT INTO `city` VALUES ('4208', '荆门市', '42');
INSERT INTO `city` VALUES ('4209', '孝感市', '42');
INSERT INTO `city` VALUES ('4210', '荆州市', '42');
INSERT INTO `city` VALUES ('4211', '黄冈市', '42');
INSERT INTO `city` VALUES ('4212', '咸宁市', '42');
INSERT INTO `city` VALUES ('4213', '随州市', '42');
INSERT INTO `city` VALUES ('4228', '恩施土家族苗族自治州', '42');
INSERT INTO `city` VALUES ('4290', '省直辖县级行政区划', '42');
INSERT INTO `city` VALUES ('4301', '长沙市', '43');
INSERT INTO `city` VALUES ('4302', '株洲市', '43');
INSERT INTO `city` VALUES ('4303', '湘潭市', '43');
INSERT INTO `city` VALUES ('4304', '衡阳市', '43');
INSERT INTO `city` VALUES ('4305', '邵阳市', '43');
INSERT INTO `city` VALUES ('4306', '岳阳市', '43');
INSERT INTO `city` VALUES ('4307', '常德市', '43');
INSERT INTO `city` VALUES ('4308', '张家界市', '43');
INSERT INTO `city` VALUES ('4309', '益阳市', '43');
INSERT INTO `city` VALUES ('4310', '郴州市', '43');
INSERT INTO `city` VALUES ('4311', '永州市', '43');
INSERT INTO `city` VALUES ('4312', '怀化市', '43');
INSERT INTO `city` VALUES ('4313', '娄底市', '43');
INSERT INTO `city` VALUES ('4331', '湘西土家族苗族自治州', '43');
INSERT INTO `city` VALUES ('4401', '广州市', '44');
INSERT INTO `city` VALUES ('4402', '韶关市', '44');
INSERT INTO `city` VALUES ('4403', '深圳市', '44');
INSERT INTO `city` VALUES ('4404', '珠海市', '44');
INSERT INTO `city` VALUES ('4405', '汕头市', '44');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (5, 'product_sku', '商品sku表', NULL, NULL, 'ProductSku', 'crud', 'com.ruoyi.system', 'system', 'sku', '商品sku', 'ruoyi', '0', '/', NULL, 'admin', '2022-03-15 22:22:08', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (6, 'product_spu', '商品spu表', NULL, NULL, 'ProductSpu', 'crud', 'com.ruoyi.system', 'system', 'spu', '商品spu', 'ruoyi', '0', '/', NULL, 'admin', '2022-03-15 22:22:08', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (7, 'user_sku_star', '用户sku收藏表', NULL, NULL, 'UserSkuStar', 'crud', 'com.ruoyi.system', 'system', 'star', '用户sku收藏', 'ruoyi', '0', '/', NULL, 'admin', '2022-03-15 22:22:08', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (8, 'order_sub', '用户订单子表', NULL, NULL, 'OrderSub', 'crud', 'com.ruoyi.system', 'system', 'sub', '用户订单子', 'ruoyi', '0', '/', NULL, 'admin', '2022-03-18 13:21:15', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (9, 'application', '报名信息表', NULL, NULL, 'Application', 'crud', 'com.ruoyi.system', 'system', 'application', '报名信息', 'ruoyi', '0', '/', NULL, 'admin', '2022-05-08 14:25:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (10, 'areas', '', NULL, NULL, 'Areas', 'crud', 'com.ruoyi.system', 'system', 'areas', NULL, 'ruoyi', '0', '/', NULL, 'admin', '2022-05-08 14:25:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (11, 'city', '城市表', NULL, NULL, 'City', 'crud', 'com.ruoyi.system', 'system', 'city', '城市', 'ruoyi', '0', '/', NULL, 'admin', '2022-05-08 14:25:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (12, 'major', '专业表', NULL, NULL, 'Major', 'crud', 'com.ruoyi.system', 'system', 'major', '专业', 'ruoyi', '0', '/', NULL, 'admin', '2022-05-08 14:25:06', '', NULL, NULL);
INSERT INTO `gen_table` VALUES (13, 'province', '省份表', NULL, NULL, 'Province', 'crud', 'com.ruoyi.system', 'system', 'province', '省份', 'ruoyi', '0', '/', NULL, 'admin', '2022-05-08 14:25:06', '', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (31, '5', 'id', '商品ID', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (32, '5', 'spu_id', 'spu的id', 'bigint', 'Long', 'spuId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (33, '5', 'name', '商品名', 'varchar(30)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (34, '5', 'sum', '商品数量', 'int', 'Long', 'sum', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (35, '5', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 5, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (36, '5', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (37, '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (38, '5', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (39, '5', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (40, '5', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (41, '6', 'id', '商品ID', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (42, '6', 'spu_id', 'spu的id', 'bigint', 'Long', 'spuId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (43, '6', 'name', '商品名', 'varchar(30)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (44, '6', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 4, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (45, '6', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 5, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (46, '6', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (47, '6', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (48, '6', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (49, '6', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 9, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (50, '7', 'user_id', '用户id', 'bigint', 'Long', 'userId', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (51, '7', 'spu_id', 'spu的id', 'bigint', 'Long', 'spuId', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 2, 'admin', '2022-03-15 22:22:08', '', NULL);
INSERT INTO `gen_table_column` VALUES (52, '8', 'id', '主键', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-03-18 13:21:15', '', NULL);
INSERT INTO `gen_table_column` VALUES (53, '8', 'user_id', '用户id', 'bigint', 'Long', 'userId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-03-18 13:21:15', '', NULL);
INSERT INTO `gen_table_column` VALUES (54, '8', 'sku_id', '商品id', 'bigint', 'Long', 'skuId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-03-18 13:21:15', '', NULL);
INSERT INTO `gen_table_column` VALUES (55, '9', 'id', '编号', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (56, '9', 'student_name', '学生姓名', 'varchar(20)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (57, '9', 'area_code', '区域code', 'varchar(20)', 'String', 'areaCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (58, '9', 'edu_code', '学历code', 'varchar(20)', 'String', 'eduCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (59, '9', 'major_code', '专业code', 'varchar(20)', 'String', 'majorCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (60, '9', 'major_id', '专业id', 'bigint', 'Long', 'majorId', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (61, '9', 'phone', '手机号', 'varchar(20)', 'String', 'phone', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (62, '9', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (63, '9', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (64, '9', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (65, '10', 'code', '区县code', 'varchar(50)', 'String', 'code', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (66, '10', 'name', '区县名', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (67, '10', 'city_code', '城市code', 'varchar(50)', 'String', 'cityCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (68, '10', 'province_code', '省份code', 'varchar(50)', 'String', 'provinceCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (69, '11', 'code', '城市code', 'varchar(50)', 'String', 'code', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (70, '11', 'name', '城市名', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (71, '11', 'province_code', '省份code', 'varchar(50)', 'String', 'provinceCode', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (72, '12', 'id', '编号', 'bigint', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (73, '12', 'code', '专业代码', 'varchar(50)', 'String', 'code', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (74, '12', 'name', '专业名称', 'varchar(50)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (75, '12', 'level', '专业层次code', 'varchar(50)', 'String', 'level', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (76, '12', 'school_belonged', '学校归属', 'varchar(50)', 'String', 'schoolBelonged', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (77, '12', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (78, '12', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (79, '12', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 8, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (80, '13', 'code', '省份code', 'varchar(50)', 'String', 'code', '1', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-05-08 14:25:06', '', NULL);
INSERT INTO `gen_table_column` VALUES (81, '13', 'name', '省份名', 'varchar(50)', 'String', 'name', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-05-08 14:25:06', '', NULL);

-- ----------------------------
-- Table structure for major
-- ----------------------------
DROP TABLE IF EXISTS `major`;
CREATE TABLE `major`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '专业代码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '专业名称',
  `level` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '专业层次code',
  `school_belonged` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学校归属',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '专业表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of major
-- ----------------------------
INSERT INTO `major` VALUES (1, '12121', '计算机科学', 'dazhuan', '清华', '2022-05-08 15:09:50', '2022-05-08 15:37:51', '分');
INSERT INTO `major` VALUES (2, '2345', '生命科学', 'gaozhong', '北京大学', '2022-05-09 19:23:31', NULL, NULL);
INSERT INTO `major` VALUES (3, '1234', '计算机科学', 'dazhuan', '长春理工大学', '2022-06-10 21:06:27', '2022-06-10 21:06:27', NULL);
INSERT INTO `major` VALUES (4, '12341', '计算机网络', 'dazhuan', '长春工业大学', '2022-06-10 21:06:27', '2022-06-10 21:06:27', NULL);
INSERT INTO `major` VALUES (5, '451234', '软件工程', 'dazhuan', '长春理工大学', '2022-06-10 21:06:27', '2022-06-10 21:06:27', NULL);

-- ----------------------------
-- Table structure for mall_product_tag
-- ----------------------------
DROP TABLE IF EXISTS `mall_product_tag`;
CREATE TABLE `mall_product_tag`  (
  `tag_id` bigint NOT NULL AUTO_INCREMENT COMMENT '标签编号',
  `product_id` bigint NOT NULL COMMENT '藏品id',
  `product_group_id` bigint NOT NULL COMMENT '藏品系列id',
  PRIMARY KEY (`tag_id`, `product_group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '藏品标签关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mall_product_tag
-- ----------------------------

-- ----------------------------
-- Table structure for mall_tag
-- ----------------------------
DROP TABLE IF EXISTS `mall_tag`;
CREATE TABLE `mall_tag`  (
  `tag_id` bigint NOT NULL AUTO_INCREMENT COMMENT '标签编号',
  `tag_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签名称',
  `use_status` tinyint NOT NULL DEFAULT 0 COMMENT '使用状态（0代表没有使用 1代表使用中）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签介绍/备注',
  `del_flag` bigint NULL DEFAULT 0 COMMENT '删除标志（0代表存在 其他数字代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`tag_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '标签表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mall_tag
-- ----------------------------

-- ----------------------------
-- Table structure for order_sub
-- ----------------------------
DROP TABLE IF EXISTS `order_sub`;
CREATE TABLE `order_sub`  (
  `id` bigint NULL DEFAULT NULL COMMENT '主键',
  `user_id` bigint NOT NULL COMMENT '用户id',
  `sku_id` bigint NOT NULL COMMENT '商品id',
  PRIMARY KEY (`user_id`, `sku_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户订单子表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of order_sub
-- ----------------------------
INSERT INTO `order_sub` VALUES (NULL, 2, 1);
INSERT INTO `order_sub` VALUES (NULL, 101, 1);
INSERT INTO `order_sub` VALUES (NULL, 101, 5);
INSERT INTO `order_sub` VALUES (NULL, 101, 104);
INSERT INTO `order_sub` VALUES (NULL, 103, 1);
INSERT INTO `order_sub` VALUES (NULL, 103, 5);
INSERT INTO `order_sub` VALUES (NULL, 103, 104);

-- ----------------------------
-- Table structure for product_sku
-- ----------------------------
DROP TABLE IF EXISTS `product_sku`;
CREATE TABLE `product_sku`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `spu_id` bigint NULL DEFAULT NULL COMMENT 'spu的id',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名',
  `show_price` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sum` int NULL DEFAULT NULL COMMENT '商品数量',
  `banner_url` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '首页封面url',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品sku表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of product_sku
-- ----------------------------
INSERT INTO `product_sku` VALUES (1, NULL, 'Zero创世纪念勋章', '18.50', 10, 'http://zero-gz.oss-cn-guangzhou.aliyuncs.com/product%2Fbanner_todu%2FZero%E5%88%9B%E4%B8%96%E7%BA%AA%E5%BF%B5%E5%8B%8B%E7%AB%A0.jpg?OSSAccessKeyId=LTAI5tHZwvSYDLu5b84RMEWr&Expires=1678718048&Signature=uCoCOvyNIHTMYXLdmsWkOjphmrY%3D', '0', '', '2022-03-18 00:07:47', '', '2022-03-18 22:34:46', 'C9635');
INSERT INTO `product_sku` VALUES (2, NULL, '十二生肖之好运牛牛', '10.00', 10, 'http://zero-gz.oss-cn-guangzhou.aliyuncs.com/product%2Fbanner_todu%2F%E5%8D%81%E4%BA%8C%E7%94%9F%E8%82%96%E4%B9%8B%E5%A5%BD%E8%BF%90%E7%89%9B%E7%89%9B.jpg?OSSAccessKeyId=LTAI5tHZwvSYDLu5b84RMEWr&Expires=1678718197&Signature=IASEAXM2mOmWl6QfMHzkFE3uBEI%3D', '0', '', '2022-03-18 00:07:47', '', '2022-03-18 22:36:43', 'A5684');
INSERT INTO `product_sku` VALUES (3, NULL, '十二生肖之龙神送福', '15.99', 10, 'http://zero-gz.oss-cn-guangzhou.aliyuncs.com/product%2Fbanner_todu%2F%E5%8D%81%E4%BA%8C%E7%94%9F%E8%82%96%E4%B9%8B%E9%BE%99%E7%A5%9E%E9%80%81%E7%A6%8F.jpg?OSSAccessKeyId=LTAI5tHZwvSYDLu5b84RMEWr&Expires=1678718259&Signature=WE5VwqtEWjSr1ytzp3vZ3bpF2Wg%3D', '0', '', '2022-03-18 00:07:47', '', '2022-03-18 22:37:52', 'A9856');
INSERT INTO `product_sku` VALUES (4, NULL, '十二生肖之生肖神蛋', '16.00', 10, 'http://zero-gz.oss-cn-guangzhou.aliyuncs.com/product%2Fbanner_todu%2F%E5%8D%81%E4%BA%8C%E7%94%9F%E8%82%96%E4%B9%8B%E7%94%9F%E8%82%96%E7%A5%9E%E8%9B%8B.jpg?OSSAccessKeyId=LTAI5tHZwvSYDLu5b84RMEWr&Expires=1678718214&Signature=qA7k%2BkXNA38NzaUIoaGLzcWVoxk%3D', '0', '', '2022-03-18 00:07:47', '', '2022-03-18 22:37:21', 'A7815');
INSERT INTO `product_sku` VALUES (5, NULL, '十二生肖之暴富虎', '17.00', 10, 'http://zero-gz.oss-cn-guangzhou.aliyuncs.com/product%2Fbanner_todu%2F%E5%8D%81%E4%BA%8C%E7%94%9F%E8%82%96%E4%B9%8B%20%E6%9A%B4%E5%AF%8C%E8%99%8E.jpg?OSSAccessKeyId=LTAI5tHZwvSYDLu5b84RMEWr&Expires=1678718166&Signature=RpgFiHGPWQ%2BQiCpzxM3xB2Fcofo%3D', '0', '', '2022-03-18 00:07:47', '', '2022-03-18 22:36:23', 'B8520');
INSERT INTO `product_sku` VALUES (104, NULL, '十二生肖之兔尤怜', '3.2', NULL, 'http://zero-gz.oss-cn-guangzhou.aliyuncs.com/product%2Fbanner_todu%2F%E5%8D%81%E4%BA%8C%E7%94%9F%E8%82%96%E4%B9%8B%E5%85%94%E5%B0%A4%E6%80%9C.jpg?OSSAccessKeyId=LTAI5tHZwvSYDLu5b84RMEWr&Expires=1678721945&Signature=zDPCKsT2seMJyySBi6ke2A7rWfE%3D', '0', '', '2022-03-18 23:40:26', '', '2022-03-19 01:36:20', NULL);

-- ----------------------------
-- Table structure for product_spu
-- ----------------------------
DROP TABLE IF EXISTS `product_spu`;
CREATE TABLE `product_spu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `spu_id` bigint NULL DEFAULT NULL COMMENT 'spu的id',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品spu表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of product_spu
-- ----------------------------
INSERT INTO `product_spu` VALUES (1, NULL, '洗衣液', '0', 'admin', '2022-03-15 19:56:23', '', '2022-03-15 22:35:44', '洗衣液spu');
INSERT INTO `product_spu` VALUES (2, NULL, '肥皂', '0', 'admin', '2022-03-15 19:56:23', '', '2022-03-15 22:36:06', '测试');

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province`  (
  `code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '省份code',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '省份名',
  PRIMARY KEY (`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '省份表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of province
-- ----------------------------
INSERT INTO `province` VALUES ('11', '北京市');
INSERT INTO `province` VALUES ('12', '天津市');
INSERT INTO `province` VALUES ('13', '河北省');
INSERT INTO `province` VALUES ('14', '山西省');
INSERT INTO `province` VALUES ('15', '内蒙古自治区');
INSERT INTO `province` VALUES ('21', '辽宁省');
INSERT INTO `province` VALUES ('22', '吉林省');
INSERT INTO `province` VALUES ('23', '黑龙江省');
INSERT INTO `province` VALUES ('31', '上海市');
INSERT INTO `province` VALUES ('32', '江苏省');
INSERT INTO `province` VALUES ('33', '浙江省');
INSERT INTO `province` VALUES ('34', '安徽省');
INSERT INTO `province` VALUES ('35', '福建省');
INSERT INTO `province` VALUES ('36', '江西省');
INSERT INTO `province` VALUES ('37', '山东省');
INSERT INTO `province` VALUES ('41', '河南省');
INSERT INTO `province` VALUES ('42', '湖北省');
INSERT INTO `province` VALUES ('43', '湖南省');
INSERT INTO `province` VALUES ('44', '广东省');
INSERT INTO `province` VALUES ('45', '广西壮族自治区');
INSERT INTO `province` VALUES ('46', '海南省');
INSERT INTO `province` VALUES ('50', '重庆市');
INSERT INTO `province` VALUES ('51', '四川省');
INSERT INTO `province` VALUES ('52', '贵州省');
INSERT INTO `province` VALUES ('53', '云南省');
INSERT INTO `province` VALUES ('54', '西藏自治区');
INSERT INTO `province` VALUES ('61', '陕西省');
INSERT INTO `province` VALUES ('62', '甘肃省');
INSERT INTO `province` VALUES ('63', '青海省');
INSERT INTO `province` VALUES ('64', '宁夏回族自治区');
INSERT INTO `province` VALUES ('65', '新疆维吾尔自治区');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日历信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint NOT NULL COMMENT '触发的时间',
  `sched_time` bigint NOT NULL COMMENT '定时器制定的时间',
  `priority` int NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '调度器状态表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint NOT NULL COMMENT '开始时间',
  `end_time` bigint NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-03-15 19:56:23', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-03-15 19:56:23', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-03-15 19:56:23', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'false', 'Y', 'admin', '2022-03-15 19:56:23', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-03-15 19:56:23', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-15 19:56:22', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 4, '大专', 'dazhuan', 'edu_level', NULL, 'default', 'N', '0', 'admin', '2022-05-08 15:00:58', 'admin', '2022-05-08 15:03:43', NULL);
INSERT INTO `sys_dict_data` VALUES (101, 2, '中专', 'zhongzhuan', 'edu_level', NULL, 'default', 'N', '0', 'admin', '2022-05-08 15:01:23', 'admin', '2022-05-08 15:02:53', NULL);
INSERT INTO `sys_dict_data` VALUES (102, 1, '无', 'none', 'edu_level', NULL, 'default', 'N', '0', 'admin', '2022-05-08 15:02:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 3, '高中', 'gaozhong', 'edu_level', NULL, 'default', 'N', '0', 'admin', '2022-05-08 15:03:36', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 5, '本科', 'benke', 'edu_level', NULL, 'default', 'N', '0', 'admin', '2022-05-08 15:04:05', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, '学历层次', 'edu_level', '0', 'admin', '2022-05-08 14:59:43', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-03-15 19:56:23', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 148 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-15 22:08:50');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-15 22:08:56');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-15 22:10:23');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-15 22:10:31');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-16 20:31:17');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-16 22:52:47');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-17 19:05:28');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码已失效', '2022-03-17 21:22:03');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-17 21:22:08');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-17 23:18:39');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 01:11:50');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码已失效', '2022-03-18 01:12:34');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 01:13:01');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-18 01:13:13');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 01:13:28');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 01:13:45');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 13:21:01');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-18 20:43:56');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 20:44:13');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-18 20:44:22');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 20:44:28');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 21:41:36');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 23:23:38');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 01:00:58');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-19 01:05:00');
INSERT INTO `sys_logininfor` VALUES (125, 'manager', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 01:05:15');
INSERT INTO `sys_logininfor` VALUES (126, 'manager', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-19 01:20:35');
INSERT INTO `sys_logininfor` VALUES (127, 'manager', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 02:16:48');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 14:34:06');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-19 14:34:21');
INSERT INTO `sys_logininfor` VALUES (130, 'manager', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-19 14:34:41');
INSERT INTO `sys_logininfor` VALUES (131, 'manager', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 14:34:47');
INSERT INTO `sys_logininfor` VALUES (132, 'manager', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 14:57:15');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码已失效', '2022-05-07 19:06:44');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-05-07 19:22:15');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-05-07 19:23:51');
INSERT INTO `sys_logininfor` VALUES (136, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2022-05-07 19:40:57');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-07 19:49:23');
INSERT INTO `sys_logininfor` VALUES (138, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-08 14:24:28');
INSERT INTO `sys_logininfor` VALUES (139, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-09 18:50:50');
INSERT INTO `sys_logininfor` VALUES (140, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2022-05-09 20:43:04');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-10 20:30:35');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2022-05-11 15:15:46');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2022-05-11 15:15:49');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2022-05-11 15:15:53');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-11 15:16:42');
INSERT INTO `sys_logininfor` VALUES (146, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '用户不存在/密码错误', '2022-06-10 13:27:24');
INSERT INTO `sys_logininfor` VALUES (147, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-06-10 13:27:30');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '用户不存在/密码错误', '2022-06-10 18:40:34');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '用户不存在/密码错误', '2022-06-10 18:40:40');
INSERT INTO `sys_logininfor` VALUES (150, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-06-10 18:40:48');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2032 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 2, 'system', NULL, '', 1, 0, 'M', '0', '1', '', 'system', 'admin', '2022-03-15 19:56:23', 'admin', '2022-05-09 20:28:05', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '0', '1', '', 'monitor', 'admin', '2022-03-15 19:56:23', 'admin', '2022-05-09 20:26:29', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '1', '', 'tool', 'admin', '2022-03-15 19:56:23', 'admin', '2022-05-09 20:27:35', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-03-15 19:56:23', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-03-15 19:56:23', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-03-15 19:56:23', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-03-15 19:56:23', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-03-15 19:56:23', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-03-15 19:56:23', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-03-15 19:56:23', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-03-15 19:56:23', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2022-03-15 19:56:23', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-03-15 19:56:23', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-03-15 19:56:23', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-03-15 19:56:23', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-03-15 19:56:23', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-03-15 19:56:23', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-03-15 19:56:23', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-03-15 19:56:23', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-03-15 19:56:23', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-03-15 19:56:23', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-03-15 19:56:23', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '全部商品', 2018, 1, 'sku', 'system/sku/index', NULL, 1, 0, 'C', '0', '0', 'system:sku:list', '#', 'admin', '2022-03-15 22:31:42', 'admin', '2022-03-18 20:47:09', '商品sku菜单');
INSERT INTO `sys_menu` VALUES (2001, '商品sku查询', 2000, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sku:query', '#', 'admin', '2022-03-15 22:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2002, '商品sku新增', 2000, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sku:add', '#', 'admin', '2022-03-15 22:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '商品sku修改', 2000, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sku:edit', '#', 'admin', '2022-03-15 22:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '商品sku删除', 2000, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sku:remove', '#', 'admin', '2022-03-15 22:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '商品sku导出', 2000, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:sku:export', '#', 'admin', '2022-03-15 22:31:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '用户sku收藏', 2018, 1, 'star', 'system/star/index', NULL, 1, 0, 'C', '0', '0', 'system:star:list', '#', 'admin', '2022-03-15 22:32:03', 'admin', '2022-03-16 20:34:27', '用户sku收藏菜单');
INSERT INTO `sys_menu` VALUES (2007, '用户sku收藏查询', 2006, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:star:query', '#', 'admin', '2022-03-15 22:32:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '用户sku收藏新增', 2006, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:star:add', '#', 'admin', '2022-03-15 22:32:03', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '用户sku收藏修改', 2006, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:star:edit', '#', 'admin', '2022-03-15 22:32:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '用户sku收藏删除', 2006, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:star:remove', '#', 'admin', '2022-03-15 22:32:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '用户sku收藏导出', 2006, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:star:export', '#', 'admin', '2022-03-15 22:32:04', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '商品spu', 2018, 1, 'spu', 'system/spu/index', NULL, 1, 0, 'C', '0', '0', 'system:spu:list', '#', 'admin', '2022-03-15 22:32:12', 'admin', '2022-03-16 20:34:34', '商品spu菜单');
INSERT INTO `sys_menu` VALUES (2013, '商品spu查询', 2012, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:spu:query', '#', 'admin', '2022-03-15 22:32:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '商品spu新增', 2012, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:spu:add', '#', 'admin', '2022-03-15 22:32:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '商品spu修改', 2012, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:spu:edit', '#', 'admin', '2022-03-15 22:32:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '商品spu删除', 2012, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:spu:remove', '#', 'admin', '2022-03-15 22:32:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '商品spu导出', 2012, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:spu:export', '#', 'admin', '2022-03-15 22:32:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2018, '商城管理', 0, 1, 'mall', NULL, NULL, 1, 0, 'M', '0', '1', '', 'shopping', 'admin', '2022-03-16 20:33:06', 'admin', '2022-05-09 20:25:49', '');
INSERT INTO `sys_menu` VALUES (2019, '商品管理', 2018, 1, 'product', NULL, NULL, 1, 0, 'C', '0', '0', NULL, 'phone', 'admin', '2022-03-16 20:34:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2020, '报名信息', 0, 1, 'application', 'system/application/index', NULL, 1, 0, 'C', '0', '0', 'system:application:list', 'build', 'admin', '2022-05-08 14:32:57', 'admin', '2022-05-09 20:26:49', '报名信息菜单');
INSERT INTO `sys_menu` VALUES (2021, '报名信息查询', 2020, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:application:query', '#', 'admin', '2022-05-08 14:32:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2022, '报名信息新增', 2020, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:application:add', '#', 'admin', '2022-05-08 14:32:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2023, '报名信息修改', 2020, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:application:edit', '#', 'admin', '2022-05-08 14:32:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2024, '报名信息删除', 2020, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:application:remove', '#', 'admin', '2022-05-08 14:32:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2025, '报名信息导出', 2020, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:application:export', '#', 'admin', '2022-05-08 14:32:57', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2026, '专业管理', 0, 1, 'major', 'system/major/index', NULL, 1, 0, 'C', '0', '0', 'system:major:list', 'international', 'admin', '2022-05-08 14:33:07', 'admin', '2022-05-09 20:27:57', '专业菜单');
INSERT INTO `sys_menu` VALUES (2027, '专业查询', 2026, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:major:query', '#', 'admin', '2022-05-08 14:33:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2028, '专业新增', 2026, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:major:add', '#', 'admin', '2022-05-08 14:33:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2029, '专业修改', 2026, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:major:edit', '#', 'admin', '2022-05-08 14:33:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '专业删除', 2026, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:major:remove', '#', 'admin', '2022-05-08 14:33:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '专业导出', 2026, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:major:export', '#', 'admin', '2022-05-08 14:33:07', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2022-03-15 19:56:23', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2022-03-15 19:56:23', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 172 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'user_sku_star,product_sku,product_spu', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:17:02');
INSERT INTO `sys_oper_log` VALUES (101, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-03-15 22:17:11');
INSERT INTO `sys_oper_log` VALUES (102, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/1', '127.0.0.1', '内网IP', '{tableIds=1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:20:32');
INSERT INTO `sys_oper_log` VALUES (103, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'product_sku', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:21:03');
INSERT INTO `sys_oper_log` VALUES (104, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-03-15 22:21:14');
INSERT INTO `sys_oper_log` VALUES (105, '代码生成', 3, 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2,3,4', '127.0.0.1', '内网IP', '{tableIds=2,3,4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:22:01');
INSERT INTO `sys_oper_log` VALUES (106, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'user_sku_star,product_spu,product_sku', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:22:08');
INSERT INTO `sys_oper_log` VALUES (107, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-03-15 22:22:10');
INSERT INTO `sys_oper_log` VALUES (108, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-03-15 22:22:49');
INSERT INTO `sys_oper_log` VALUES (109, '商品spu', 2, 'com.ruoyi.web.controller.system.ProductSpuController.edit()', 'PUT', 1, 'admin', NULL, '/system/spu', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1647345383000,\"updateBy\":\"\",\"name\":\"洗衣液\",\"remark\":\"洗衣液spu\",\"updateTime\":1647354944001,\"id\":1,\"delFlag\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:35:44');
INSERT INTO `sys_oper_log` VALUES (110, '商品spu', 2, 'com.ruoyi.web.controller.system.ProductSpuController.edit()', 'PUT', 1, 'admin', NULL, '/system/spu', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1647345383000,\"updateBy\":\"\",\"name\":\"肥皂\",\"remark\":\"测试员\",\"updateTime\":1647354959754,\"id\":2,\"delFlag\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:35:59');
INSERT INTO `sys_oper_log` VALUES (111, '商品spu', 2, 'com.ruoyi.web.controller.system.ProductSpuController.edit()', 'PUT', 1, 'admin', NULL, '/system/spu', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1647345383000,\"updateBy\":\"\",\"name\":\"肥皂\",\"remark\":\"测试\",\"updateTime\":1647354966200,\"id\":2,\"delFlag\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:36:06');
INSERT INTO `sys_oper_log` VALUES (112, '商品sku', 2, 'com.ruoyi.web.controller.system.ProductSkuController.edit()', 'PUT', 1, 'admin', NULL, '/system/sku', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1647345383000,\"updateBy\":\"\",\"name\":\"蓝月亮洗衣液\",\"remark\":\"蓝月亮洗衣液\",\"sum\":1000,\"updateTime\":1647354998665,\"id\":1,\"delFlag\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:36:38');
INSERT INTO `sys_oper_log` VALUES (113, '商品sku', 2, 'com.ruoyi.web.controller.system.ProductSkuController.edit()', 'PUT', 1, 'admin', NULL, '/system/sku', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1647345383000,\"updateBy\":\"\",\"name\":\"雕牌洗衣液\",\"remark\":\"雕牌洗衣液\",\"sum\":1400,\"updateTime\":1647355023787,\"id\":2,\"delFlag\":\"0\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-15 22:37:03');
INSERT INTO `sys_oper_log` VALUES (114, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"shopping\",\"orderNum\":1,\"menuName\":\"商城管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"mall\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-16 20:33:06');
INSERT INTO `sys_oper_log` VALUES (115, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"phone\",\"orderNum\":1,\"menuName\":\"商品管理\",\"params\":{},\"parentId\":2018,\"isCache\":\"0\",\"path\":\"product\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-16 20:34:00');
INSERT INTO `sys_oper_log` VALUES (116, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"商品sku\",\"params\":{},\"parentId\":2018,\"isCache\":\"0\",\"path\":\"sku\",\"component\":\"system/sku/index\",\"children\":[],\"createTime\":1647354702000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"C\",\"perms\":\"system:sku:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-16 20:34:20');
INSERT INTO `sys_oper_log` VALUES (117, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"用户sku收藏\",\"params\":{},\"parentId\":2018,\"isCache\":\"0\",\"path\":\"star\",\"component\":\"system/star/index\",\"children\":[],\"createTime\":1647354723000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2006,\"menuType\":\"C\",\"perms\":\"system:star:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-16 20:34:27');
INSERT INTO `sys_oper_log` VALUES (118, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"商品spu\",\"params\":{},\"parentId\":2018,\"isCache\":\"0\",\"path\":\"spu\",\"component\":\"system/spu/index\",\"children\":[],\"createTime\":1647354732000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2012,\"menuType\":\"C\",\"perms\":\"system:spu:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-16 20:34:34');
INSERT INTO `sys_oper_log` VALUES (119, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"system\",\"orderNum\":2,\"menuName\":\"系统管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1647345383000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-16 20:37:06');
INSERT INTO `sys_oper_log` VALUES (120, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.48', '内网IP', '{}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\r\n### The error may exist in file [E:\\project\\uniapp\\zero-verse-back\\ruoyi-system\\target\\classes\\mapper\\system\\UserSkuStarMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.UserSkuStarMapper.insertUserSkuStar-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_sku_star\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2022-03-17 23:35:34');
INSERT INTO `sys_oper_log` VALUES (121, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.48', '内网IP', '{}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\r\n### The error may exist in file [E:\\project\\uniapp\\zero-verse-back\\ruoyi-system\\target\\classes\\mapper\\system\\UserSkuStarMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.UserSkuStarMapper.insertUserSkuStar-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_sku_star\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2022-03-17 23:35:36');
INSERT INTO `sys_oper_log` VALUES (122, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.48', '内网IP', '{}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\r\n### The error may exist in file [E:\\project\\uniapp\\zero-verse-back\\ruoyi-system\\target\\classes\\mapper\\system\\UserSkuStarMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.UserSkuStarMapper.insertUserSkuStar-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_sku_star\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2022-03-17 23:37:03');
INSERT INTO `sys_oper_log` VALUES (123, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.48', '内网IP', '{}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\r\n### The error may exist in file [E:\\project\\uniapp\\zero-verse-back\\ruoyi-system\\target\\classes\\mapper\\system\\UserSkuStarMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.UserSkuStarMapper.insertUserSkuStar-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into user_sku_star\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'\' at line 1', '2022-03-17 23:37:44');
INSERT INTO `sys_oper_log` VALUES (124, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.48', '内网IP', '{\"userId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-17 23:40:18');
INSERT INTO `sys_oper_log` VALUES (125, '商品sku', 3, 'com.ruoyi.web.controller.system.ProductSkuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/sku/1', '192.168.43.48', '内网IP', '{ids=1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-17 23:40:21');
INSERT INTO `sys_oper_log` VALUES (126, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.228', '内网IP', '{\"userId\":101}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-17 23:58:01');
INSERT INTO `sys_oper_log` VALUES (127, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.228', '内网IP', '{\"userId\":102}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 00:39:06');
INSERT INTO `sys_oper_log` VALUES (128, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.228', '内网IP', '{\"userId\":103}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 00:52:37');
INSERT INTO `sys_oper_log` VALUES (129, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, 'admin', NULL, '/system/star', '192.168.43.228', '内网IP', '{\"userId\":104}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 00:53:19');
INSERT INTO `sys_oper_log` VALUES (130, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, '18401245270', NULL, '/system/star', '192.168.43.228', '内网IP', '{\"userId\":105}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 00:57:15');
INSERT INTO `sys_oper_log` VALUES (131, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, '18401245270', NULL, '/system/star', '192.168.43.228', '内网IP', '{\"userId\":106}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 00:58:35');
INSERT INTO `sys_oper_log` VALUES (132, '用户新增收藏', 1, 'com.ruoyi.web.controller.system.UserSkuStarController.add()', 'POST', 1, '18401245270', NULL, '/system/star', '192.168.43.228', '内网IP', '{\"userId\":107}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 01:16:39');
INSERT INTO `sys_oper_log` VALUES (133, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'order_sub', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 13:21:15');
INSERT INTO `sys_oper_log` VALUES (134, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-03-18 13:21:23');
INSERT INTO `sys_oper_log` VALUES (135, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"全部商品\",\"params\":{},\"parentId\":2018,\"isCache\":\"0\",\"path\":\"sku\",\"component\":\"system/sku/index\",\"children\":[],\"createTime\":1647354702000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"C\",\"perms\":\"system:sku:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 20:47:09');
INSERT INTO `sys_oper_log` VALUES (136, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"13103161178\",\"admin\":false,\"password\":\"$2a$10$us2uDfWJkKDX9.HjMV8zsu9viVeqQ1x5nsAcrV8RxBkd./w5XiQHy\",\"postIds\":[],\"nickName\":\"商城管理者\",\"params\":{},\"userName\":\"manager\",\"userId\":102,\"createBy\":\"admin\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 20:48:58');
INSERT INTO `sys_oper_log` VALUES (137, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', 1, 'admin', NULL, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"商城管理者\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1647345383000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"common\",\"roleName\":\"商城管理者\",\"menuIds\":[2018,2000,2001,2002,2003,2004,2005],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-03-18 20:49:34');
INSERT INTO `sys_oper_log` VALUES (138, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'major,application,areas,city,province', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 14:25:06');
INSERT INTO `sys_oper_log` VALUES (139, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-05-08 14:25:28');
INSERT INTO `sys_oper_log` VALUES (140, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"学历层次\",\"params\":{},\"dictType\":\"edu_level\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 14:59:43');
INSERT INTO `sys_oper_log` VALUES (141, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"zhuanke\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"专科\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:00:58');
INSERT INTO `sys_oper_log` VALUES (142, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"zhuan2ben\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"专升本\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:01:23');
INSERT INTO `sys_oper_log` VALUES (143, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"none\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"无\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:02:25');
INSERT INTO `sys_oper_log` VALUES (144, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"zhongzhuan\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"中专\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1651993283000,\"dictCode\":101,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:02:53');
INSERT INTO `sys_oper_log` VALUES (145, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"dazhuan\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"大专\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1651993258000,\"dictCode\":100,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:03:11');
INSERT INTO `sys_oper_log` VALUES (146, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"gaozhong\",\"listClass\":\"default\",\"dictSort\":3,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"高中\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:03:36');
INSERT INTO `sys_oper_log` VALUES (147, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"dazhuan\",\"listClass\":\"default\",\"dictSort\":4,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"大专\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1651993258000,\"dictCode\":100,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:03:43');
INSERT INTO `sys_oper_log` VALUES (148, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"benke\",\"listClass\":\"default\",\"dictSort\":5,\"params\":{},\"dictType\":\"edu_level\",\"dictLabel\":\"本科\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:04:05');
INSERT INTO `sys_oper_log` VALUES (149, '专业', 1, 'com.ruoyi.web.controller.system.MajorController.add()', 'POST', 1, 'admin', NULL, '/system/major', '127.0.0.1', '内网IP', '{\"code\":\"12121\",\"schoolBelonged\":\"清华\",\"createTime\":1651993790233,\"level\":\"zhongzhuan\",\"name\":\"计算机科学\",\"id\":1,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:09:50');
INSERT INTO `sys_oper_log` VALUES (150, '字典类型', 9, 'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()', 'DELETE', 1, 'admin', NULL, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:20:20');
INSERT INTO `sys_oper_log` VALUES (151, '专业', 2, 'com.ruoyi.web.controller.system.MajorController.edit()', 'PUT', 1, 'admin', NULL, '/system/major', '127.0.0.1', '内网IP', '{\"code\":\"12121\",\"schoolBelonged\":\"清华\",\"createTime\":1651993790000,\"level\":\"gaozhong\",\"name\":\"计算机科学\",\"updateTime\":1651995425011,\"id\":1,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:37:05');
INSERT INTO `sys_oper_log` VALUES (152, '专业', 2, 'com.ruoyi.web.controller.system.MajorController.edit()', 'PUT', 1, 'admin', NULL, '/system/major', '127.0.0.1', '内网IP', '{\"code\":\"12121\",\"schoolBelonged\":\"清华\",\"createTime\":1651993790000,\"level\":\"dazhuan\",\"name\":\"计算机科学\",\"updateTime\":1651995465665,\"id\":1,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:37:45');
INSERT INTO `sys_oper_log` VALUES (153, '专业', 2, 'com.ruoyi.web.controller.system.MajorController.edit()', 'PUT', 1, 'admin', NULL, '/system/major', '127.0.0.1', '内网IP', '{\"code\":\"12121\",\"schoolBelonged\":\"清华\",\"createTime\":1651993790000,\"level\":\"dazhuan\",\"name\":\"计算机科学\",\"remark\":\"分\",\"updateTime\":1651995470714,\"id\":1,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:37:50');
INSERT INTO `sys_oper_log` VALUES (154, '报名信息', 1, 'com.ruoyi.web.controller.system.ApplicationController.add()', 'POST', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"majorId\":212,\"eduCode\":\"111\",\"params\":{},\"areaCode\":\"1101\",\"createTime\":1651996746185,\"phone\":\"6565466546\",\"studentName\":\"张三\",\"id\":2,\"majorCode\":\"111\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-08 15:59:06');
INSERT INTO `sys_oper_log` VALUES (155, '报名信息', 1, 'com.ruoyi.web.controller.system.ApplicationController.add()', 'POST', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"eduCode\":\"104\",\"params\":{},\"areaCode\":\"110101\",\"createTime\":1652094935867,\"phone\":\"15656565656\",\"studentName\":\"紫\",\"id\":3,\"majorCode\":\"12121\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 19:15:35');
INSERT INTO `sys_oper_log` VALUES (156, '报名信息', 1, 'com.ruoyi.web.controller.system.ApplicationController.add()', 'POST', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"eduCode\":\"102\",\"params\":{},\"areaCode\":\"110101\",\"createTime\":1652095105128,\"phone\":\"15656565656\",\"studentName\":\"紫\",\"id\":4,\"majorCode\":\"12121\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 19:18:25');
INSERT INTO `sys_oper_log` VALUES (157, '报名信息', 1, 'com.ruoyi.web.controller.system.ApplicationController.add()', 'POST', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"eduCode\":\"102\",\"params\":{},\"areaCode\":\"110101\",\"createTime\":1652095141660,\"phone\":\"15656565656\",\"studentName\":\"我去热武器\",\"id\":5,\"majorCode\":\"12121\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 19:19:01');
INSERT INTO `sys_oper_log` VALUES (158, '报名信息', 1, 'com.ruoyi.web.controller.system.ApplicationController.add()', 'POST', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"eduCode\":\"100\",\"params\":{},\"areaCode\":\"370304\",\"createTime\":1652095311894,\"phone\":\"15656565656\",\"studentName\":\"的发射点\",\"id\":6,\"majorCode\":\"12121\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 19:21:51');
INSERT INTO `sys_oper_log` VALUES (159, '专业', 1, 'com.ruoyi.web.controller.system.MajorController.add()', 'POST', 1, 'admin', NULL, '/system/major', '127.0.0.1', '内网IP', '{\"code\":\"2345\",\"schoolBelonged\":\"北京大学\",\"createTime\":1652095410883,\"level\":\"gaozhong\",\"name\":\"生命科学\",\"id\":2,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 19:23:30');
INSERT INTO `sys_oper_log` VALUES (160, '报名信息', 1, 'com.ruoyi.web.controller.system.ApplicationController.add()', 'POST', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"eduCode\":\"102\",\"params\":{},\"areaCode\":\"110101\",\"createTime\":1652095986907,\"phone\":\"15656565656\",\"studentName\":\"大\",\"id\":7,\"majorCode\":\"12121\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 19:33:06');
INSERT INTO `sys_oper_log` VALUES (161, '报名信息', 1, 'com.ruoyi.web.controller.system.ApplicationController.add()', 'POST', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"eduCode\":\"101\",\"params\":{},\"areaCode\":\"110101\",\"createTime\":1652096381982,\"phone\":\"13100003434\",\"studentName\":\"大概是\",\"id\":8,\"majorCode\":\"12121\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 19:39:42');
INSERT INTO `sys_oper_log` VALUES (162, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:24:02');
INSERT INTO `sys_oper_log` VALUES (163, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2018', '127.0.0.1', '内网IP', '{menuId=2018}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', 0, NULL, '2022-05-09 20:25:43');
INSERT INTO `sys_oper_log` VALUES (164, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"shopping\",\"orderNum\":1,\"menuName\":\"商城管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"mall\",\"children\":[],\"createTime\":1647433986000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2018,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:25:49');
INSERT INTO `sys_oper_log` VALUES (165, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"monitor\",\"orderNum\":2,\"menuName\":\"系统监控\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"monitor\",\"children\":[],\"createTime\":1647345383000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:26:29');
INSERT INTO `sys_oper_log` VALUES (166, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":1,\"menuName\":\"报名信息\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"application\",\"component\":\"system/application/index\",\"children\":[],\"createTime\":1651991577000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2020,\"menuType\":\"C\",\"perms\":\"system:application:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:26:49');
INSERT INTO `sys_oper_log` VALUES (167, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":1,\"menuName\":\"专业管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"major\",\"component\":\"system/major/index\",\"children\":[],\"createTime\":1651991587000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2026,\"menuType\":\"C\",\"perms\":\"system:major:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:27:13');
INSERT INTO `sys_oper_log` VALUES (168, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":3,\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1647345383000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:27:35');
INSERT INTO `sys_oper_log` VALUES (169, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"international\",\"orderNum\":1,\"menuName\":\"专业管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"major\",\"component\":\"system/major/index\",\"children\":[],\"createTime\":1651991587000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2026,\"menuType\":\"C\",\"perms\":\"system:major:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:27:57');
INSERT INTO `sys_oper_log` VALUES (170, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"system\",\"orderNum\":2,\"menuName\":\"系统管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1647345383000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 20:28:05');
INSERT INTO `sys_oper_log` VALUES (171, '报名信息', 2, 'com.ruoyi.web.controller.system.ApplicationController.edit()', 'PUT', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"eduCode\":\"zhongzhuan\",\"updateTime\":1652103062667,\"params\":{},\"areaCode\":\"110101\",\"phone\":\"13145678907\",\"studentName\":\"想不想吃\",\"id\":1,\"majorCode\":\"2345\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 21:31:02');
INSERT INTO `sys_oper_log` VALUES (172, '报名信息', 2, 'com.ruoyi.web.controller.system.ApplicationController.edit()', 'PUT', 1, 'admin', NULL, '/system/application', '127.0.0.1', '内网IP', '{\"majorId\":212,\"eduCode\":\"dazhuan\",\"updateTime\":1652103346344,\"params\":{},\"areaCode\":\"120101\",\"createTime\":1651996746000,\"phone\":\"13105061452\",\"studentName\":\"张三\",\"id\":2,\"majorCode\":\"12121\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-05-09 21:35:46');
INSERT INTO `sys_oper_log` VALUES (173, '用户管理', 6, 'com.ruoyi.web.controller.system.SysUserController.importData()', 'POST', 1, 'admin', NULL, '/system/user/importData', '127.0.0.1', '内网IP', 'true', NULL, 1, '', '2022-06-10 20:52:26');
INSERT INTO `sys_oper_log` VALUES (174, '专业', 6, 'com.ruoyi.web.controller.system.MajorController.importData()', 'POST', 1, 'admin', NULL, '/system/major/importData', '127.0.0.1', '内网IP', 'true', NULL, 1, '很抱歉，导入失败！共 3 条数据格式不正确，错误如下：<br/>1、账号 计算机科学 导入失败：nested exception is org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'code\', mode=IN, javaType=class java.lang.Long, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Long<br/>2、账号 计算机网络 导入失败：nested exception is org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'code\', mode=IN, javaType=class java.lang.Long, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Long<br/>3、账号 软件工程 导入失败：nested exception is org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'code\', mode=IN, javaType=class java.lang.Long, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Long', '2022-06-10 20:53:51');
INSERT INTO `sys_oper_log` VALUES (175, '专业', 6, 'com.ruoyi.web.controller.system.MajorController.importData()', 'POST', 1, 'admin', NULL, '/system/major/importData', '127.0.0.1', '内网IP', 'true', NULL, 1, '很抱歉，导入失败！共 3 条数据格式不正确，错误如下：<br/>1、专业 计算机科学 导入失败：nested exception is org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'code\', mode=IN, javaType=class java.lang.Long, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Long<br/>2、专业 计算机网络 导入失败：nested exception is org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'code\', mode=IN, javaType=class java.lang.Long, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Long<br/>3、专业 软件工程 导入失败：nested exception is org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'code\', mode=IN, javaType=class java.lang.Long, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Long', '2022-06-10 21:02:23');
INSERT INTO `sys_oper_log` VALUES (176, '专业', 6, 'com.ruoyi.web.controller.system.MajorController.importData()', 'POST', 1, 'admin', NULL, '/system/major/importData', '127.0.0.1', '内网IP', 'true', '{\"msg\":\"恭喜您，数据已全部导入成功！共 3 条，数据如下：<br/>1、专业 计算机科学 导入成功<br/>2、专业 计算机网络 导入成功<br/>3、专业 软件工程 导入成功\",\"code\":200}', 0, NULL, '2022-06-10 21:06:27');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-03-15 19:56:23', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-03-15 19:56:23', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-03-15 19:56:23', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '商城管理者', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2022-03-15 19:56:23', 'admin', '2022-03-18 20:49:34', '商城管理者');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `dept_id` bigint NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 2000);
INSERT INTO `sys_role_menu` VALUES (2, 2001);
INSERT INTO `sys_role_menu` VALUES (2, 2002);
INSERT INTO `sys_role_menu` VALUES (2, 2003);
INSERT INTO `sys_role_menu` VALUES (2, 2004);
INSERT INTO `sys_role_menu` VALUES (2, 2005);
INSERT INTO `sys_role_menu` VALUES (2, 2018);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '张三', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$rMc.4cmUkOLNIASUIkfwn.E645seOYV5HLQMzAvozJjI6thtrpTu.', '0', '0', '127.0.0.1', '2022-06-10 18:40:48', 'admin', '2022-03-15 19:56:23', '', '2022-06-10 18:40:48', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '李四', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-03-15 19:56:23', 'admin', '2022-03-15 19:56:23', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (100, NULL, 'jk', '罗琳', '00', '', '15732458199', '0', '', '', '0', '0', '', NULL, '', '2022-03-17 23:30:23', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (101, NULL, 'jack', '马', '00', '', '18401245270', '0', '', '', '0', '0', '', NULL, '', '2022-03-17 23:57:50', '', NULL, NULL);
INSERT INTO `sys_user` VALUES (102, NULL, 'manager', '商城管理者', '00', '', '13103161178', '0', '', '$2a$10$us2uDfWJkKDX9.HjMV8zsu9viVeqQ1x5nsAcrV8RxBkd./w5XiQHy', '0', '0', '127.0.0.1', '2022-03-19 14:57:15', 'admin', '2022-03-18 20:48:58', '', '2022-03-19 14:57:15', NULL);
INSERT INTO `sys_user` VALUES (103, NULL, '17703167819', '17703167819', '00', '', '17703167819', '0', '', '', '0', '0', '', NULL, '', '2022-03-19 01:59:04', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (102, 2);

-- ----------------------------
-- Table structure for user_sku_star
-- ----------------------------
DROP TABLE IF EXISTS `user_sku_star`;
CREATE TABLE `user_sku_star`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `sku_id` bigint NOT NULL COMMENT 'sku的id',
  PRIMARY KEY (`user_id`, `sku_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户sku收藏表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_sku_star
-- ----------------------------
INSERT INTO `user_sku_star` VALUES (1, 1);
INSERT INTO `user_sku_star` VALUES (1, 2);
INSERT INTO `user_sku_star` VALUES (1, 3);
INSERT INTO `user_sku_star` VALUES (2, 1);
INSERT INTO `user_sku_star` VALUES (2, 2);
INSERT INTO `user_sku_star` VALUES (2, 3);
INSERT INTO `user_sku_star` VALUES (101, 1);
INSERT INTO `user_sku_star` VALUES (101, 2);
INSERT INTO `user_sku_star` VALUES (101, 3);
INSERT INTO `user_sku_star` VALUES (108, 0);

SET FOREIGN_KEY_CHECKS = 1;
